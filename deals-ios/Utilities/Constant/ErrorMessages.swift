import Foundation

class ErrorMessages {
    static let unsupportedCountry = "Current country not supported. Please select another country"
    static let enterPin = "Please enter 4-digit code"
    static let selectPlan = "Please select a plan"
    static let paystackError = "Error making Payment with Paystack. Please use other payment options or try again later."
    static let flutterwaveError = "Error making Payment with Flutterwave. Please use other payment options or try again later."
    static let paymentCancelled = "Payment Cancelled"
    static let invalidURL = "Invalid URL"
    static let enterNumber = "Please enter a valid number"
    static let selectCountry = "Please select a country"
    static let enterReferralId = "Please enter a referral id"
    static let imageFileError = "Unable to select image file."
    static let selectPaymentGateway = "Please select a payment gateway"
    static let walletUnavailable = "Wallet unavailable"
    static let loggedOut = "User Logged out"
    static let internetDisconnected = "Internet Disconnected"
    static var fieldError: (String) -> String {
        { item in
            "Please enter your \(item)"
        }
    }
    static let noDealFound = "No deals found. Please try again later"
    static let unknownError = "Unknown Error"
    static let serviceUnavailable = "Service unavailable"
}
