import Foundation
import FCUUID

class Constants {
    
    /// Unique device Id
    static var deviceId: String {
        return FCUUID.uuidForDevice()
    }
    
    /// Uniqueddevice Id
    static var deviceLocation: (latitude: String, longitude: String) {
        return (latitude: "6.507320", longitude: "3.345810")
    }
    
    /// This is the terms and conditions url
    static var termsAndConditions: String {
        return "https://dealday.africa/terms"
    }
    
    /// This is the privacy policy url
    static var privacyPolicy: String {
        return "https://thankucash.com/privacy"
    }
    
    /// This is the refund policy url
    static var refundPolicy: String {
        return "https://dealday.africa/refundpolicy"
    }
    
    /// This is the thank u cash twitter urls containing both app url and web url.
    ///  Web url is to be used only when app fails (not installed)
    static var twitter: (appURl: String, webUrl: String) {
        return (appURl: "twitter://user?screen_name=thankucash", webUrl: "https://twitter.com/thankucash")
    }
    
    /// This is the thank u cash whatsapp urls containing both app url and web url.
    ///  Web url is to be used only when app fails (not installed)
    static var whatsApp: (appURl: String, webUrl: String) {
        return (appURl: "https://api.whatsapp.com/send?phone=\(+2349087106997)", webUrl: "https://wa.me/\(+2349087106997)")
    }
    
    /// This is the thank u cash facebookurls containing both app url and web url.
    ///  Web url is to be used only when app fails (not installed)
    static var faceBook: (appURl: String, webUrl: String) {
        return (appURl: "fb://profile/Thankucash", webUrl: "http://www.facebook.com/Thankucash")
    }
    
    /// This is the thank u cash youtube urls containing both app url and web url.
    ///  Web url is to be used only when app fails (not installed)
    static var youTube: (appURl: String, webUrl: String) {
        return (appURl: "youtube://thankucash", webUrl: "https://www.youtube.com/thankucash")
    }
    
    /// This is the thank u cash mail url
    static var mail: String {
        return "message://hello@thankucash.com"
    }
    
    /// This is the thank u cash google map urls containing both app url and web url.
    ///  Web url is to be used only when app fails (not installed)
    static var googleMapApi: (appURl: String, webUrl: String) {
        return (appURl: "comgooglemaps-x-callback://?saddr=&daddr=\(6.452970),\(3.476900)&directionsmode=driving", webUrl: "https://www.google.co.in/maps/dir/?saddr=&daddr=\(6.452970),\(3.476900)&directionsmode=driving")
    }
    
    /// This is a thank u cash support phone number
    static var numberOne: String {
        return "tel://+2349087106997"
    }
    
    /// This is a thank u cash support phone number
    static var numberTwo: String {
        return "tel://+2348133178563"
    }
    
    /// This is a thank u cash support phone number
    static var numberThree: String {
        return "tel://01 888 8177"
    }
}
