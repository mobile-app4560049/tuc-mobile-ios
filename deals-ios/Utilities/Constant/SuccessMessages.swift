import Foundation

class SuccessMessages {
    static var copySuccess: (String) -> String {
        { item in
            "\(item) copied successfully!"
        }
    }
    static let guestLogin = "Logged in as a guest"
    
}

