import UIKit

class Validator{
    private var controller: UIViewController!
    
    init(_ controller: UIViewController) {self.controller = controller}
    
//    func usernameValidation(_ username: String?) -> Bool{
//        if (username?.matches("^[A-Za-z0-9_]+(?:[_][A-Za-z0-9]+)*$") ?? false){return true}
//        let usernameMessage = "Username must not contain any special characters"
//        controller.showError(message: usernameMessage)
//        return false
//    }
    
    func emailValidation(_ email: String) -> Bool {
        let emailRegex = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        let emailPredicate = NSPredicate(format: "SELF MATCHES %@", emailRegex)
        if emailPredicate.evaluate(with: email) {return true}
        controller.showError(message: "Please enter a valid email")
        return false
    }
    
    func lengthValidation(_ title: String, _ number: String, minLenght: Int) -> Bool{
        if (number.count >= minLenght) {return true}
        controller.showError(message: "\(title) must be at least \(minLenght) digits")
        return false
    }

    func pinValidation(_ pin: String) -> Bool{
        if (pin.count == 4) {return true}
        controller.showError(message: "Please Enter 4-digit PIN")
        return false
    }
    
    func amountValidation(_ amount: String?, min: Double, max: Double = -1) -> Bool{
        if let amount = amount {
            let amount = Double(amount) ?? 0
            if (min == -1 || amount >= min) && (max == -1 || amount <= max){return true}
        }
        controller.showError(message: "Enter an amount from \(min) & less than \(max)")
        return false
    }
    
    func phoneNumberValidation(_ phoneNumber: String?, _ message: String = "invalPhoneNumber") -> Bool{
        if let phoneNumber = phoneNumber {
            if (phoneNumber.count >= 5) && (phoneNumber.count <= 15){return true}
        }
        controller.showError(message: "Invalid Phone number")
        return false
    }
    
    
    func textValidation(_ text: String?, _ message: String) -> Bool {
        if text != nil && !(text!.elementsEqual("")) {return true}
        controller.showError(message: message)
        return false
    }
    
    func checkEqual(_ textA: String?, _ textB: String?, fieldName: String) -> Bool{
        if (textA ?? "").elementsEqual(textB ?? ""){ return true }
        controller.showError(message: "\(fieldName) do not match")
        return false
    }
    
    func checkSame(_ textA: String?, _ textB: String?, _ message: String) -> Bool {
        if (textA ?? "").elementsEqual(textB ?? "") {
            controller.showError(message: message)
            return false
        }
        return true
    }
}
