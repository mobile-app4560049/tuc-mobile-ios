import Foundation

protocol DealsKeyValueDataSource {
    
    //MARK: Onboarding
    func isLoggedIn() -> Bool
    func isOnboarded() -> Bool
    func logOut()
    
    //MARK: User
    func saveUser(_ user: User?)
    func getUser() -> User?
    func updateUser(displayName: String?, name: String?, iconUrl: String?, referralCode: String?, country: Country?)
}
