import Foundation

//MARK: Keys for identifying data to be saved
/// Keys for identifying data to be saved
enum Keys {
    case userPreference, userKey, onBoarded, loggedIn, countryForApi
    
    /// String values for keys to identify data to be saved
    var value: String {
        switch self {
        case .userPreference:
            return "user_preference"
        case .userKey:
            return "deals.currrent_user"
        case .onBoarded:
            return "deals.on_boarded"
        case .loggedIn:
            return "deals.logged_in"
        case .countryForApi:
            return "deals.country_for_api"
        }
    }
}

