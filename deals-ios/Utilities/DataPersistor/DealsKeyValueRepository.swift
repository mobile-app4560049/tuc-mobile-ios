import Foundation

class DealsKeyValueRepository: DealsKeyValueDataSource {
    
    static let sharedInstance = DealsKeyValueRepository()
    private let userDefault: UserDefaults
    
    private init() {
        self.userDefault = UserDefaults(suiteName: Keys.userPreference.value)!
    }

    //MARK: Onboarding
    /// Check if user has seen onboarding screens
    func isOnboarded() -> Bool {
        if let isOnboard = self.userDefault.value(forKey: Keys.onBoarded.value) as? Bool {
            return isOnboard
        }
        return false
    }
    
    /// Sets the onBoarded status of a user so they never see the onboarding screens
    func onBoardUser() {
        self.userDefault.set(true, forKey: Keys.onBoarded.value)
    }
    
    /// Sets the Logged in status of a user
    func loginUser(countryForApi: SelectedCountry) {
        self.userDefault.set(true, forKey: Keys.loggedIn.value)
        self.userDefault.set(try? PropertyListEncoder().encode(countryForApi), forKey: Keys.countryForApi.value)
    }
    
    /// Gets the current country to be user for api calls
    func getCountryForApi() -> SelectedCountry? {
        if let user = self.userDefault.object(forKey: Keys.countryForApi.value) as? Data {
            return try? PropertyListDecoder().decode(SelectedCountry.self, from: user)
        } else {
            return nil
        }
    }
    
    /// Saves user country
    func saveUserCountry(_ country: SelectedCountry) {
        self.userDefault.set(try? PropertyListEncoder().encode(country), forKey: Keys.countryForApi.value)
    }
    
    /// Check if user is logged in
    func isLoggedIn() -> Bool {
//        return self.getUser() != nil //&& self.getAuthToken() != nil
        if let isLoggedIn = self.userDefault.value(forKey: Keys.loggedIn.value) as? Bool {
            return isLoggedIn
        }
        return false
    }
    
    /// Logs out user and clears data from local storage
    func logOut() {
        self.userDefault.removeObject(forKey: Keys.userKey.value)
        self.userDefault.removeObject(forKey: Keys.countryForApi.value)
        self.userDefault.set(false, forKey: Keys.loggedIn.value)
    }
    
    //MARK: User
    ///Saves user data to local storage
    func saveUser(_ user: User?) {
        if let value = user {
            self.userDefault.set(try? PropertyListEncoder().encode(value), forKey: Keys.userKey.value)
        } else {
            self.userDefault.removeObject(forKey: Keys.userKey.value)
        }
    }
    
    /// Gets the currently logged in user
    func getUser() -> User? {
        if let user = self.userDefault.object(forKey: Keys.userKey.value) as? Data {
            return try? PropertyListDecoder().decode(User.self, from: user)
        } else {
            return nil
        }
    }
    
    /// Updates fields for the currently logged in user
    func updateUser(displayName: String? = nil, name: String? = nil, iconUrl: String? = nil, referralCode: String? = nil, country: Country? = nil) {
        if var user = getUser() {
            
            if let displayName = displayName {
                user.displayName = displayName
            }
            
            if let name = name {
                user.name = name
            }
            
            if let iconUrl = iconUrl {
                user.iconUrl = iconUrl
            }
            
            if let referralCode = referralCode {
                user.referralCode = referralCode
            }
            
            if let country = country {
                user.country = country
            }
        }
    }

}

