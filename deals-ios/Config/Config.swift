import Foundation
import CoreLocation

enum BuildType: String {
    case debug, release
}

/// Build Configurations settings for the app
struct Configuration {
    
    /// Build environment type
    ///   Dev, Staging and Prod
    static let environment: Environment = {
    #if DEV
        return .dev
    #elseif STAGING
        return .staging
    #else
        return .prod
    #endif
    }()
    
    /// Build types for the app
    /// This is the build type depending on what environment flavor is currently being run (debug and release)
    static let buildType: BuildType = {
    #if DEV
        return .debug
    #else
        return .release
    #endif
    }()
}

enum Environment {
    case dev, staging, prod
    
    ///BaseURL for the app
    /// This is the base URL depending on what environment flavor is currently being run
    var baseURL: String {
        switch self {
        case .dev:
            return "https://dealsconnect.thankucash.dev/api/"
        case .staging:
            return "https://dealsconnect.thankucash.tech/api/"
        case .prod:
            return "https://dealsconnect.thankucash.com/api/"
        }
    }
    
    /// FlutterWave key for the app
    /// This is the base flutter wave key depending on what environment flavor is currently being run
    var fltWKey: String {
        switch self {
        case .dev:
            return "FLWPUBK_TEST-a6e984618172d190fd60d61b15673910-X"
            //TODO: Change this key
        case .staging:
            return "FLWPUBK_TEST-a6e984618172d190fd60d61b15673910-X"
            //TODO: Change this key
        case .prod:
            return "FLWPUBK_TEST-a6e984618172d190fd60d61b15673910-X"
        }
    }
    
    /// FlutterWave key for the app
    /// This is the base flutter wave encryption key depending on what environment flavor is currently being run
    var fltWEncryptKey: String {
        switch self {
        case .dev:
            return "FLWSECK_TESTf4e17befeb88"
            //TODO: Change this key
        case .staging:
            return "FLWSECK_TESTf4e17befeb88"
            //TODO: Change this key
        case .prod:
            return "FLWSECK_TESTf4e17befeb88"
        }
    }
    
    /// Paystack key for the app
    /// This is the base paystack key depending on what environment flavor is currently being run
    var paystackKey: String {
        switch self {
        case .dev:
            return "pk_test_4cca266d686312285a54570e20b46a808ae0d0f6"
            //TODO: Change this key
        case .staging:
            return "pk_test_4cca266d686312285a54570e20b46a808ae0d0f6"
            //TODO: Change this key
        case .prod:
            return "pk_test_4cca266d686312285a54570e20b46a808ae0d0f6"
        }
    }
    
    /// AppCenter secret key for the app
    /// This is the secret key for AppCenter depending on what environment flavor is currently being run
    var appCenterKey: String {
        switch self {
        case .dev:
            return "12b407c0-0e8e-4584-89cf-10bbf74c1ad6"
            //TODO: Change this key
        case .staging:
            return "12b407c0-0e8e-4584-89cf-10bbf74c1ad6"
            //TODO: Change this key
        case .prod:
            return "12b407c0-0e8e-4584-89cf-10bbf74c1ad6"
        }
    }
    
    /// FreshChat App Configuration settings (appID, appKey and domain)
    var freshChatConfig: (appID: String, appKey: String, domain: String) {
        return (appID: "1c7544b2-a22a-4154-958e-bf12598f58cc", appKey: "a3cd73a2-73b4-4df7-bd26-8b5cecad08c1", domain: "msdk.freshchat.com")
    }
    
    /// Network app key for api authentication
    var appKey: String {
        return "9a369214b7a84d73b954bfb79c5809e6"
    }
    
    /// Network app version key for api authentication
    var appVersionKey: String {
        return "5f320c496782415c91a74891668d5126"
    }
    
    /// Network app version key for api authentication
    var authorizationKey: String {
        return "MThkYTY0OGE1OTAyNGYyYmE5M2UyZTA3MzE5YjE3NjQ="
    }
    
    /// Network ask key for api authentication
    var ask: String {
       return "Wk9PRDVzaHBKSkFEdEpzVGNXeiszMFc4K0c2ZmowUm9FUktvbk5xRFdNbFFUbHIxTEhFQ0lYaW1wTUduczEwdg=="
    }
    
}
