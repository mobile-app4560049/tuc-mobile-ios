import Foundation

struct RewardHistoryDetail {
    let rewardPoints: String
    let location: String
    let transactionStatus: String
    let paidAmount: String
    let date: String
    let time: String
    let referenceID: String
    let referenceNumber: String
    let salesInvoiceNumber: String
}
