import Foundation

enum SelectedCountry: Codable {
    case nigeria, ghana, kenya, rwanda, uganda
    
    /// Country phone code `234`
    var isd: String {
        switch self {
        case .nigeria:
            return "234"
        case .ghana:
            return "233"
        case .kenya:
            return "254"
        case .rwanda:
            return "250"
        case .uganda:
            return "256"
        }
    }
    
    /// Country id `1`
    var id: Int {
        switch self {
        case .nigeria:
            return 1
        case .ghana:
            return 2
        case .kenya:
            return 3
        case .rwanda:
            return 4
        case .uganda:
            return 5
        }
    }
    
    /// Country key
    /// This is the country name in lowercase `nigeria`
    var countryKey: String {
        switch self {
        case .nigeria:
            return "nigeria"
        case .ghana:
            return "ghana"
        case .kenya:
            return "kenya"
        case .rwanda:
            return "rwanda"
        case .uganda:
            return "uganda"
        }
    }
    
    var countryISOCode: String {
        switch self {
        case .nigeria:
            return "ng"
        case .ghana:
            return "gh"
        case .kenya:
            return "ke"
        case .rwanda:
            return "rw"
        case .uganda:
            return "ug"
        }
    }
}
