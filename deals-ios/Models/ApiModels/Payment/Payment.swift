//
//  Payment.swift
//  deals-ios
//
//  Created by ThankUCash Tech02 on 3/31/23.
//

import Foundation

enum PaymentSource {
    case card, wallet
    
    var literal: String {
        switch self {
        case .card:
            return "online"
        case .wallet:
            return "wallet"
        }
    }
}
