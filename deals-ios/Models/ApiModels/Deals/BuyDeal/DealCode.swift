import Foundation

struct DealCodeResponse: Codable {
    let referenceID: Int
    let referenceKey: String
    let dealReferenceID: Int
    let dealReferenceKey: String
    let accountID: Int
    let accountKey, accountDisplayName, accountMobileNumber: String
    let accountIconURL: String
    let itemCode, startDate, endDate: String
    let statusID: Int
    let statusCode, statusName: String
    let quantity, amount, commissionAmount, charge: Int
    let deliveryCharge, totalAmount: Int
    let title, description: String
    let imageURL: String
    let merchantReferenceID: Int
    let merchantReferenceKey, merchantDisplayName: String
    let merchantIconURL: String
    let merchantContactNumber, merchantEmail, redeemInstruction, createDate: String
    let sharedCustomerIconURL: String
    let dealTypeID: Int
    let locations: [Location]
    let dealTypeCode, deliveryTypeCode: String
    let deliveryDetails: DeliveryDetails
    let paymentDetails: PaymentDetails
    let transactionID: Int

    enum CodingKeys: String, CodingKey {
        case referenceID = "ReferenceId"
        case referenceKey = "ReferenceKey"
        case dealReferenceID = "DealReferenceId"
        case dealReferenceKey = "DealReferenceKey"
        case accountID = "AccountId"
        case accountKey = "AccountKey"
        case accountDisplayName = "AccountDisplayName"
        case accountMobileNumber = "AccountMobileNumber"
        case accountIconURL = "AccountIconUrl"
        case itemCode = "ItemCode"
        case startDate = "StartDate"
        case endDate = "EndDate"
        case statusID = "StatusId"
        case statusCode = "StatusCode"
        case statusName = "StatusName"
        case quantity = "Quantity"
        case amount = "Amount"
        case commissionAmount = "CommissionAmount"
        case charge = "Charge"
        case deliveryCharge = "DeliveryCharge"
        case totalAmount = "TotalAmount"
        case title = "Title"
        case description = "Description"
        case imageURL = "ImageUrl"
        case merchantReferenceID = "MerchantReferenceId"
        case merchantReferenceKey = "MerchantReferenceKey"
        case merchantDisplayName = "MerchantDisplayName"
        case merchantIconURL = "MerchantIconUrl"
        case merchantContactNumber = "MerchantContactNumber"
        case merchantEmail = "MerchantEmail"
        case redeemInstruction = "RedeemInstruction"
        case createDate = "CreateDate"
        case sharedCustomerIconURL = "SharedCustomerIconUrl"
        case dealTypeID = "DealTypeId"
        case locations = "Locations"
        case dealTypeCode = "DealTypeCode"
        case deliveryTypeCode = "DeliveryTypeCode"
        case deliveryDetails = "DeliveryDetails"
        case paymentDetails = "PaymentDetails"
        case transactionID = "TransactionId"
    }
}

struct DeliveryDetails: Codable {
}

struct Location: Codable {
    let displayName, address, contactNumber: String
    let latitude, longitude: Int

    enum CodingKeys: String, CodingKey {
        case displayName = "DisplayName"
        case address = "Address"
        case contactNumber = "ContactNumber"
        case latitude = "Latitude"
        case longitude = "Longitude"
    }
}

struct PaymentDetails: Codable {
    let paymentTypeID: Int
    let paymentTypeName, paymentTypeCode: String
    let itemAmount, deliveryCharges, totalAmount, itemCount: Int

    enum CodingKeys: String, CodingKey {
        case paymentTypeID = "PaymentTypeId"
        case paymentTypeName = "PaymentTypeName"
        case paymentTypeCode = "PaymentTypeCode"
        case itemAmount = "ItemAmount"
        case deliveryCharges = "DeliveryCharges"
        case totalAmount = "TotalAmount"
        case itemCount = "ItemCount"
    }
}
