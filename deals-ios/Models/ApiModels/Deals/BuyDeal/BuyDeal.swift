import Foundation

struct BuyDealInitResponse: Codable {
    let referenceID, referenceKey: String
    let amount: Int
    let fee: Int
    let paymentSource: String
    let totalAmount, deliveryCharge: Int
    let balance: Double

    enum CodingKeys: String, CodingKey {
        case referenceID = "ReferenceId"
        case referenceKey = "ReferenceKey"
        case amount = "Amount"
        case fee = "Fee"
        case paymentSource = "PaymentSource"
        case totalAmount = "TotalAmount"
        case balance = "Balance"
        case deliveryCharge = "DeliveryCharge"
    }
}

struct BuyPointInitResponse: Codable {
    let amount, charge, totalAmount: Int
    let paymentMode, paymentReference, paystackKey, emailAddress: String
    let currency, flutterwaveKey: String

    enum CodingKeys: String, CodingKey {
        case amount = "Amount"
        case charge = "Charge"
        case totalAmount = "TotalAmount"
        case paymentMode = "PaymentMode"
        case paymentReference = "PaymentReference"
        case paystackKey = "PaystackKey"
        case emailAddress = "EmailAddress"
        case currency = "Currency"
        case flutterwaveKey = "FlutterwaveKey"
    }
}

struct BuyPointConfirmResponse: Codable {
    let amount, charge, totalAmount, referenceID: Int
    let paymentReference, transactionDate, statusName, statusCode: String
    let balance: Balance?

    enum CodingKeys: String, CodingKey {
        case amount = "Amount"
        case charge = "Charge"
        case totalAmount = "TotalAmount"
        case referenceID = "ReferenceId"
        case paymentReference = "PaymentReference"
        case transactionDate = "TransactionDate"
        case statusName = "StatusName"
        case statusCode = "StatusCode"
        case balance = "Balance"
    }
}

struct Balance: Codable {
    let systemDate: Date
    let credit, debit, balance, balanceValidity: Int
    let invoiceAmount: Int
    let accountClass: String
    let accountClassLimit, pendingReward, tucPlusCredit, tucPlusDebit: Int
    let tucPlusBalance, tucPlusBalanceValidity: Int

    enum CodingKeys: String, CodingKey {
        case systemDate = "SystemDate"
        case credit = "Credit"
        case debit = "Debit"
        case balance = "Balance"
        case balanceValidity = "BalanceValidity"
        case invoiceAmount = "InvoiceAmount"
        case accountClass = "AccountClass"
        case accountClassLimit = "AccountClassLimit"
        case pendingReward = "PendingReward"
        case tucPlusCredit = "TucPlusCredit"
        case tucPlusDebit = "TucPlusDebit"
        case tucPlusBalance = "TucPlusBalance"
        case tucPlusBalanceValidity = "TucPlusBalanceValidity"
    }
}


struct BuyDealConfirmResponse: Codable {
    let referenceID: Int
    let referenceKey: String

    enum CodingKeys: String, CodingKey {
        case referenceID = "ReferenceId"
        case referenceKey = "ReferenceKey"
    }
}
