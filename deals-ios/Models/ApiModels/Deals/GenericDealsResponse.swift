import Foundation

struct GenericDealsResponse<T: Codable> : Codable {
    let offset: Int
    let limit: Int
    let totalRecords: Int
    let data: T
    
    private enum CodingKeys: String, CodingKey {
        case offset = "Offset"
        case limit = "Limit"
        case totalRecords = "TotalRecords"
        case data = "Data"
    }
}
