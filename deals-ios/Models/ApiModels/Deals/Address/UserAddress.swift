import Foundation

struct UserAddress: Codable {
    let referenceID: Int
    let referenceKey: String
    let locationTypeID: Int
    let locationTypeName, displayName, name, contactNumber: String
    let emailAddress, addressLine1: String
    let cityID: Int
    let cityKey, cityName: String
    let stateID: Int
    let stateKey, stateName: String
    let countryID: Int
    let countryKey, countryName: String
    let latitude, longitude: Int
    let createDate: String
    let createdByID: Int
    let createdByDisplayName: String

    enum CodingKeys: String, CodingKey {
        case referenceID = "ReferenceId"
        case referenceKey = "ReferenceKey"
        case locationTypeID = "LocationTypeId"
        case locationTypeName = "LocationTypeName"
        case displayName = "DisplayName"
        case name = "Name"
        case contactNumber = "ContactNumber"
        case emailAddress = "EmailAddress"
        case addressLine1 = "AddressLine1"
        case cityID = "CityId"
        case cityKey = "CityKey"
        case cityName = "CityName"
        case stateID = "StateId"
        case stateKey = "StateKey"
        case stateName = "StateName"
        case countryID = "CountryId"
        case countryKey = "CountryKey"
        case countryName = "CountryName"
        case latitude = "Latitude"
        case longitude = "Longitude"
        case createDate = "CreateDate"
        case createdByID = "CreatedById"
        case createdByDisplayName = "CreatedByDisplayName"
    }
}
