import Foundation

struct MerchantCategory: Codable {
    let categoryId: Int
    let merchantId: Int
    
    private enum CodingKeys: String, CodingKey {
        case categoryId = "CategoryId"
        case merchantId = "MerchantId"
    }
}
