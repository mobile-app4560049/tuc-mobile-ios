import Foundation


//let referenceID: Int
//    let referenceKey, title, categoryKey, categoryName: String
//    let merchantID: Int
//    let merchantIconURL: String
//    let merchantKey, merchantName: String
//    let imageURL: String
//    let startDate, endDate, createDate: String
//    let actualPrice, sellingPrice, storeCount: Int
//    let address, cityAreaName, cityName, stateName: String
//    let isFlashDeal: Bool
//    let views, purchase: Int
//    let isSoldout, isBookmarked: Bool
//    let dealTypeCode, deliveryTypeCode, slug: String

struct Deal: Codable {
    let referenceID: Int
    let referenceKey, title, categoryKey, categoryName: String
    let merchantID: Int
    let merchantIconURL: String
    let merchantKey, merchantName: String
    let imageURL: String
    let startDate, endDate, createDate: String
    let actualPrice, sellingPrice, saving, discountPercentage: Int
    let address, cityAreaName, cityName, stateName: String?
    let storeCount: Int
    let isFlashDeal: Bool
    let views, purchase: Int
    let isSoldout, isBookmarked: Bool
    let subCategoryKey, subCategoryName, dealTypeCode, deliveryTypeCode: String?
    let slug: String
    let totalReviews: Int
    let dealCodeValidityTypeCode, dealCodeValidityTypeName, dealCodeValidityEndDate: String?
    let dealCodeValidityDays, dealCodeValidityTypeID: Int?

    private enum CodingKeys: String, CodingKey {
        case referenceID = "ReferenceId"
        case referenceKey = "ReferenceKey"
        case title = "Title"
        case categoryKey = "CategoryKey"
        case categoryName = "CategoryName"
        case merchantID = "MerchantId"
        case merchantIconURL = "MerchantIconUrl"
        case merchantKey = "MerchantKey"
        case merchantName = "MerchantName"
        case imageURL = "ImageUrl"
        case startDate = "StartDate"
        case endDate = "EndDate"
        case createDate = "CreateDate"
        case actualPrice = "ActualPrice"
        case sellingPrice = "SellingPrice"
        case saving = "Saving"
        case discountPercentage = "DiscountPercentage"
        case address = "Address"
        case cityAreaName = "CityAreaName"
        case cityName = "CityName"
        case stateName = "StateName"
        case storeCount = "StoreCount"
        case isFlashDeal = "IsFlashDeal"
        case views = "Views"
        case purchase = "Purchase"
        case isSoldout = "IsSoldout"
        case isBookmarked = "IsBookmarked"
        case subCategoryKey = "SubCategoryKey"
        case subCategoryName = "SubCategoryName"
        case dealTypeCode = "DealTypeCode"
        case deliveryTypeCode = "DeliveryTypeCode"
        case slug = "Slug"
        case totalReviews = "TotalReviews"
        case dealCodeValidityTypeID = "DealCodeValidityTypeId"
        case dealCodeValidityTypeCode = "DealCodeValidityTypeCode"
        case dealCodeValidityTypeName = "DealCodeValidityTypeName"
        case dealCodeValidityEndDate = "DealCodeValidityEndDate"
        case dealCodeValidityDays = "DealCodeValidityDays"
    }
}
