import Foundation

struct Merchant: Codable {
    let referenceId: Int
    let displayName: String
    let name: String
    let contactNumber: String
    let emailAddress: String
    let iconUrl: String
    let rewardPercentage: Double
    
    private enum CodingKeys: String, CodingKey {
        case referenceId = "ReferenceId"
        case displayName = "DisplayName"
        case name = "Name"
        case contactNumber = "ContactNumber"
        case emailAddress = "EmailAddress"
        case iconUrl = "IconUrl"
        case rewardPercentage = "RewardPercentage"
    }
}
