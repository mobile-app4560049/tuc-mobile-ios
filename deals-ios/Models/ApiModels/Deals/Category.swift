import Foundation

struct Category: Codable {
    let iconUrl: String
    let referenceId: Int
    let name: String
    
    private enum CodingKeys: String, CodingKey {
        case iconUrl = "IconUrl"
        case referenceId = "ReferenceId"
        case name = "Name"
    }
}
