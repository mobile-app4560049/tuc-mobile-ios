import Foundation

struct User: Codable {
    let key: String
    let isNewAccount: Bool
    let accountKey: String
    let accountId: Int
    let loginTime: String
    var displayName: String
    let emailAddress: String
    var name: String
    var iconUrl: String
    let isMobileNumberVerified: Bool
    let requestToken: String
    var referralCode: String
    let createdAt: String
    var country: Country
    
    private enum CodingKeys: String, CodingKey {
        case key = "Key"
        case isNewAccount = "IsNewAccount"
        case accountKey = "AccountKey"
        case accountId = "AccountId"
        case loginTime = "LoginTime"
        case displayName = "DisplayName"
        case emailAddress = "EmailAddress"
        case name = "Name"
        case iconUrl = "IconUrl"
        case isMobileNumberVerified = "IsMobileNumberVerified"
        case requestToken = "RequestToken"
        case referralCode = "ReferralCode"
        case createdAt = "CreatedAt"
        case country = "Country"
    }
}

struct Country: Codable {
    let name: String
    let isd: String
    let iso: String
    let symbol: String
    
    private enum CodingKeys: String, CodingKey {
        case name = "Name"
        case isd = "Isd"
        case iso = "Iso"
        case symbol = "Symbol"
    }
}

struct UserVerificationResponse: Codable {
    let requestToken: String
    let isNewAccount: Bool
    let mobileNumber: String
   
    private enum CodingKeys: String, CodingKey {
        case requestToken = "RequestToken"
        case isNewAccount = "IsNewAccount"
        case mobileNumber = "MobileNumber"
    }
}

struct PinModel: Codable {
    let newPin : String
    
    private enum CodingKeys: String, CodingKey {
        case newPin = "NewPin"
    }
}
