import Foundation


struct ApiVersion {
    static let v1 = "v1"
    static let v2 = "v2"
    static let v3 = "v3"
}

struct ApiService {
    static let account = "account"
    static let maddeals = "maddeals"
    static let customer = "cust"
    static let deals = "deals"
}

struct ApiSubService {
    static let register = "register"
    static let account = "account"
    static let deals = "deals"
    static let address = "address"
    static let category = "cat"
    static let system = "sys"
    static let operation = "op"
    static let transaction = "trans"
}
