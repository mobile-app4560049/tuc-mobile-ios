import Foundation

/// Model for API error when requests fail
/// It contains message that can be displayed to the user using the showError(message: String) method
struct ApiError: Error, Codable {
    let status: String
    let message: String
    let responseCode: String
    let mode: String
    
    private enum CodingKeys: String, CodingKey {
        case status = "Status"
        case message = "Message"
        case responseCode = "ResponseCode"
        case mode = "Mode"
    }
}

/// Fallback messages for API error when requests fail
/// It contains message that can be displayed to the user using the showError(message: String) method
extension ApiError {
  static func fallBackErrorMessage(from error: String) -> String {
    let error = error.lowercased()
    if error.contains("request timed out") {
      return "Request timed out, kindly check your internet connection and try again."
    }else if error.contains("The Internet connection appears to be offline".lowercased()) {
      return "Couldn’t connect, Kindly check your internet connection and try again."
    }else if error.contains("Urlsessiontask Failed With Error".lowercased()) {
      return "Kindly check your internet connection and try again."
    }else if error.contains("Response status code was unacceptable".lowercased()) {
      return "Kindly check your internet connection and try again."
    }else {
      return "Unknown error, please try again later"
    }
  }
}
