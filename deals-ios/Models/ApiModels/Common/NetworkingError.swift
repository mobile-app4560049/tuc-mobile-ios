import Foundation

public struct NetworkingError: Error {
    let httpResponse: HTTPURLResponse?
    let data: Any?
    let baseError: Error?
}
