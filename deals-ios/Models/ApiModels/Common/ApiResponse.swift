import Foundation

struct Base64Response: Codable {
    let zx : String
}

struct ApiResponse<T: Codable> : Codable {
    let status: String
    let message: String
    let result: T?
    let responseCode: String
    let mode: String
    
    private enum CodingKeys: String, CodingKey {
        case status = "Status"
        case message = "Message"
        case result = "Result"
        case responseCode = "ResponseCode"
        case mode = "Mode"
    }
}

struct EmptyResponse: Codable {
    let status: String
    let message: String
    let responseCode: String
    let mode: String
    
    private enum CodingKeys: String, CodingKey {
        case status = "Status"
        case message = "Message"
        case responseCode = "ResponseCode"
        case mode = "Mode"
    }
}
