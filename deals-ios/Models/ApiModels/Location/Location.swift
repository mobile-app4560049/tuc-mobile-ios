import Foundation

struct State: Codable {
    let referenceID: Int
    let referenceKey: String
    let name: String
    let systemName: String
    
    private enum CodingKeys: String, CodingKey {
        case referenceID = "ReferenceId"
        case referenceKey = "ReferenceKey"
        case name = "Name"
        case systemName = "SystemName"
    }
}

struct City: Codable {
    let referenceID: Int
    let referenceKey: String
    let name: String
    let systemName: String
    
    private enum CodingKeys: String, CodingKey {
        case referenceID = "ReferenceId"
        case referenceKey = "ReferenceKey"
        case name = "Name"
        case systemName = "SystemName"
    }
}


