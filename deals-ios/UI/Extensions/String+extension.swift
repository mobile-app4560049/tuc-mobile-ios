import Foundation

extension StringProtocol {
    var firstUppercased: String { prefix(1).uppercased() + dropFirst() }
    var firstCapitalized: String { prefix(1).capitalized + dropFirst() }
}
extension String {
    func updateCurrencySymbol(currency: String) -> String {
        var tempAmount = self
        tempAmount.removeFirst()
        switch currency.lowercased() {
       
        case "usd":
          tempAmount = "$\(tempAmount)"
        case "eur":
          tempAmount = "€\(tempAmount)"
        case "ngn":
          tempAmount = "₦\(tempAmount)"
        default:
          break
        }
        return tempAmount
      }
    
    func encodeToBase64() -> String {
        let utf8 = self.data(using: .utf8)
        if let base64Encoded = utf8?.base64EncodedString() {
            return "\(base64Encoded)"
        }
        return ""
    }
    


        func urlSafeBase64Decoded() -> String? {
            var st = self
                .replacingOccurrences(of: "_", with: "/")
                .replacingOccurrences(of: "-", with: "+")
            let remainder = self.count % 4
            if remainder > 0 {
                st = self.padding(toLength: self.count + 4 - remainder,
                                  withPad: "=",
                                  startingAt: 0)
            }
            guard let d = Data(base64Encoded: st, options: .ignoreUnknownCharacters) else{
                return nil
            }
            return String(data: d, encoding: .utf8)
        }
    

}
