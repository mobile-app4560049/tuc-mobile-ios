import UIKit

extension UIApplication {
    class func tryURL(urls: [String]) {
        let application = UIApplication.shared
        for url in urls {
            guard let openUrl = URL(string: url) else {return}
            if application.canOpenURL(openUrl) {
                application.open(openUrl)
                return
            }
        }
    }
}
