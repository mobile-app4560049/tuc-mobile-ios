import UIKit

extension UIImageView {
    
    func load(url: String) {
        let indicator = UIActivityIndicatorView(style: .medium)
        indicator.color = R.color.brightPurple()!
        indicator.translatesAutoresizingMaskIntoConstraints = false
        indicator.hidesWhenStopped = true
        addSubview(indicator)
        NSLayoutConstraint.activate([
            indicator.centerXAnchor.constraint(equalTo: centerXAnchor),
            indicator.centerYAnchor.constraint(equalTo: centerYAnchor),
        ])
        DispatchQueue.main.async {
            indicator.startAnimating()
        }
        
        DispatchQueue.global().async { [weak self] in
            guard let imageUrl = URL(string: url) else {
                indicator.stopAnimating()
                return
            }
            
            if let data = try? Data(contentsOf: imageUrl) {
                if let image = UIImage(data: data) {
                    DispatchQueue.main.async {
                        indicator.stopAnimating()
                        self?.image = image
                    }
                } else {DispatchQueue.main.async {
                    indicator.stopAnimating()
                }}
            } else{DispatchQueue.main.async {
                indicator.stopAnimating()
            }}
        }
        
        
    }

    
    func generateQRCode(uniqueId: String) {
        let data = uniqueId.data(using: String.Encoding.ascii)
        
        guard let qrFilter = CIFilter(name: "CIQRCodeGenerator") else {return}
        qrFilter.setValue(data, forKey: "inputMessage")
        guard let qrCodeImage = qrFilter.outputImage else {return}
        
        let transform = CGAffineTransform(scaleX: 10, y: 10)
        let scaledImage = qrCodeImage.transformed(by: transform)
        
        let context = CIContext()
        guard let cgImage = context.createCGImage(scaledImage, from: scaledImage.extent) else {return}
        let uiImage = UIImage(cgImage: cgImage)
        self.image = uiImage
    }
    
}
