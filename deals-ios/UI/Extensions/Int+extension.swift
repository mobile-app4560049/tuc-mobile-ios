//
//  Int+extension.swift
//  deals-ios
//
//  Created by ThankUCash Tech02 on 3/30/23.
//

import Foundation

extension Int {
    func currencify(currency: String = "NGN") -> String {
      var returnString:String!
      let floatString = Float(self)
      let currencyFormatter = NumberFormatter()
      currencyFormatter.usesGroupingSeparator = true
      currencyFormatter.numberStyle = .currency
      currencyFormatter.locale = Locale(identifier: "en_NG")
      if let priceString = currencyFormatter.string(from: NSNumber(value: floatString)){
        returnString = priceString.updateCurrencySymbol(currency: currency)
      } else {
        returnString = "0.00"
      }
      return returnString
    }
}
