import UIKit

extension UIView {
    
    @IBInspectable var cornerRadius: CGFloat {
        get {
            return layer.cornerRadius
        }
        set {
            layer.cornerRadius = newValue
            layer.masksToBounds = newValue > 0
        }
    }
    
    @IBInspectable var borderWidth: CGFloat {
        get {
            return layer.borderWidth
        }
        set {
            layer.borderWidth = newValue
        }
    }
    
    @IBInspectable var borderColor: UIColor? {
        get {
            return UIColor(cgColor: layer.borderColor!)
        }
        set {
            layer.borderColor = newValue?.cgColor
        }
    }
    
    func setX(x: CGFloat) {
        var frame = self.frame
        frame.origin.x = x
        self.frame = frame
    }
    
    func setY(y: CGFloat) {
        var frame = self.frame
        frame.origin.y = y
        self.frame = frame
    }
    
    func setWidth(width :CGFloat) {
        var frame = self.frame
        frame.size.width = width
        self.frame = frame
    }
    
    func setHeight(height: CGFloat) {
        var frame = self.frame
        frame.size.height = height
        self.frame = frame
    }
    
      // In order to create computed properties for extensions, we need a key to
      // store and access the stored property
      fileprivate struct AssociatedObjectKeys {
        static var tapGestureRecognizer = "MediaViewerAssociatedObjectKey_mediaViewer"
      }
    
    // Creates an action
      fileprivate typealias Action = (() -> Void)?
    
    
      // Set our computed property type to a closure
      fileprivate var tapGestureRecognizerAction: Action? {
        set {
          if let newValue = newValue {
            // Computed properties get stored as associated objects
            objc_setAssociatedObject(self, &AssociatedObjectKeys.tapGestureRecognizer, newValue, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN)
          }
        }
        get {
          let tapGestureRecognizerActionInstance = objc_getAssociatedObject(self, &AssociatedObjectKeys.tapGestureRecognizer) as? Action
          return tapGestureRecognizerActionInstance
        }
      }
    
      // This is the meat of the sauce, here we create the tap gesture recognizer and
      // store the closure the user passed to us in the associated object we declared above
      public func addTapGestureRecognizer(action: (() -> Void)?) {
        self.isUserInteractionEnabled = true
        self.tapGestureRecognizerAction = action
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(handleTapGesture))
        self.addGestureRecognizer(tapGestureRecognizer)
      }
    
      // Every time the user taps on the UIView, this function gets called,
      // which triggers the closure we stored
      @objc fileprivate func handleTapGesture(sender: UITapGestureRecognizer) {
        if let action = self.tapGestureRecognizerAction {action?()}
      }
    
    func dropShadow(opacity:Float) {
        self.layer.masksToBounds = false
        self.layer.shadowColor = R.color.black()!.cgColor
        self.layer.shadowOffset =  CGSize.zero
        self.layer.shadowOpacity = opacity
        self.layer.shadowRadius = 4
      }
}
