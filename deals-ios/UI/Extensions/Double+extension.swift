import Foundation

extension Double {
  func withCommas() -> String? {
    let formatter = NumberFormatter()
    formatter.numberStyle = .decimal
    return formatter.string(from: NSNumber(value: self))
  }
  var to2Decimal: Double {
    let doubleStr = String(format: "%.2f", self)
    return Double(doubleStr) ?? 0.0
  }
  func currencify(currency: String = "NGN") -> String {
    var returnString:String!
    let floatString = Float(self)
    let currencyFormatter = NumberFormatter()
    currencyFormatter.usesGroupingSeparator = true
    currencyFormatter.numberStyle = .currency
    currencyFormatter.locale = Locale(identifier: "en_NG")
    if let priceString = currencyFormatter.string(from: NSNumber(value: floatString)){
      returnString = priceString.updateCurrencySymbol(currency: currency)
    } else {
      returnString = "0.00"
    }
    return returnString
  }
}
