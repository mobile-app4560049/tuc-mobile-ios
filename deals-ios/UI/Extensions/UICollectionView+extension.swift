import UIKit

extension UICollectionView {
    func setEmptyMessage(_ message: String) {
        let image = UIImageView(image: R.image.dealsNotFound())
        image.contentMode = .scaleAspectFit
        image.setHeight(height: 195)
        image.setWidth(width: 250)
        image.translatesAutoresizingMaskIntoConstraints = false
        let titleLabel = UILabel()
        titleLabel.text = "Oops"
        titleLabel.numberOfLines = 0
        titleLabel.font = R.font.brFirmaSemiBold(size: 50)
        titleLabel.textColor = R.color.black()
        let messageLabel = UILabel()
        messageLabel.text = message
        messageLabel.numberOfLines = 2
        messageLabel.font = R.font.brFirmaMedium(size: 12)
        messageLabel.textColor = R.color.textfieldTitleColor()
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineHeightMultiple = 1.17
        paragraphStyle.alignment = .center
        messageLabel.attributedText = NSMutableAttributedString(string: message, attributes: [NSAttributedString.Key.paragraphStyle: paragraphStyle])
        let stack = UIStackView(arrangedSubviews: [image, titleLabel, messageLabel])
        stack.axis = .vertical
        stack.alignment = .center
        stack.translatesAutoresizingMaskIntoConstraints = false
        stack.spacing = 20
        let view = UIView()
        view.addSubview(stack)
        stack.topAnchor.constraint(equalTo: view.topAnchor, constant: 50).isActive = true
        stack.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 20).isActive = true
        stack.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -20).isActive = true
        self.backgroundView = view
    }
    
    func setEmptyString(_ message: String) {
        let messageLabel = UILabel()
        messageLabel.text = message
        messageLabel.numberOfLines = 2
        messageLabel.font = R.font.brFirmaMedium(size: 12)
        messageLabel.textColor = R.color.textfieldTitleColor()
        messageLabel.translatesAutoresizingMaskIntoConstraints = false
        let view = UIView()
        view.addSubview(messageLabel)
        messageLabel.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        messageLabel.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        self.backgroundView = view
    }
    
}
