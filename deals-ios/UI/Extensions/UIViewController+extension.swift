import UIKit
import MaterialComponents
import Lottie

class DealsActivityIndicator: UIView {
    
    static let shared = DealsActivityIndicator()
    private let translucentView: UIView
    private var animationView: AnimationView
    private let animationFileName = "loader"
    
    //options
    private var translucentBg  = UIColor.black.withAlphaComponent(0.8)
    
    override init(frame: CGRect) {
        self.translucentView = UIView()
        self.animationView = AnimationView()
        super.init(frame: frame)
        self.setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        self.translucentView = UIView()
        self.animationView = AnimationView()
        super.init(coder: aDecoder)
        self.setup()
    }
    
    func setup() {
        self.addSubview(translucentView)
    }
    
    override func didMoveToSuperview() {
        super.didMoveToSuperview()
        if let superview = self.superview {
            self.animationView.removeFromSuperview()
            
            self.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
            translucentView.frame = self.bounds
            translucentView.backgroundColor = self.translucentBg
            layer.masksToBounds = true
            
            self.animationView = AnimationView(name: self.animationFileName)
            self.animationView.frame = CGRect(x: 0, y: 0, width: self.translucentView.frame.width/2, height: self.translucentView.frame.size.height/2)
            self.animationView.contentMode = .scaleAspectFit
            self.animationView.center = superview.center
            
            self.translucentView.addSubview(animationView)
            self.animationView.loopMode = LottieLoopMode.loop
            self.animationView.play()
            
            self.hide()
        }
    }
    
    func show() {
        self.isHidden = false
    }
    
    func hide() {
        self.isHidden = true
    }
}

extension UIViewController {
    
    private static var isShowingIndicator = false
    
    func showError(message: String) {
        let snackMessage = MDCSnackbarMessage(text: message)
        MDCSnackbarManager.messageTextColor = R.color.white()!
        MDCSnackbarManager.messageFont = R.font.brFirmaSemiBold(size: 14)
        MDCSnackbarManager.snackbarMessageViewBackgroundColor = R.color.red()!
        MDCSnackbarManager.show(snackMessage)
    }
    
    func showInfo(message: String) {
        let snackMessage = MDCSnackbarMessage(text: message)
        MDCSnackbarManager.messageTextColor = R.color.white()!
        MDCSnackbarManager.messageFont = R.font.brFirmaSemiBold(size: 14)
        MDCSnackbarManager.snackbarMessageViewBackgroundColor = R.color.yellow()!
        MDCSnackbarManager.show(snackMessage)
    }
    
    func showSuccess(message: String) {
        let snackMessage = MDCSnackbarMessage(text: message)
        MDCSnackbarManager.messageTextColor = R.color.white()!
        MDCSnackbarManager.messageFont = R.font.brFirmaSemiBold(size: 14)
        MDCSnackbarManager.snackbarMessageViewBackgroundColor = R.color.green()!
        MDCSnackbarManager.show(snackMessage)
    }
    
    func showProgressIndicator(){
        DispatchQueue.main.async {
            guard let topView = UIApplication.shared.keyWindow else {return}
            let activityIndicator = DealsActivityIndicator.shared
            topView.addSubview(activityIndicator)
            
            activityIndicator.snp.makeConstraints{make in
                make.edges.equalTo(topView)
            }
            
            activityIndicator.show()
        }
    }
    
    func dismissProgressIndicator(){
        DispatchQueue.main.async {
            let activityIndicator = DealsActivityIndicator.shared
            activityIndicator.hide()
        }
    }
    
}
