import Foundation
import RxSwift
import RxCocoa

class BaseViewModel {
    
    let error = PublishSubject<String>()
    let progress = BehaviorRelay<Bool>(value: false)
    let disposeBag = DisposeBag()
}
