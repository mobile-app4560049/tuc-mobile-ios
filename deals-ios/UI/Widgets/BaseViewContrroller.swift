import UIKit
import RxSwift

class BaseViewController: UIViewController {
    
    internal var disposeBag = DisposeBag()
    
    var hasBack: Bool = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = R.color.white()!
        setFont()
        if hasBack {
            setupBackButton()
        }
        hideKeyboardWhenTappedAround()
        self.extendedLayoutIncludesOpaqueBars = true
        self.navigationController?.isNavigationBarHidden = false
    }
    
    func setFont() {
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.font: R.font.brFirmaSemiBold(size: 16)!]
    }
    
    func setupBackButton(){
        let button = UIButton()
        button.setImage(R.image.backButton()!, for: .normal)
        button.addTapGestureRecognizer {
            self.navigationController?.popViewController(animated: true)
        }
        button.frame = CGRect(x: 0, y: 0, width: 10, height: 20)
        let barButton = UIBarButtonItem(customView: button)
        self.navigationItem.leftBarButtonItem = barButton
        self.navigationController?.navigationBar.backgroundColor = R.color.white()!
    }
    
    func removeBackButton(){
        self.navigationItem.leftBarButtonItem = UIBarButtonItem()
    }
    
    func hideKeyboardWhenTappedAround(){
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard(){
        view.endEditing(true)
    }
}


