import UIKit
import RaveSDK
import PaystackCheckout
import RxSwift

enum PaymentGateway {
    case wallet, paystack, flutterWave
    
    var literal: String {
        switch self {
        case .wallet:
            return "wallet"
        case .flutterWave:
            return "flutterwave"
        case .paystack:
            return "paystack"
        }
    }
}
enum PaymentType { case fundWallet, airTime, data, purchaseDeal, utility }

class CheckOutViewController: BaseViewController {
    
    @IBOutlet weak var walletView: UIView!
    @IBOutlet weak var fltWaveView: UIView!
    @IBOutlet weak var paystackView: UIView!
        
    @IBOutlet weak var walletRadio: UIImageView!
    @IBOutlet weak var walletImage: UIImageView!
    
    @IBOutlet weak var fltRadio: UIImageView!
    @IBOutlet weak var fltImage: UIImageView!
    
    @IBOutlet weak var paystackRadio: UIImageView!
    @IBOutlet weak var paystackImage: UIImageView!
    
    @IBOutlet weak var amountLabel: UILabel!
    @IBOutlet weak var btn: UIView!
    
    var amount: Double!
    
    var dealResponse: BuyDealInitResponse?
    var paymentGateway: PaymentGateway?
    var paymentType: PaymentType!
    var checkoutDetails: CheckoutDetails?
    
    private let viewModel: BuyDealViewModel = BuyDealViewModel(datasource: BuyDealRepository())

    override func viewDidLoad() {
        super.viewDidLoad()
        addGestures()
        bindBuyDealVm()
        walletView.isHidden = paymentType == .fundWallet
        amountLabel.text = amount.currencify()
    }
    
    func addGestures() {
        walletView.addTapGestureRecognizer {
            self.paymentGateway = .wallet
            self.setColors(wallet: true)
            
        }
        fltWaveView.addTapGestureRecognizer {
            self.paymentGateway = .flutterWave
            self.setColors(fltWave: true)
        }
        paystackView.addTapGestureRecognizer {
            self.paymentGateway = .paystack
            self.setColors(payStack: true)
        }
        btn.addTapGestureRecognizer { [weak self] in
            guard let self = self else {return}
            switch self.paymentGateway {
            case .flutterWave:
                let chargeAmount = self.amount.rounded()
                self.payWithFlW(email: "test@mail.com", phoneNumber: "08101234567", firstName: "Bash", lastName: "Mike", amount: "\(chargeAmount)")
            case .paystack:
                switch self.checkoutDetails?.deal.dealTypeCode {
                case DealTypeCode.service.literal:
                    if let amount = self.dealResponse?.amount, let deal = self.checkoutDetails?.deal{
                            self.viewModel.buyPointInitialize(deal: deal, amount: "\(amount)", paymentMode: PaymentGateway.paystack.literal)
                    }
                default:
                    return
                }
            case .wallet:
                self.showInfo(message: ErrorMessages.walletUnavailable)
            default:
                self.showError(message: ErrorMessages.selectPaymentGateway)
            }
        }
    }
    
    private func bindBuyDealVm() {
        
        viewModel.error.bind { [unowned self] error in
            self.showError(message: error)
        }.disposed(by: disposeBag)
        
        viewModel.progress.bind { [unowned self] loading in
            loading ? self.showProgressIndicator() : self.dismissProgressIndicator()
        }.disposed(by: disposeBag)
        
//        viewModel.buyDealInitResp.bind { response in
//            if let initResp = response {
//                self.dealResponse = initResp
//                self.viewModel.buyPointInitialize(amount: "\(initResp.amount)", paymentMode: PaymentGateway.paystack.literal)
//            }
//        }.disposed(by: disposeBag)
        
        viewModel.buyPointInitResp.bind { response in
            if let initResp = response {
                let amount = initResp.totalAmount * 100
                self.payWithPaystack(amount: amount, email: initResp.emailAddress, reference: initResp.paymentReference)
            }
        }.disposed(by: disposeBag)
        
        viewModel.buyPointConfirmResp.bind { response in
            if let response = response, let detail = self.checkoutDetails, let initResp = self.dealResponse {
                self.viewModel.buyDealConfirm(dealInitResp: initResp, deal: detail.deal, quantity: detail.quantity, buyPointConfRes: response)
            }
        }.disposed(by: disposeBag)
        
        viewModel.buyDealConfirmResp.bind { response in
            if let response = response {
                self.viewModel.getDealCode(bbuyDealResp: response)
            }
        }.disposed(by: disposeBag)
        
        viewModel.getDealCodeResp.bind { dealCodeResp in
            if let dealCodeResp = dealCodeResp {
                PurchaseSuccessViewController.launch(self, amount: self.amount.currencify(), dealCode: dealCodeResp)
            }
        }.disposed(by: disposeBag)
        
        viewModel.successMessage.bind { [unowned self] message in
            self.showSuccess(message: message)
        }.disposed(by: disposeBag)

    }
    
    func setColors(wallet: Bool = false, fltWave: Bool = false, payStack: Bool = false){
        walletView.borderColor = wallet ? R.color.brightPurple()! : R.color.textfieldBorder()!
        walletRadio.image = wallet ? R.image.radioSelected()! : R.image.radio()!
        walletImage.image = wallet ? R.image.deviceSelected()! : R.image.device()!
        
        fltWaveView.borderColor = fltWave ? R.color.brightPurple()! : R.color.textfieldBorder()!
        fltRadio.image = fltWave ? R.image.radioSelected()! : R.image.radio()!
        fltImage.image = fltWave ? R.image.cardSelected()! : R.image.card()!
        
        paystackView.borderColor = payStack ? R.color.brightPurple()! : R.color.textfieldBorder()!
        paystackRadio.image = payStack ? R.image.radioSelected()! : R.image.radio()!
        paystackImage.image = payStack ? R.image.cardSelected()! : R.image.card()!
    }
    
}


//MARK: - Flutterwave Payment
extension CheckOutViewController: RavePayProtocol {
    func tranasctionSuccessful(flwRef: String?, responseData: [String : Any]?) {
        switch paymentType {
        case .fundWallet:
//            PurchaseSuccessViewController.launch(self, amount: amount.currencify())
            SuccessViewController.launch(self, type: .data)
        case .airTime:
            SuccessViewController.launch(self,  type: .airTime)
        case .data:
            SuccessViewController.launch(self, type: .data)
        case .purchaseDeal:
//            PurchaseSuccessViewController.launch(self, amount: amount.currencify())
            SuccessViewController.launch(self, type: .data)
        case .utility:
            SuccessViewController.launch(self, type: .utility)
        default:
//            PurchaseSuccessViewController.launch(self, amount: amount.currencify())
            SuccessViewController.launch(self, type: .data)
        }
    }
    
    func tranasctionFailed(flwRef: String?, responseData: [String : Any]?) {
        self.showError(message: ErrorMessages.flutterwaveError)
    }
    
    func onDismiss() {
        self.showInfo(message: ErrorMessages.paymentCancelled)
    }
    
    func payWithFlW(email: String, phoneNumber: String, firstName: String, lastName: String, amount: String) {
        let config = RaveConfig.sharedConfig()
        config.country = "NG" // Country Code
        config.currencyCode = "NGN" // Currency
        config.email = email // Customer's email
        config.isStaging = Configuration.environment == .dev  // Toggle this for staging and live environment
        config.phoneNumber = phoneNumber //Phone number
        config.transcationRef = "ref" // transaction ref
        config.firstName = firstName
        config.lastName = lastName
        config.meta = [["metaname":"sdk", "metavalue":"ios"]]
        
        config.publicKey = Configuration.environment.fltWKey //Public key
        config.encryptionKey = Configuration.environment.fltWEncryptKey //Encryption key
        
        
        let controller = NewRavePayViewController()
        let nav = UINavigationController(rootViewController: controller)
        controller.amount = amount
        controller.delegate = self
        self.present(nav, animated: true)
    }
    
    
}


//MARK: - Paystack Payment
extension CheckOutViewController: CheckoutProtocol {
    func onSuccess(response: TransactionResponse) {
        
        switch paymentType {
        case .fundWallet:
//            PurchaseSuccessViewController.launch(self, amount: amount.currencify())
            SuccessViewController.launch(self, type: .data)
        case .airTime:
            SuccessViewController.launch(self,  type: .airTime)
        case .data:
            SuccessViewController.launch(self, type: .data)
        case .purchaseDeal:
            guard let dealResponse = dealResponse else {return}
            viewModel.buyPointConfirm(amount: dealResponse.totalAmount, paymentReference: response.reference, refTransactionId: response.id, refStatus: "success", refMessage: "Approved", paymentMode: PaymentGateway.paystack.literal)
        case .utility:
            SuccessViewController.launch(self, type: .utility)
        default:
//            PurchaseSuccessViewController.launch(self, amount: amount.currencify())
            SuccessViewController.launch(self, type: .data)
        }
    
    }
      
      func onError(error: Error?) {
          self.showError(message: ErrorMessages.paystackError)
      }
        
      func onDimissal() {
          self.showInfo(message: ErrorMessages.paymentCancelled)
      }

    func payWithPaystack(amount: Int, email: String, reference: String?) {
        let params = TransactionParams(
            amount: amount,
            email: email,
            key: Configuration.environment.paystackKey,
            reference: reference
        )
        let checkoutVC = CheckoutViewController(params: params, delegate: self)
        present(checkoutVC, animated: true)
    }
}

//MARK: Launcher...... a payment type must be provided when launching this screen
extension CheckOutViewController {
    static func launch(_ caller: UIViewController, paymentType: PaymentType, amount:Double, dealDetails: CheckoutDetails? = nil, dealResp: BuyDealInitResponse? = nil) {
        let controller = R.storyboard.checkout.checkOutViewController()!
        controller.navigationController?.isNavigationBarHidden = false
        controller.paymentType = paymentType
        controller.amount = amount
        controller.dealResponse = dealResp
        controller.checkoutDetails = dealDetails
        caller.navigationController?.pushViewController(controller, animated: true)
    }
}
