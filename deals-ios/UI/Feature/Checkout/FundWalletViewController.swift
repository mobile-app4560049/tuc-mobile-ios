import UIKit

class FundWalletViewController: BaseViewController {

    var numberString = ""
    var decimalPressed = false
    
    @IBOutlet weak var amountField: UILabel!
    @IBOutlet weak var btn1: UILabel!
    @IBOutlet weak var btn2: UILabel!
    @IBOutlet weak var btn3: UILabel!
    @IBOutlet weak var btn4: UILabel!
    @IBOutlet weak var btn5: UILabel!
    @IBOutlet weak var btn6: UILabel!
    @IBOutlet weak var btn7: UILabel!
    @IBOutlet weak var btn8: UILabel!
    @IBOutlet weak var btn9: UILabel!
    @IBOutlet weak var btn0: UILabel!
    @IBOutlet weak var decimal: UILabel!
    @IBOutlet weak var delete: UILabel!
    @IBOutlet weak var btn: UIView!
   
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addGestures()
    }
    
    func addGestures (){
        let viewArray: [UILabel] = [btn0, btn1, btn2, btn3, btn4, btn5, btn6, btn7, btn8, btn9]
        for array in viewArray {
            array.addTapGestureRecognizer {
                if self.numberString.count < 10 {
                    self.numberString += array.text ?? ""
                    self.updateLabel()
                    self.amountField.textColor = R.color.black()!
                }
            }
        }
        
        decimal.addTapGestureRecognizer { [self] in
            if !decimalPressed {
                numberString += "."
                decimalPressed = true
            }
            updateLabel()
        }
        
        delete.addTapGestureRecognizer { [self] in
            if numberString.count > 0 {
                numberString.removeLast()
                if numberString.last == "." {
                    decimalPressed = false
                }
            }
            updateLabel()
        }
        
        btn.addTapGestureRecognizer { [weak self] in
            guard let self = self else {return}
            if Double(self.numberString) != nil {
                CheckOutViewController.launch(self, paymentType: .fundWallet, amount: Double(self.numberString)!)
            } else {
                self.showError(message: ErrorMessages.enterNumber)
            }
        }
    }
    
    func updateLabel(){
        var formattedString  = ""
        let number = Double(numberString) ?? 0
        
        let numberFormatter = NumberFormatter()
        numberFormatter.numberStyle = .decimal
        formattedString = numberFormatter.string(from: NSNumber(value: number)) ?? ""
        
        amountField.text = "₦ \(formattedString)"
        print(number)
    }

    
}

extension FundWalletViewController {
    static func launch(_ caller: UIViewController) {
        let controller = R.storyboard.checkout.fundWalletViewController()!
        caller.navigationController?.pushViewController(controller, animated: true)
    }
}
