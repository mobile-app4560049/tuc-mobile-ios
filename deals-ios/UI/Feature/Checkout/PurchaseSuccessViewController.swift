import UIKit

class PurchaseSuccessViewController: BaseViewController {

    @IBOutlet weak var history: UILabel!
    @IBOutlet weak var message: UILabel!
    @IBOutlet weak var codeLabel: UILabel!
    @IBOutlet weak var btn: UIView!
    
    var amount:String!
    var dealCode: DealCodeResponse?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        codeLabel.isHidden = dealCode == nil
        if let dealCode = dealCode?.dealTypeCode {
            codeLabel.text = "Deal Code: " + dealCode
        }
        
        message.text = "You have successfully made a payment of \(amount!)"
        history.addTapGestureRecognizer {
            HistoryViewController.launch(self, selectedHistory: .billsPayment)
        }
        btn.addTapGestureRecognizer {
            HomeTabBarController.launch()
        }

    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.removeBackButton()
    }

}

extension PurchaseSuccessViewController {
    static func launch(_ caller: UIViewController, amount: String, dealCode: DealCodeResponse) {
        let controller = R.storyboard.checkout.purchaseSuccessViewController()!
        controller.amount = amount
        controller.dealCode = dealCode
        controller.navigationController?.isNavigationBarHidden = false
        caller.navigationController?.pushViewController(controller, animated: true)
    }
}
