import UIKit

class UtilityBillsViewController: BaseViewController {

    @IBOutlet weak var segmentControl: DealsSegmentedControl!
    @IBOutlet weak var merchantCollection: UICollectionView!
    @IBOutlet weak var billsButton: UIView!
    @IBOutlet weak var btnLabbel: UILabel!
    
    @IBOutlet weak var planField: DealsInputField!
    @IBOutlet weak var amountField: DealsAmountField!
    
    
    private let viewModel = UtilitiesViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        merchantCollection.dataSource = self
        merchantCollection.delegate = self
        merchantCollection.register(UINib(nibName: R.nib.utilityCollectionViewCell.name, bundle: nil), forCellWithReuseIdentifier: R.nib.utilityCollectionViewCell.name)
        planField.inputContainer.backgroundColor = R.color.white()!
        planField.addTapGestureRecognizer {
            UtilityPlanViewController.launch(self, utilityTitle: "DStv Plans")
        }
        billsButton.addTapGestureRecognizer {[weak self] in
            guard let self = self else {return}
            if let amount = Double(self.amountField.content), amount > 0 {
                CheckOutViewController.launch(self, paymentType: .utility, amount:amount)
            } else {
                self.showError(message: ErrorMessages.selectPlan)
            }
        }
    }
    
    @IBAction func didSwitchTopUpSegment(_ sender: DealsSegmentedControl) {
        planField.contentTextField.text = ""
        amountField.contentTextField.text = ""
        switch sender.selectedSegmentIndex {
        case 0:
            btnLabbel.text = "Recharge TV"
        case 1:
            btnLabbel.text = "Buy Electricity"
        default:
            btnLabbel.text = "Buy Utility"
        }
    }

}


extension UtilityBillsViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModel.images.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: R.nib.utilityCollectionViewCell.name, for: indexPath) as! UtilityCollectionViewCell
        cell.setup(image: viewModel.images[indexPath.item])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let cells = merchantCollection.visibleCells as? [UtilityCollectionViewCell] {
            for cell in cells {
                if cell.cellView.borderColor == R.color.brightPurple()! {
                    //Changes the color of a previously selected cell back to default (white) and its text color to deep purple
                    cell.deSelectCell()
                }
            }
        }
        
        // Changes the actual selected cell background color and text color
        if let cell = collectionView.cellForItem(at: indexPath) as? UtilityCollectionViewCell {
            cell.selectCell()
        }
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 120, height: 95)
    }
    
}

extension UtilityBillsViewController {
    static func launch(_ caller: UIViewController) {
        let controller = R.storyboard.utilities.utilityBillsViewController()!
        caller.navigationController?.pushViewController(controller, animated: true)
    }
}
