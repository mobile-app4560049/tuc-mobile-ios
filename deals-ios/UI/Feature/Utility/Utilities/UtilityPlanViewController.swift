import UIKit

class UtilityPlanViewController: BaseViewController {

    @IBOutlet weak var closeImage: UIImageView!
    @IBOutlet weak var searchBar: DealsInputField!
    @IBOutlet weak var planTable: UITableView!
    @IBOutlet weak var titleLabel: UILabel!
    
    var utilityTitle: String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        titleLabel.text = utilityTitle
        planTable.delegate = self
        planTable.dataSource = self
        planTable.register(UINib(nibName: R.nib.utilityPlanTableViewCell.name, bundle: nil), forCellReuseIdentifier: R.nib.utilityPlanTableViewCell.identifier)
        
        closeImage.addTapGestureRecognizer {
            self.dismiss(animated: true)
        }
    }
    
}
extension UtilityPlanViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: R.nib.utilityPlanTableViewCell.identifier, for: indexPath) as! UtilityPlanTableViewCell
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
    
}


extension UtilityPlanViewController {
    static func launch(_ caller: UIViewController, utilityTitle: String) {
        let controller = R.storyboard.utilities.utilityPlanViewController()!
        controller.utilityTitle = utilityTitle
        controller.modalPresentationStyle = .overCurrentContext
        caller.present(controller, animated: true)
    }
}
