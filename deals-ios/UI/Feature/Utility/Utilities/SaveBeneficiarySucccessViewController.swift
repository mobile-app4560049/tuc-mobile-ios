import UIKit

class SaveBeneficiarySucccessViewController: BaseViewController {

    @IBOutlet weak var close: UIImageView!
    @IBOutlet weak var messageLabel: UILabel!

    
    override func viewDidLoad() {
        super.viewDidLoad()

        close.addTapGestureRecognizer {
            self.dismiss(animated: true) {
                HomeTabBarController.launch()
            }
        }
    }

}

extension SaveBeneficiarySucccessViewController {
    static func launch(_ caller: UIViewController) {
        let controller = R.storyboard.utilities.saveBeneficiarySucccessViewController()!
        if let sheet = controller.sheetPresentationController {
            sheet.detents = [.medium()]
            sheet.preferredCornerRadius = 20
        }
        caller.present(controller, animated: true)
    }
}

