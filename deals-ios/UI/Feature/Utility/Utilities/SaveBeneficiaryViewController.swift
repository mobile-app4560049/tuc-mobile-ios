import UIKit

class SaveBeneficiaryViewController: BaseViewController {

    @IBOutlet weak var nameField: DealsInputField!
    @IBOutlet weak var providerField: DealsInputField!
    @IBOutlet weak var numberField: DealsPhoneNumberField!
    @IBOutlet weak var btn: UIView!
    
    var parentVC: UIViewController!
    override func viewDidLoad() {
        super.viewDidLoad()
        btn.addTapGestureRecognizer {
            self.dismiss(animated: true) {
                SaveBeneficiarySucccessViewController.launch(self.parentVC)
            }
        }
    }

}

extension SaveBeneficiaryViewController {
    static func launch(_ caller: UIViewController) {
        let controller = R.storyboard.utilities.saveBeneficiaryViewController()!
        if let sheet = controller.sheetPresentationController {
            sheet.detents = [.medium()]
            sheet.preferredCornerRadius = 20
        }
        controller.parentVC = caller
        caller.present(controller, animated: true)
    }
}
