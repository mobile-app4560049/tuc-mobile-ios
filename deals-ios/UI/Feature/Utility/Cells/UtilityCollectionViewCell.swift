import UIKit

class UtilityCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var iconImage: UIImageView!
    @IBOutlet weak var cellLabel: UILabel!
    @IBOutlet weak var cellView: RaisedView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func setup(image: UIImage) {
        iconImage.image = image
    }
    
    func selectCell(){
        cellView.borderColor = R.color.brightPurple()!
        cellLabel.textColor = R.color.black()!
    }

    func deSelectCell(){
        cellView.borderColor = R.color.white()!
        cellLabel.textColor = R.color.deepPurple()!
    }
}
