import UIKit


protocol TopUpBeneficiaryDelegate {
    func selectBeneficiary(name: String, number: String)
}

class TopUpBeneficiariesViewController: BaseViewController {
    
    @IBOutlet weak var closeImage: UIImageView!
    @IBOutlet weak var beneficiaryTable: UITableView!

    var beneficiaryDelegate: TopUpBeneficiaryDelegate!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        beneficiaryTable.delegate = self
        beneficiaryTable.dataSource = self
        beneficiaryTable.register(UINib(nibName: R.nib.utilityPlanTableViewCell.name, bundle: nil), forCellReuseIdentifier: R.nib.utilityPlanTableViewCell.identifier)
        closeImage.addTapGestureRecognizer {
            self.dismiss(animated: true)
        }
    }

}

extension TopUpBeneficiariesViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: R.nib.utilityPlanTableViewCell.identifier, for: indexPath) as! UtilityPlanTableViewCell
        cell.cellOptionalLabel.isHidden = true
        cell.cellTitle.text = "Victoria 08075333212"
        cell.cellSubtitle.text = "GLOQuickCharge(Top-up)"
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.dismiss(animated: true){
            self.beneficiaryDelegate.selectBeneficiary(name: "Victoria", number: "08075333212")
        }
    }
    
}

extension TopUpBeneficiariesViewController {
    static func launch(_ caller: UIViewController, delegate: TopUpBeneficiaryDelegate) {
        let controller = R.storyboard.utilities.topUpBeneficiariesViewController()!
        controller.beneficiaryDelegate = delegate
        if let sheet = controller.sheetPresentationController {
            sheet.detents = [.medium(), .large()]
            sheet.preferredCornerRadius = 20
        }
        caller.present(controller, animated: true)
    }
}
