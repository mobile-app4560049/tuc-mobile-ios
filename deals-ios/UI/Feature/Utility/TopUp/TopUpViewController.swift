import UIKit
import ContactsUI

enum TopUpType {
    case airtime, data
    
    var stringValue: String {
        switch self {
        case .airtime:
            return "Airtime"
        case .data:
            return "Data"
        }
    }
}

class TopUpViewController: BaseViewController {
    
    @IBOutlet weak var segmentControl: DealsSegmentedControl!
    @IBOutlet weak var topupTypeCollection: UICollectionView!
    
    @IBOutlet weak var findBeneficary: UILabel!
    
    
    @IBOutlet weak var planField: DealsInputField!
    @IBOutlet weak var numberField: DealsInputField!
    @IBOutlet weak var amountField: DealsAmountField!
    
    
    @IBOutlet weak var amountStack: UIStackView!
    
    @IBOutlet weak var oneHundred: RaisedView!
    @IBOutlet weak var twoHundred: RaisedView!
    @IBOutlet weak var fiveHundred: RaisedView!
    @IBOutlet weak var oneThousand: RaisedView!
    @IBOutlet weak var twoThousand: RaisedView!
    @IBOutlet weak var fiveThousand: RaisedView!
    
    @IBOutlet weak var topUpButton: UIView!
    @IBOutlet weak var btnLabel: UILabel!
    
    var topUpType: TopUpType!
    let viewModel = TopUpViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = topUpType.stringValue
        btnLabel.text = topUpType == .airtime ? "Buy Airtime" : "Buy Data"
        segmentControl.selectedSegmentIndex = topUpType == .airtime ? 0 : 1
        
        topupTypeCollection.dataSource = self
        topupTypeCollection.delegate = self
        topupTypeCollection.register(UINib(nibName: R.nib.utilityCollectionViewCell.name, bundle: nil), forCellWithReuseIdentifier: R.nib.utilityCollectionViewCell.name)
        
        numberField.iconImageView.addTapGestureRecognizer {
            let contactPicker = CNContactPickerViewController()
            contactPicker.delegate = self
            contactPicker.displayedPropertyKeys =
            [CNContactGivenNameKey
             , CNContactPhoneNumbersKey]
            self.present(contactPicker, animated: true, completion: nil)
        }
        amountStack.isHidden = topUpType == .data
        planField.isHidden = topUpType == .airtime
        planField.inputContainer.backgroundColor = R.color.white()!
        numberField.setKeyBoardType(UIKeyboardType.numberPad)
        addGestures()
    }
    
    func addGestures(){
        oneHundred.addTapGestureRecognizer {
            self.amountField.contentTextField.text = 100.currencify()
            self.amountField.content = 100.currencify()
            self.selectAmount(oneHundred: true)
        }
        twoHundred.addTapGestureRecognizer {
            self.amountField.contentTextField.text = 200.currencify()
            self.amountField.content = 200.currencify()
            self.selectAmount(twoHundred: true)
        }
        fiveHundred.addTapGestureRecognizer {
            self.amountField.contentTextField.text = 500.currencify()
            self.amountField.content = 500.currencify()
            self.selectAmount(fiveHundred: true)
        }
        oneThousand.addTapGestureRecognizer {
            self.amountField.contentTextField.text = 1000.currencify()
            self.amountField.content = 1000.currencify()
            self.selectAmount(oneThousand: true)
        }
        twoThousand.addTapGestureRecognizer {
            self.amountField.content = 2000.currencify()
            self.amountField.contentTextField.text = 2000.currencify()
            self.selectAmount(twoThousand: true)
        }
        fiveThousand.addTapGestureRecognizer {
            self.amountField.contentTextField.text = 5000.currencify()
            self.amountField.content = 5000.currencify()
            self.selectAmount(fiveThousand: true)
        }
        planField.addTapGestureRecognizer {
            UtilityPlanViewController.launch(self, utilityTitle: "Data Plans")
        }
        findBeneficary.addTapGestureRecognizer {
            TopUpBeneficiariesViewController.launch(self, delegate: self)
        }
    }
    
    func selectAmount(oneHundred: Bool = false, twoHundred: Bool = false, fiveHundred: Bool = false, oneThousand: Bool = false, twoThousand: Bool = false, fiveThousand: Bool = false) {
        self.oneHundred.borderColor = oneHundred ? R.color.brightPurple()! : R.color.textfieldBorder()!
        self.twoHundred.borderColor = twoHundred ? R.color.brightPurple()! : R.color.textfieldBorder()!
        self.fiveHundred.borderColor = fiveHundred ? R.color.brightPurple()! : R.color.textfieldBorder()!
        self.oneThousand.borderColor = oneThousand ? R.color.brightPurple()! : R.color.textfieldBorder()!
        self.twoThousand.borderColor = twoThousand ? R.color.brightPurple()! : R.color.textfieldBorder()!
        self.fiveThousand.borderColor = fiveThousand ? R.color.brightPurple()! : R.color.textfieldBorder()!
    }
    
    @IBAction func didSwitchTopUpSegment(_ sender: DealsSegmentedControl) {
        self.title = sender.selectedSegmentIndex == 0 ? TopUpType.airtime.stringValue : TopUpType.data.stringValue
        self.topUpType = sender.selectedSegmentIndex == 0 ? .airtime : .data
        self.btnLabel.text = sender.selectedSegmentIndex == 0 ? "Buy Airtime" : "Buy Data"
        amountStack.isHidden = sender.selectedSegmentIndex == 1
        planField.isHidden = sender.selectedSegmentIndex == 0
        amountField.contentTextField.text = ""
    }
    
    @IBAction func topUpTapped(_ sender: DealsButton) {
        if let amount = Double(amountField.content.replacingOccurrences(of: "₦", with: "").replacingOccurrences(of: ",", with: "")), amount > 0 {
            switch segmentControl.selectedSegmentIndex {
            case 0:
                CheckOutViewController.launch(self, paymentType: .airTime, amount: amount)
            case 1:
                CheckOutViewController.launch(self, paymentType: .data, amount: amount)
            default:
                return
            }
        }
    }
    
}

extension TopUpViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModel.images.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: R.nib.utilityCollectionViewCell.name, for: indexPath) as! UtilityCollectionViewCell
        cell.setup(image: viewModel.images[indexPath.item])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let cells = topupTypeCollection.visibleCells as? [UtilityCollectionViewCell] {
            for cell in cells {
                if cell.cellView.borderColor == R.color.brightPurple()! {
                    //Changes the color of a previously selected cell back to default (white) and its text color to deep purple
                    cell.deSelectCell()
                }
            }
        }
        
        // Changes the actual selected cell background color and text color
        if let cell = collectionView.cellForItem(at: indexPath) as? UtilityCollectionViewCell {
            cell.selectCell()
        }
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 120, height: 95)
    }
    
}

extension TopUpViewController: TopUpBeneficiaryDelegate {
    func selectBeneficiary(name: String, number: String) {
        numberField.content = number
    }
    

}

extension TopUpViewController: CNContactPickerDelegate {
    func contactPicker(_ picker: CNContactPickerViewController, didSelect contact: CNContact) {
        numberField.content = contact.phoneNumbers[0].value.stringValue
    }
}

extension TopUpViewController {
    static func launch(_ caller: UIViewController, topUpType: TopUpType = .airtime) {
        let controller = R.storyboard.utilities.topUpViewController()!
        controller.topUpType = topUpType
        caller.navigationController?.pushViewController(controller, animated: true)
    }
}
