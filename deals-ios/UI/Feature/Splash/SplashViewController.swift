import UIKit

class SplashViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        DispatchQueue.main.asyncAfter(deadline: .now()) {
            self.goToNextScreen()
        }
    }
    
    func goToNextScreen() {
        if DealsKeyValueRepository.sharedInstance.isOnboarded() {
            if DealsKeyValueRepository.sharedInstance.isLoggedIn() {
                //Go to Home
                HomeTabBarController.launch()
            } else {
                //Go to Login
                LoginViewController.launch(self)
            }
        } else {
            //Go to onboarding
            OnBoardingViewController.launch(self)
        }
    }

}

