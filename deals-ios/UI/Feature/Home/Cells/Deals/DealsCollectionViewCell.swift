import UIKit

class DealsCollectionViewCell: UICollectionViewCell {
        
    @IBOutlet weak var cellDescription: UILabel!
    @IBOutlet weak var cellLocation: UILabel!
    @IBOutlet weak var cellItemPrice: UILabel!
    @IBOutlet weak var cellOldPrice: StrikeThroughLabel!
    @IBOutlet weak var discountLabel: UILabel!
    @IBOutlet weak var buyNow: UIView!
    @IBOutlet weak var menu: UIImageView!
    @IBOutlet weak var itemImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    func configure(with deal: Deal) {
        cellDescription.text = deal.title
        cellLocation.text = deal.merchantName
        cellItemPrice.text = deal.sellingPrice.currencify()
        cellOldPrice.text = deal.actualPrice.currencify()
        discountLabel.text = "\(deal.discountPercentage))% OFF"
        itemImage.load(url: deal.imageURL)
    }
}
