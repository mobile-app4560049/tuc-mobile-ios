import UIKit

class HistoryTableViewCell: UITableViewCell {

    @IBOutlet weak var historyType: UILabel!
    @IBOutlet weak var historyDescription: UILabel!
    @IBOutlet weak var date: UILabel!
    @IBOutlet weak var time: UILabel!
    @IBOutlet weak var price: UILabel!
    @IBOutlet weak var contView: UIView!
    @IBOutlet weak var giftCard: UIImageView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        contView.dropShadow(opacity: 0.1)
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setupCell(historyType: String, historyDescription: String, date: String, time: String, price: String, isTopUp:Bool = false) {
        self.historyType.text = historyType
        self.historyDescription.text = historyDescription
        self.date.text = date
        self.time.text = time
        self.price.text = price
        self.giftCard.isHidden = isTopUp
    }
    
}
