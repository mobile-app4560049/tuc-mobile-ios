import UIKit

class MoreItemsTableViewCell: UITableViewCell {
    
    @IBOutlet weak var cellLabel: UILabel!
    @IBOutlet weak var chevron: UIImageView!
    @IBOutlet weak var cellImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setupCells(text: String, image: UIImage){
        cellLabel.text = text
        cellImage.image = image
        if text == "History" || text == "Change PIN" {
            chevron.isHidden = false
        } else {
            chevron.isHidden = true
        }
    }
    
}
