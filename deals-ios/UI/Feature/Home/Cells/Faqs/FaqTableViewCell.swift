import UIKit

class FaqTableViewCell: UITableViewCell {

    @IBOutlet weak var questionLabel: UILabel!
    @IBOutlet weak var iconImage: UIImageView!
    @IBOutlet weak var answerlabel: UILabel!
    @IBOutlet weak var answerView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        let backgroundView = UIView()
        backgroundView.backgroundColor = R.color.white()!
        self.selectedBackgroundView = backgroundView
        hideAnswer()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        if isAnswerHidden, selected {
            showAnswer()
        } else {
            hideAnswer()
        }
    }
    func setupCell(question: String, answer: String) {
        questionLabel.text = question
        answerlabel.text = answer
    }

    var isAnswerHidden: Bool {
        return answerlabel.isHidden
    }
    
    func showAnswer(){
        answerView.isHidden = false
        answerlabel.isHidden = false
        iconImage.image = R.image.faqDownwardArrow()!
    }
    
    func hideAnswer(){
        answerView.isHidden = true
        answerlabel.isHidden = true
        iconImage.image = R.image.faqForwardArrow()!
    }
   
  
    
}
