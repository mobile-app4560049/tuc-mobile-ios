import Foundation
import RxSwift
import RxCocoa
import RxDataSources


class BuyDealViewModel: BaseViewModel {
    
    // TODO: - Add BBuyDeal Vm Documentation

    private let datasource: BuyDealDataSource
    let successMessage = PublishSubject<String>()

    init(datasource: BuyDealDataSource) {
        self.datasource = datasource
    }
    
    let buyDealInitResp = BehaviorRelay<BuyDealInitResponse?>(value: nil)
    let buyPointInitResp = BehaviorRelay<BuyPointInitResponse?>(value: nil)
    let buyPointConfirmResp = BehaviorRelay<BuyPointConfirmResponse?>(value: nil)
    let buyDealConfirmResp = BehaviorRelay<BuyDealConfirmResponse?>(value: nil)
    let getDealCodeResp = BehaviorRelay<DealCodeResponse?>(value: nil)
    
    func buyDealInitialize(deal: Deal, paymentSource: PaymentSource, quantity: Int){
        progress.accept(true)
        datasource.buyDealInitialize(deal: deal, paymentSource: paymentSource, quantity: quantity).observeOn(MainScheduler.instance).subscribe { [weak self] (response)in
            guard let self = self else {return}
            self.progress.accept(false)
            if let data = response.result {
                self.successMessage.onNext(response.message)
                self.buyDealInitResp.accept(data)
            }
        } onError: { [weak self] (error) in
            guard let self = self else {return}
            self.progress.accept(false)
            if let error = error as? ApiError {
                self.error.onNext(error.message)
            } else {
                self.error.onNext(error.localizedDescription)
            }
        }.disposed(by: disposeBag)
    }
    
    func buyPointInitialize(deal: Deal, amount: String, paymentMode: String){
        progress.accept(true)
        datasource.buyPointInitialize(deal: deal, amount: amount, paymentMode: paymentMode).observeOn(MainScheduler.instance).subscribe { [weak self] (response)in
            guard let self = self else {return}
            self.progress.accept(false)
            if let data = response.result {
                self.successMessage.onNext(response.message)
                self.buyPointInitResp.accept(data)
            }
        } onError: { [weak self] (error) in
            guard let self = self else {return}
            self.progress.accept(false)
            if let error = error as? ApiError {
                self.error.onNext(error.message)
            } else {
                self.error.onNext(error.localizedDescription)
            }
        }.disposed(by: disposeBag)
    }
    
    func buyPointConfirm(amount: Int, paymentReference: String, refTransactionId: String, refStatus: String, refMessage: String, paymentMode: String){
        progress.accept(true)
        datasource.buyPointConfirm(amount: amount, paymentReference: paymentReference, refTransactionId: refTransactionId, refStatus: refStatus, refMessage: refMessage, paymentMode: paymentMode).observeOn(MainScheduler.instance).subscribe { [weak self] (response)in
            guard let self = self else {return}
            self.progress.accept(false)
            if let data = response.result {
                self.successMessage.onNext(response.message)
                self.buyPointConfirmResp.accept(data)
            } else {
                self.error.onNext(response.message)
            }
        } onError: { [weak self] (error) in
            guard let self = self else {return}
            self.progress.accept(false)
            if let error = error as? ApiError {
                self.error.onNext(error.message)
            } else {
                self.error.onNext(error.localizedDescription)
            }
        }.disposed(by: disposeBag)
    }
    
    func buyDealConfirm(dealInitResp: BuyDealInitResponse, deal: Deal, quantity: Int, buyPointConfRes: BuyPointConfirmResponse){
        progress.accept(true)
        datasource.buyDealConfirm(dealInitResp: dealInitResp, deal: deal, quantity: quantity, buyPointConfRes: buyPointConfRes).observeOn(MainScheduler.instance).subscribe { [weak self] (response)in
            guard let self = self else {return}
            self.progress.accept(false)
            if let data = response.result {
                self.successMessage.onNext(response.message)
                self.buyDealConfirmResp.accept(data)
            } else {
                self.error.onNext(response.message)
            }
        } onError: { [weak self] (error) in
            guard let self = self else {return}
            self.progress.accept(false)
            if let error = error as? ApiError {
                self.error.onNext(error.message)
            } else {
                self.error.onNext(error.localizedDescription)
            }
        }.disposed(by: disposeBag)
    }
    
    func getDealCode(bbuyDealResp: BuyDealConfirmResponse){
        progress.accept(true)
        datasource.getDealCode(bbuyDealResp: bbuyDealResp).observeOn(MainScheduler.instance).subscribe { [weak self] (response)in
            guard let self = self else {return}
            self.progress.accept(false)
            if let data = response.result {
                self.successMessage.onNext(response.message)
                self.getDealCodeResp.accept(data)
            }
        } onError: { [weak self] (error) in
            guard let self = self else {return}
            self.progress.accept(false)
            if let error = error as? ApiError {
                self.error.onNext(error.message)
            } else {
                self.error.onNext(error.localizedDescription)
            }
        }.disposed(by: disposeBag)
    }
 }
