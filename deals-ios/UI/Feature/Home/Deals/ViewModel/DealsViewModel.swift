import UIKit
import RxSwift
import RxCocoa
import RxDataSources

class DealsViewModel: BaseViewModel {
    private let datasource: DealsDataSource
    
    let pageProgress = BehaviorRelay<Bool>(value: false)
    
    init(datasource: DealsDataSource) {
        self.datasource = datasource
    }
    
    var topDealsResponse = BehaviorRelay<[Deal]>(value: [])
    var featuredDealsResponse = BehaviorRelay<[Deal]>(value: [])
    var flashDealsResponse = BehaviorRelay<[Deal]>(value: [])
    var allDealsResponse = BehaviorRelay<[Deal]>(value: [])
    var categoriesResponse = BehaviorRelay<[Category]>(value: [])
        
    /// This function gets top deals with pagination
    ///
    /// Returns a `[Deal]` Object
    func getTopDeals(
        page: Int,
        firstLoad: Bool = false,
        searchCondition: String? = nil,
        categories: [String]? = nil,
        merchants: [String]? = nil,
        city: String? = nil
    ) {
        firstLoad ? progress.accept(true) : pageProgress.accept(true)
        datasource.getTopDeals(
            offset: page, searchCondition: searchCondition ?? "",
            sortExpression: "Newest", categories: categories ?? [],
            merchants: merchants ?? [], city: city ?? "",
            location: "Top Deals"
        ).observeOn(MainScheduler.instance).subscribe { [weak self] (response)in
            guard let self = self else {return}
            firstLoad ? self.progress.accept(false) : self.pageProgress.accept(false)
            var deals: [Deal] = self.topDealsResponse.value
            if let newDeals: [Deal] = response.result?.data {
                deals = deals + newDeals
            }
            self.topDealsResponse.accept(deals)
        } onError: { [weak self] (error) in
            guard let self = self else {return}
            firstLoad ? self.progress.accept(false) : self.pageProgress.accept(false)
            if let error = error as? ApiError {
                self.error.onNext(error.message)
            } else {
                self.error.onNext(error.localizedDescription)
            }
        }.disposed(by: disposeBag)
    }
 
    /// This function gets featured deals with pagination
    ///
    /// Returns a `[Deal]` Object
    func getFeaturedDeals(
        page: Int,
        firstLoad: Bool = false,
        searchCondition: String? = nil,
        categories: [String]? = nil,
        merchants: [String]? = nil,
        city: String? = nil
    ) {
        firstLoad ? progress.accept(true) : pageProgress.accept(true)
        datasource.getTopDeals(
            offset: page, searchCondition: searchCondition ?? "",
            sortExpression: "Newest", categories: categories ?? [],
            merchants: merchants ?? [], city: city ?? "",
            location: "Featured Deals"
        ).observeOn(MainScheduler.instance).subscribe { [weak self] (response)in
            guard let self = self else {return}
            firstLoad ? self.progress.accept(false) : self.pageProgress.accept(false)
            var deals: [Deal] = self.featuredDealsResponse.value
            if let newDeals: [Deal] = response.result?.data {
                deals = deals + newDeals
            }
            self.featuredDealsResponse.accept(deals)
        } onError: { [weak self] (error) in
            guard let self = self else {return}
            firstLoad ? self.progress.accept(false) : self.pageProgress.accept(false)
            if let error = error as? ApiError {
                self.error.onNext(error.message)
            } else {
                self.error.onNext(error.localizedDescription)
            }
        }.disposed(by: disposeBag)
    }
    
    /// This function gets allcategories
    ///
    /// Returns a `[Category]` Object
    func getCategories() {
        progress.accept(true)
        datasource.getCategories().observeOn(MainScheduler.instance).subscribe { [weak self] (response)in
            guard let self = self else {return}
            self.progress.accept(false)
            if let categories: [Category] = response.result?.data {
                self.categoriesResponse.accept(categories)
            }
        } onError: { [weak self] (error) in
            guard let self = self else {return}
            self.progress.accept(false)
            if let error = error as? ApiError {
                self.error.onNext(error.message)
            } else {
                self.error.onNext(error.localizedDescription)
            }
        }.disposed(by: disposeBag)
    }
    
    
    /// This function gets flash deals with pagination
    ///
    /// Returns a `[Deal]` Object
    func getFlashDeals(
        page: Int,
        firstLoad: Bool = false,
        searchCondition: String? = nil,
        categories: [String]? = nil,
        merchants: [String]? = nil,
        city: String? = nil,
        cityId: Int? = nil,
        cityKey: String? = nil
    ) {
        firstLoad ? self.progress.accept(true) : self.pageProgress.accept(true)
        datasource.getAllDeals(
            offset: page,
            searchCondition: searchCondition ?? "",
            //TODO: Change back to flash
            sortExpression: "Newest",
            categories: categories ?? [],
            merchants: merchants ?? [],
            city: city ?? "",
            cityId: cityId ?? 0,
            cityKey: cityKey ?? ""
        ).observeOn(MainScheduler.instance).subscribe { [weak self] (response)in
            guard let self = self else {return}
            firstLoad ? self.progress.accept(false) : self.pageProgress.accept(false)
            var deals: [Deal] = self.flashDealsResponse.value
            if let newDeals: [Deal] = response.result?.data {
                deals = deals + newDeals
            }
            self.flashDealsResponse.accept(deals)
        } onError: { [weak self] (error) in
            guard let self = self else {return}
            firstLoad ? self.progress.accept(false) : self.pageProgress.accept(false)
            if let error = error as? ApiError {
                self.error.onNext(error.message)
            } else {
                self.error.onNext(error.localizedDescription)
            }
        }.disposed(by: disposeBag)
    }
    
    /// This function gets all deals with pagination
    ///
    /// Returns a `[Deal]` Object
    func getAllDeals(
        page: Int,
        firstLoad: Bool = false,
        searchCondition: String? = nil,
        categories: [String]? = nil,
        merchants: [String]? = nil,
        city: String? = nil,
        cityId: Int? = nil,
        cityKey: String? = nil
    ) {
        firstLoad ? self.progress.accept(true) : self.pageProgress.accept(true)
        datasource.getAllDeals(
            offset: page,
            searchCondition: searchCondition ?? "",
            sortExpression: "Newest",
            categories: categories ?? [],
            merchants: merchants ?? [],
            city: city ?? "",
            cityId: cityId ?? 0,
            cityKey: cityKey ?? ""
        ).observeOn(MainScheduler.instance).subscribe { [weak self] (response)in
            guard let self = self else {return}
            firstLoad ? self.progress.accept(false) : self.pageProgress.accept(false)
            var deals: [Deal] = self.allDealsResponse.value
            if let newDeals: [Deal] = response.result?.data {
                deals = deals + newDeals
            }
            self.allDealsResponse.accept(deals)
        } onError: { [weak self] (error) in
            guard let self = self else {return}
            firstLoad ? self.progress.accept(false) : self.pageProgress.accept(false)
            if let error = error as? ApiError {
                self.error.onNext(error.message)
            } else {
                self.error.onNext(error.localizedDescription)
            }
        }.disposed(by: disposeBag)
    }
}
