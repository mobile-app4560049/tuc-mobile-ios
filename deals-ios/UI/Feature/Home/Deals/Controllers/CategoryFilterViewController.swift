import UIKit
import RxSwift

protocol CategoryFilterDelegate {
    func filter(category: String)
}

class CategoryFilterViewController: BaseViewController {
    
    var filterDelegate: CategoryFilterDelegate!
    
    @IBOutlet weak var filterTable: UITableView!
    
    private var viewModel = DealsViewModel(datasource: DealsRepository())
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupX()
        filterTable.register(UINib(nibName: R.nib.categoryTableViewCell.name, bundle: nil), forCellReuseIdentifier: R.nib.categoryTableViewCell.identifier)
        bindVm()
    }
    private func bindVm(){
        viewModel.getCategories()
        
        viewModel.error.bind { [unowned self] error in
            self.showError(message: error)
        }.disposed(by: disposeBag)
        
        viewModel.progress.bind { [unowned self] loading in
            loading ? self.showProgressIndicator() : self.dismissProgressIndicator()
        }.disposed(by: disposeBag)
        
        viewModel.categoriesResponse.bind(to: filterTable.rx.items(cellIdentifier: R.nib.categoryTableViewCell.identifier)) {  [weak self]
            (index, item: Category, cell: CategoryTableViewCell) in
            guard let self = self else {return}
            cell.configure(with: item)
            cell.addTapGestureRecognizer {
                self.navigationController?.popViewController(animated: true)
                self.filterDelegate.filter(category: item.name)
            }
        }.disposed(by: disposeBag)
    }
    
    func setupX(){
        let button = UIButton()
        button.setImage(R.image.historyClose()!, for: .normal)
        button.addTapGestureRecognizer {
            self.navigationController?.popViewController(animated: true)
        }
        button.frame = CGRect(x: 0, y: 0, width: 20, height: 20)
        let barButton = UIBarButtonItem(customView: button)
        self.navigationItem.leftBarButtonItem = barButton
        self.navigationController?.navigationBar.backgroundColor = R.color.white()!
    }
}

extension CategoryFilterViewController {
    static func launch(_ caller: UIViewController, delegate: CategoryFilterDelegate) {
        let controller = R.storyboard.deals.categoryFilterViewController()!
        controller.filterDelegate = delegate
        caller.navigationController?.pushViewController(controller, animated: true)
    }
}
