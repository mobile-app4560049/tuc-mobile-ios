import UIKit
import BEMCheckBox

enum DealTypeCode {
    case service, product
    
    var literal: String {
        switch self {
        case .service:
            return "dealtype.service"
        case .product:
            return "dealtype.product"
        }
    }
}
enum ProductCheckout {
    case delivery, storePickup, service
}

class RegisteredProductCheckoutViewController: BaseViewController {

    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var merchantNameLabel: UILabel!
    @IBOutlet weak var dealImageView: UIImageView!
    @IBOutlet weak var dealItemLabel: UILabel!
    @IBOutlet weak var dealPriceLabel: UILabel!
    @IBOutlet weak var dealOldPriceLabel: UILabel!
    @IBOutlet weak var dealOffPercentLabel: UILabel!
    
    @IBOutlet weak var checkBox: BEMCheckBox!
    
    @IBOutlet weak var addDeal: UIImageView!
    @IBOutlet weak var removeDeal: UIImageView!
    
    @IBOutlet weak var amountLabel: UILabel!
    @IBOutlet weak var conFeeLabel: UILabel!
    @IBOutlet weak var deliFeeLabel: UILabel!
    @IBOutlet weak var totalAmountLabel: UILabel!
    
    @IBOutlet weak var receiverName: UILabel!
    @IBOutlet weak var receiverNumber: UILabel!
    @IBOutlet weak var receiverLocation: UILabel!
    
    @IBOutlet weak var btn: UIView!
    @IBOutlet weak var btnLabel: UILabel!
    @IBOutlet weak var pickupDeliveryLabel: UILabel!
    
    @IBOutlet weak var deliveryStack: UIStackView!
    
    @IBOutlet weak var deliveryLabel: UILabel!
    @IBOutlet weak var pickupLabel: UILabel!
    @IBOutlet weak var deliveryView: UIView!
    @IBOutlet weak var pickupView: UIView!
    
    @IBOutlet weak var changeAddress: UIView!

    
    var details: CheckoutDetails!
    var productCheckout: ProductCheckout = .delivery
    var quantity: Int = 1
    
    var convenienceFee: Double {
        get {
            details.deal.dealTypeCode == DealTypeCode.service.literal ? 0 : Double((details.deal.sellingPrice)) / 100.0
        }
    }
    var deliveryFee: Double {
        get {
            0
        }
    }
   
    var unitPrice: Double {
        get {
            Double(details.deal.sellingPrice) * Double(quantity)
        }
    }
    
    var totalPrice: Double {
        get {
            (Double(details.deal.sellingPrice) * Double(quantity)) + convenienceFee + deliveryFee
        }
    }
    private let viewModel: BuyDealViewModel = BuyDealViewModel(datasource: BuyDealRepository())

    override func viewDidLoad() {
        super.viewDidLoad()
        addGestures()
        setupNavButtons()
        setupUI()
        setupUI()
        setupView()
        bindBuyDealVm()
        updateViews()
    }
    
    private func updateCheckoutType(delivery: Bool = false, storePickup: Bool = false){
        deliveryLabel.textColor = delivery ? R.color.white()! : R.color.dashboardPurple()!
        deliveryView.backgroundColor = delivery ? R.color.dashboardPurple()! : R.color.white()!
        pickupLabel.textColor = storePickup ? R.color.white()! : R.color.dashboardPurple()!
        pickupView.backgroundColor = storePickup ? R.color.dashboardPurple()! : R.color.white()!
    }
    private func bindBuyDealVm() {
        
        viewModel.error.bind { [unowned self] error in
            self.showError(message: error)
        }.disposed(by: disposeBag)
        
        viewModel.progress.bind { [unowned self] loading in
            loading ? self.showProgressIndicator() : self.dismissProgressIndicator()
        }.disposed(by: disposeBag)
        
        viewModel.buyDealInitResp.bind { response in
            if let initResp = response {
                CheckOutViewController.launch(self, paymentType: .purchaseDeal, amount: self.totalPrice, dealDetails: self.details, dealResp: initResp)
            }
        }.disposed(by: disposeBag)
    }
    
    private func setupView(){
        deliveryStack.isHidden = details.deal.dealTypeCode == DealTypeCode.service.literal
        pickupView.isHidden = details.deal.dealTypeCode == DealTypeCode.service.literal
        deliveryLabel.text = details.deal.dealTypeCode == DealTypeCode.service.literal ? "Service" : "Delivery"
    }
    
    private func addGestures() {
        productCheckout = details.deal.dealTypeCode == DealTypeCode.service.literal ? .service : .storePickup
        
        btn.addTapGestureRecognizer { [self] in
            switch self.productCheckout {
            case .delivery:
                self.showInfo(message: InfoMessages.comingSoon)
            case .storePickup:
                self.showInfo(message: InfoMessages.comingSoon)
            case .service:
                viewModel.buyDealInitialize(deal: details.deal, paymentSource: .card, quantity: quantity)
            }
            
        }
        addDeal.addTapGestureRecognizer {
            self.quantity += 1
            self.updateViews()
        }
        removeDeal.addTapGestureRecognizer {
            if self.quantity != 1 {
                self.quantity -= 1
                self.updateViews()
            }
        }
        deliveryView.addTapGestureRecognizer {
            self.updateCheckoutType(delivery: true)
            self.productCheckout = .delivery
        }
        
        pickupView.addTapGestureRecognizer {
            self.updateCheckoutType(storePickup: true)
            self.productCheckout = .storePickup
        }
        
        changeAddress.addTapGestureRecognizer {
            UserAddressListViewController.launch(self, delegate: self)
        }
    }
    
    private func updateViews(){
        descriptionLabel.text = details.deal.dealTypeCode == DealTypeCode.service.literal
        ? "\(quantity) Service\(quantity > 1 ? "s" : "") from "
        : "\(quantity) Product\(quantity > 1 ? "s" : "") from "
        amountLabel.text = unitPrice.currencify()
        conFeeLabel.text = convenienceFee.currencify()
        totalAmountLabel.text = totalPrice.currencify()
        btnLabel.text = "Buy now @ \(totalPrice.currencify())"
        removeDeal.image = quantity == 1 ? R.image.removeDealDisabled() : R.image.removeDealEnabled()
    }
    
    private func setupUI() {
        quantity = details.quantity
        descriptionLabel.text = details.deal.dealTypeCode == DealTypeCode.service.literal
        ? "\(details.quantity) Service\(quantity > 1 ? "s" : "") from "
        : "\(details.quantity) Product\(quantity > 1 ? "s" : "") from "
        merchantNameLabel.text = details.deal.merchantName
        dealImageView.load(url: details.deal.imageURL)
        dealItemLabel.text = details.deal.title
        dealPriceLabel.text = Double(details.deal.sellingPrice).currencify()
        dealOldPriceLabel.text = Double(details.deal.actualPrice).currencify()
        dealOffPercentLabel.text = "\(details.percentOff)% off "
        btnLabel.text = "Buy now @ \(totalPrice.currencify())"
        amountLabel.text = unitPrice.currencify()
        conFeeLabel.text = convenienceFee.currencify()
        deliFeeLabel.text = deliveryFee.currencify()
        totalAmountLabel.text = totalPrice.currencify()
        
        receiverName.text = details.recepient.name
        receiverNumber.text = details.recepient.contact
        receiverLocation.text = details.recepient.address
        checkBox.boxType = .square
        checkBox.isEnabled = false
    }
    
    private func setupNavButtons(){
        let icon = UIImageView(image: R.image.trashCan()!)
        icon.frame = CGRect(x: 0, y: 0, width: 24, height: 24)
        icon.addTapGestureRecognizer {
            
        }
        let iconBTN = UIBarButtonItem(customView: icon)
        
        self.navigationItem.rightBarButtonItems = [iconBTN]
        self.navigationController?.navigationBar.backgroundColor = R.color.white()!
    }

}

extension RegisteredProductCheckoutViewController {
    static func launch(_ caller: UIViewController, details: CheckoutDetails) {
        let controller = R.storyboard.deals.registeredProductCheckoutViewController()!
        controller.details = details
        caller.navigationController?.pushViewController(controller, animated: true)
    }
}

extension RegisteredProductCheckoutViewController: UserAddressDelegate {
    func setAddress(address: UserAddress) {
        receiverLocation.text = "\(address.addressLine1) \(address.cityName) \(address.stateName)"
        receiverName.text = address.name
        receiverNumber.text = address.contactNumber
    }
}

struct CheckoutDetails {
    let deal: Deal
    var quantity: Int
    let percentOff: Double
    let recepient: Recepient
}

struct Recepient {
    let name: String
    let email: String
    let contact: String
    let address: String
}
