import UIKit
import RxSwift

class FeaturedDealsViewController: BaseViewController {
    
    @IBOutlet weak var featuredDealsCollection: UICollectionView!
    @IBOutlet weak var searchField: DealsInputField!
    @IBOutlet weak var filter: UIImageView!
    @IBOutlet weak var productServiceLabel: UILabel!
    
    private let viewModel: DealsViewModel = DealsViewModel(datasource: DealsRepository())
    private var dealList = [Deal]()
    override func viewDidLoad() {
        super.viewDidLoad()
        featuredDealsCollection.delegate = self
        featuredDealsCollection.register(UINib(nibName: R.nib.dealsCollectionViewCell.name, bundle: nil), forCellWithReuseIdentifier: R.nib.dealsCollectionViewCell.identifier)
        bindVm()
        filter.addTapGestureRecognizer {
            SubCategoriesViewController.launch(self, delegate: self)
        }
    }
    
    private func bindVm(){
        viewModel.getFeaturedDeals(page: 10, firstLoad: true)
        
        viewModel.error.bind { [unowned self] error in
            self.showError(message: error)
        }.disposed(by: disposeBag)
        
        viewModel.progress.bind { [unowned self] loading in
            loading ? self.showProgressIndicator() : self.dismissProgressIndicator()
        }.disposed(by: disposeBag)
        
        viewModel.featuredDealsResponse.bind(to: featuredDealsCollection.rx.items(cellIdentifier: R.nib.dealsCollectionViewCell.identifier)) {  [weak self]
            (index, item: Deal, cell: DealsCollectionViewCell) in
            guard let self = self else {return}
            self.dealList.append(item)
            cell.configure(with: item)
            cell.addTapGestureRecognizer {
                DealDetailsViewController.launch(self, deal: item)
            }
            
        }.disposed(by: disposeBag)
        
        viewModel.featuredDealsResponse.bind { deals in
            if !self.viewModel.progress.value && deals.isEmpty {
                self.featuredDealsCollection.setEmptyMessage(ErrorMessages.noDealFound)
            }
        }.disposed(by: disposeBag)
    }
    
    
}

extension FeaturedDealsViewController: UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width - 30, height: 350)
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        guard viewModel.featuredDealsResponse.value.count >= 10 else {return}
        if indexPath.row == viewModel.featuredDealsResponse.value.count - 1 {
            viewModel.getTopDeals(page: viewModel.featuredDealsResponse.value.count + 10)
        }
    }
}

extension FeaturedDealsViewController {
    static func launch(_ caller: UIViewController) {
        let controller = R.storyboard.deals.featuredDealsViewController()!
        caller.navigationController?.pushViewController(controller, animated: true)
    }
}

extension FeaturedDealsViewController: SubCategoryFilterDelegate {
    func filter(delivery: String?, discount: String?, price: (min: String, max: String)?) {
        viewModel.getFeaturedDeals(page: viewModel.allDealsResponse.value.count + 10, firstLoad: true, categories: [delivery ?? "", discount ?? "", price?.min ?? "", price?.max ?? ""])
    }
}
