import UIKit

class FlashDealsViewController: BaseViewController {

    @IBOutlet weak var flashDealsCollection: UICollectionView!
    @IBOutlet weak var searchField: DealsInputField!
    @IBOutlet weak var filter: UIImageView!
    @IBOutlet weak var productServiceLabel: UILabel!
    
    private let viewModel: DealsViewModel = DealsViewModel(datasource: DealsRepository())
    private var flashDealsList = [Deal]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavButtons()
        flashDealsCollection.delegate = self
        flashDealsCollection.register(UINib(nibName: R.nib.dealsCollectionViewCell.name, bundle: nil), forCellWithReuseIdentifier: R.nib.dealsCollectionViewCell.identifier)
        bindVm()
        filter.addTapGestureRecognizer {
            SubCategoriesViewController.launch(self, delegate: self)
        }
    }
    
    private func bindVm(){
        viewModel.getFlashDeals(page: 1, firstLoad: true)
        
        viewModel.error.bind { [unowned self] error in
            self.showError(message: error)
        }.disposed(by: disposeBag)
        
        viewModel.progress.bind { [unowned self] loading in
            loading ? self.showProgressIndicator() : self.dismissProgressIndicator()
        }.disposed(by: disposeBag)
        
        viewModel.flashDealsResponse.bind(to: flashDealsCollection.rx.items(cellIdentifier: R.nib.dealsCollectionViewCell.identifier)) {  [weak self]
            (index, item: Deal, cell: DealsCollectionViewCell) in
            guard let self = self else {return}
            self.flashDealsList.append(item)
            cell.configure(with: item)
            cell.addTapGestureRecognizer {
                DealDetailsViewController.launch(self, deal: item)
            }
        }.disposed(by: disposeBag)
        
        viewModel.flashDealsResponse.bind { deals in
            if !self.viewModel.pageProgress.value && !self.viewModel.progress.value && deals.isEmpty {
                self.flashDealsCollection.setEmptyMessage(ErrorMessages.noDealFound)
            }
        }.disposed(by: disposeBag)
    }
    
    func setupNavButtons(){
        
        let timer = UIImageView(image: R.image.dashboardTimer()!)
        timer.frame = CGRect(x: 0, y: 0, width: 24, height: 24)
        let timerBTN = UIBarButtonItem(customView: timer)
        
        let timerLabel = UILabel()
        timerLabel.font = R.font.brFirmaMedium(size: 12)
        timerLabel.textColor = R.color.red()!
        timerLabel.text = "22:20:16"

        let timerLabelDisplay = UIBarButtonItem(customView: timerLabel)
        
        self.navigationItem.rightBarButtonItems = [timerLabelDisplay, timerBTN]
        self.navigationController?.navigationBar.backgroundColor = R.color.white()!
    }

}

extension FlashDealsViewController: UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width - 30, height: 350)
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        guard viewModel.flashDealsResponse.value.count >= 10 else {return}
        if indexPath.row == viewModel.flashDealsResponse.value.count - 1 {
            viewModel.getTopDeals(page: viewModel.flashDealsResponse.value.count + 10)
        }
    }
    
}


extension FlashDealsViewController {
    static func launch(_ caller: UIViewController) {
        let controller = R.storyboard.deals.flashDealsViewController()!
        caller.navigationController?.pushViewController(controller, animated: true)
    }
}

extension FlashDealsViewController: SubCategoryFilterDelegate {
    func filter(delivery: String?, discount: String?, price: (min: String, max: String)?) {
        viewModel.getFlashDeals(page: viewModel.allDealsResponse.value.count + 10, firstLoad: true, categories: [delivery ?? "", discount ?? "", price?.min ?? "", price?.max ?? ""])
    }
}
