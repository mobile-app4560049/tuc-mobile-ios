import UIKit
import RangeSeekSlider

protocol SubCategoryFilterDelegate {
    func filter(delivery: String?, discount: String?, price: (min: String, max: String)? )
}

class SubCategoriesViewController: BaseViewController {
    
    @IBOutlet weak var slider: RangeSeekSlider!
    @IBOutlet weak var minAmount: DealsAmountField!
    @IBOutlet weak var maxAmount: DealsAmountField!
    @IBOutlet weak var btn: UIView!

    var delegate: SubCategoryFilterDelegate!
    override func viewDidLoad() {
        super.viewDidLoad()
        slider.delegate = self
        btn.addTapGestureRecognizer { [self] in
            self.navigationController?.popViewController(animated: true)
            self.delegate.filter(delivery: "", discount: "", price: (minAmount.content, maxAmount.content))
        }
    }


}

extension SubCategoriesViewController: RangeSeekSliderDelegate {
    func rangeSeekSlider(_ slider: RangeSeekSlider, didChange minValue: CGFloat, maxValue: CGFloat) {
        minAmount.contentTextField.text = Double(minValue).currencify()
        maxAmount.contentTextField.text = Double(maxValue).currencify()
    }
}


extension SubCategoriesViewController {
    static func launch(_ caller: UIViewController, delegate: SubCategoryFilterDelegate) {
        let controller = R.storyboard.deals.subCategoriesViewController()!
        controller.delegate = delegate
        caller.navigationController?.pushViewController(controller, animated: true)
    }
}

