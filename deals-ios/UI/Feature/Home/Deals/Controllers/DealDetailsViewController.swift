import UIKit

class DealDetailsViewController: BaseViewController {

    @IBOutlet weak var dealImageView: UIImageView!
    @IBOutlet weak var dealItemLabel: UILabel!
    @IBOutlet weak var dealPriceLabel: UILabel!
    @IBOutlet weak var dealOldPriceLabel: UILabel!
    @IBOutlet weak var dealOffPercentLabel: UILabel!
    @IBOutlet weak var dealCategoryLabel: UILabel!
    
    @IBOutlet weak var merchantImageView: UIImageView!
    @IBOutlet weak var merchantNameLael: UILabel!
    
    @IBOutlet weak var star1: UIImageView!
    @IBOutlet weak var star2: UIImageView!
    @IBOutlet weak var star3: UIImageView!
    @IBOutlet weak var star4: UIImageView!
    @IBOutlet weak var star5: UIImageView!
    
    @IBOutlet weak var about: UILabel!
    @IBOutlet weak var aboutText: UILabel!
    @IBOutlet weak var aboutLine: UIView!
    @IBOutlet weak var aboutView: UIView!
    
    @IBOutlet weak var location: UILabel!
    @IBOutlet weak var locationText: UILabel!
    @IBOutlet weak var locationLine: UIView!
    @IBOutlet weak var locationView: UIView!
    
    @IBOutlet weak var terms: UILabel!
    @IBOutlet weak var termsText: UILabel!
    @IBOutlet weak var termsLine: UIView!
    @IBOutlet weak var termsView: UIView!
    
    @IBOutlet weak var amountLabel: UILabel!
    @IBOutlet weak var addDeal: UIImageView!
    @IBOutlet weak var removeDeal: UIImageView!
    @IBOutlet weak var buyButton: UIView!
    @IBOutlet weak var buyButtonLael: UILabel!
            
    var deal: Deal!
    var quantity: Int = 1
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavButtons()
        setupUI()
        addGestures()
    }
    
    private func addGestures(){
        addDeal.addTapGestureRecognizer {
            self.quantity += 1
            self.updateViews()
        }
        removeDeal.addTapGestureRecognizer {
            if self.quantity != 1 {
                self.quantity -= 1
                self.updateViews()
            }
        }
        buyButton.addTapGestureRecognizer { [weak self] in
            guard let self = self else {return}
            if let user = DealsKeyValueRepository.sharedInstance.getUser() {
                let details = CheckoutDetails(
                    deal: self.deal,
                    quantity: self.quantity,
                    percentOff: (Double((1 - (self.deal.sellingPrice / self.deal.actualPrice))) * 100).rounded(),
                    recepient: Recepient(
                        name: user.name,
                        email: user.emailAddress,
                        contact: "\(user.country.isd)\(user.referralCode)",
                        address: "4 princess folashade ojutalayo, Lekki Phase 1")
                )
                RegisteredProductCheckoutViewController.launch(self, details: details)
            } else {
                UnregisteredProductCheckoutViewController.launch(self, deal: self.deal)
            }
        }
        about.addTapGestureRecognizer { [weak self] in
            guard let self = self else {return}
            self.switchView(about: true)
        }
        
        location.addTapGestureRecognizer { [weak self] in
            guard let self = self else {return}
            self.switchView(location: true)
        }
        
        terms.addTapGestureRecognizer { [weak self] in
            guard let self = self else {return}
            self.switchView(terms: true)
        }
    }
    
    private func switchView(about:Bool = false, location:Bool = false, terms:Bool = false){
        self.about.textColor = about ? R.color.brightPurple()! : R.color.history()!
        self.aboutLine.backgroundColor = about ? R.color.brightPurple()! : R.color.history()!
        self.aboutView.isHidden = !about
        
        self.location.textColor = location ? R.color.brightPurple()! : R.color.history()!
        self.locationLine.backgroundColor = location ? R.color.brightPurple()! : R.color.history()!
        self.locationView.isHidden = !location
        
        self.terms.textColor = terms ? R.color.brightPurple()! : R.color.history()!
        self.termsLine.backgroundColor = terms ? R.color.brightPurple()! : R.color.history()!
        self.termsView.isHidden = !terms
    }
    
    private func updateViews(){
        let amount = deal.sellingPrice * quantity
        amountLabel.text = "\(quantity)"
        buyButtonLael.text = "Add \(quantity) for \(amount.currencify())."
        removeDeal.image = quantity == 1 ? R.image.removeDealDisabled() : R.image.removeDealEnabled()
    }
    
    private func setupUI(){
        switchView(about: true)
        dealImageView.load(url: deal.imageURL)
        dealItemLabel.text = deal.title
        dealPriceLabel.text = deal.sellingPrice.currencify()
        dealOldPriceLabel.text = deal.actualPrice.currencify()
        dealOffPercentLabel.text = "\(deal.discountPercentage)% OFF"
        dealCategoryLabel.text = deal.categoryName
        merchantImageView.load(url: deal.merchantIconURL)
        amountLabel.text = "\(quantity)"
        let amount = deal.sellingPrice * quantity
        buyButtonLael.text = "Add \(quantity) for \(amount.currencify())."
        merchantNameLael.text = deal.merchantName
        aboutText.text = deal.title
        locationText.text = "Location \n4 Princess Folashade Ojutalayo, Lekki Phase 1"
        termsText.text = InfoMessages.terms
    }
    
    private func setupNavButtons(){
        
        let icon = UIImageView(image: R.image.trashCan()!)
        icon.frame = CGRect(x: 0, y: 0, width: 24, height: 24)
        icon.addTapGestureRecognizer {
            
        }
        let iconBTN = UIBarButtonItem(customView: icon)
        
        self.navigationItem.rightBarButtonItems = [iconBTN]
        self.navigationController?.navigationBar.backgroundColor = R.color.white()!
    }

}

extension DealDetailsViewController {
    static func launch(_ caller: UIViewController, deal: Deal) {
        let controller = R.storyboard.deals.dealDetailsViewController()!
        controller.deal = deal
        caller.navigationController?.pushViewController(controller, animated: true)
    }
}

