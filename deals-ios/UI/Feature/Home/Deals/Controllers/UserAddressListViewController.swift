import UIKit

protocol UserAddressDelegate {
    func setAddress(address: UserAddress)
}

class UserAddressListViewController: BaseViewController {

    @IBOutlet weak var addressTable: UITableView!
    var delegate: UserAddressDelegate!
    private var viewModel = LocationViewModel(datasource: LocationRepository())

    override func viewDidLoad() {
        super.viewDidLoad()
        addressTable.register(UINib(nibName: R.nib.categoryTableViewCell.name, bundle: nil), forCellReuseIdentifier: R.nib.categoryTableViewCell.identifier)
        bindVm()
    }
    
    private func bindVm(){
        viewModel.getUserAddresses(offSet: 1)
        
        viewModel.error.bind { [unowned self] error in
            self.showError(message: error)
        }.disposed(by: disposeBag)
        
        viewModel.progress.bind { [unowned self] loading in
            loading ? self.showProgressIndicator() : self.dismissProgressIndicator()
        }.disposed(by: disposeBag)
        
        viewModel.userAddressesResponse.bind(to: addressTable.rx.items(cellIdentifier: R.nib.categoryTableViewCell.identifier)) {  [weak self]
            (index, item: UserAddress, cell: CategoryTableViewCell) in
            guard let self = self else {return}
            cell.cellImageView.image = R.image.dealsLocation()!
            cell.cellLabel.text = "\(item.addressLine1) \(item.cityName) \(item.stateName)"
            cell.addTapGestureRecognizer {
                self.navigationController?.popViewController(animated: true)
                self.delegate.setAddress(address: item)
            }
        }.disposed(by: disposeBag)
    }
}

extension UserAddressListViewController {
    static func launch(_ caller: UIViewController, delegate: UserAddressDelegate) {
        let controller = R.storyboard.deals.userAddressListViewController()!
        controller.delegate = delegate
        caller.navigationController?.pushViewController(controller, animated: true)
    }
}
