import UIKit
import ActionSheetPicker_3_0

class UnregisteredProductCheckoutViewController: BaseViewController {
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var merchantNameLabel: UILabel!
    @IBOutlet weak var dealImageView: UIImageView!
    @IBOutlet weak var dealItemLabel: UILabel!
    @IBOutlet weak var dealPriceLabel: UILabel!
    @IBOutlet weak var dealOldPriceLabel: UILabel!
    @IBOutlet weak var dealOffPercentLabel: UILabel!
    
    @IBOutlet weak var addDeal: UIImageView!
    @IBOutlet weak var removeDeal: UIImageView!
    
    @IBOutlet weak var loginLabel: UILabel!
    
    @IBOutlet weak var nameField: UITextField!
    @IBOutlet weak var numberField: UITextField!
    @IBOutlet weak var emailField: UITextField!
    @IBOutlet weak var addressField: UITextField!
    @IBOutlet weak var stateField: UITextField!
    @IBOutlet weak var cityField: UITextField!
    @IBOutlet weak var btn: UIView!
    
    var deal: Deal!
    var quantity: Int = 1
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavButtons()
        addGestures()
        setupUI()
    }
    
    private func addGestures(){
        stateField.addTapGestureRecognizer {
            ActionSheetStringPicker.show(
                withTitle: "State",
                rows: ["Lagos", "Abuja", "Oyo", "Rivers"],
                initialSelection: 0,
                doneBlock: { picker, index, item in
                    if let item = item {
                        self.stateField.text = "\(item)"
                    }
                },
                cancel: nil,
                origin: self
            )
        }
        btn.addTapGestureRecognizer { [weak self] in
            guard let self = self else {return}
            let validator = Validator(self)
            if let name = self.nameField.text,
               let number = self.numberField.text,
               let email = self.emailField.text,
               let address = self.addressField.text,
               let state = self.stateField.text,
               let city = self.cityField.text {
                if validator.textValidation(self.nameField.text, ErrorMessages.fieldError("name")) &&
                    validator.textValidation(self.numberField.text, ErrorMessages.fieldError("phone number")) &&
                    validator.emailValidation(email) &&
                    validator.textValidation(self.addressField.text, ErrorMessages.fieldError("address")) &&
                    validator.textValidation(self.stateField.text, ErrorMessages.fieldError("state")) &&
                    validator.textValidation(self.cityField.text, ErrorMessages.fieldError("city"))
                {
                    let details = CheckoutDetails(
                        deal: self.deal,
                        quantity: self.quantity,
                        percentOff: Double(self.deal.discountPercentage),
                        recepient: Recepient(
                            name: name,
                            email: email,
                            contact: number,
                            address: "\(address) \(city) \(state)")
                    )
                    RegisteredProductCheckoutViewController.launch(self, details: details)
                }
                
            }
        }
        addDeal.addTapGestureRecognizer {
            self.quantity += 1
            self.updateViews()
        }
        removeDeal.addTapGestureRecognizer {
            if self.quantity != 1 {
                self.quantity -= 1
                self.updateViews()
            }
        }
    }
    
    private func updateViews(){
        descriptionLabel.text = "\(quantity) Product from "
    }
    
    private func setupUI() {
        descriptionLabel.text = "\(quantity) Product from "
        merchantNameLabel.text = deal.merchantName
        dealImageView.load(url: deal.imageURL)
        dealItemLabel.text = deal.title
        dealPriceLabel.text = Double(deal.sellingPrice).currencify()
        dealOldPriceLabel.text = Double(deal.actualPrice).currencify()
        dealOffPercentLabel.text = "\(deal.discountPercentage)% off "
    }
    
    private func setupNavButtons(){
        
        let icon = UIImageView(image: R.image.trashCan()!)
        icon.frame = CGRect(x: 0, y: 0, width: 24, height: 24)
        icon.addTapGestureRecognizer {
            
        }
        let iconBTN = UIBarButtonItem(customView: icon)
        
        self.navigationItem.rightBarButtonItems = [iconBTN]
        self.navigationController?.navigationBar.backgroundColor = R.color.white()!
    }
    
}

extension UnregisteredProductCheckoutViewController {
    static func launch(_ caller: UIViewController, deal: Deal) {
        let controller = R.storyboard.deals.unregisteredProductCheckoutViewController()!
        controller.deal = deal
        caller.navigationController?.pushViewController(controller, animated: true)
    }
}
