import UIKit

class DealsViewController: BaseViewController {
    
    @IBOutlet weak var searchField: DealsInputField!
    @IBOutlet weak var filterIcon: UIView!

    @IBOutlet weak var allDealsCollection: UICollectionView!

    private let viewModel: DealsViewModel = DealsViewModel(datasource: DealsRepository())
    private var flashDealsList = [Deal]()
    private var featuredDealsList = [Deal]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavButtons()
        
        allDealsCollection.delegate = self
        allDealsCollection.register(UINib(nibName: R.nib.dealsCollectionViewCell.name, bundle: nil), forCellWithReuseIdentifier: R.nib.dealsCollectionViewCell.identifier)
        bindVm()
        filterIcon.addTapGestureRecognizer {
            SubCategoriesViewController.launch(self, delegate: self)
        }
    }
    
    private func bindVm(){
        viewModel.getAllDeals(page: 10, firstLoad: true)
        
        viewModel.error.bind { [unowned self] error in
            self.showError(message: error)
        }.disposed(by: disposeBag)
        
        viewModel.progress.bind { [unowned self] loading in
            loading ? self.showProgressIndicator() : self.dismissProgressIndicator()
        }.disposed(by: disposeBag)
        
        viewModel.allDealsResponse.bind(to: allDealsCollection.rx.items(cellIdentifier: R.nib.dealsCollectionViewCell.identifier)) {  [weak self]
            (index, item: Deal, cell: DealsCollectionViewCell) in
            guard let self = self else {return}
            self.flashDealsList.append(item)
            cell.configure(with: item)
            cell.addTapGestureRecognizer {
                DealDetailsViewController.launch(self, deal: item)
            }
        }.disposed(by: disposeBag)
        
        viewModel.allDealsResponse.bind { deals in
            if !self.viewModel.progress.value && !self.viewModel.pageProgress.value && deals.isEmpty {
                self.allDealsCollection.setEmptyMessage(ErrorMessages.noDealFound)
            }
        }.disposed(by: disposeBag)

    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        self.title = "All Deals"
        self.removeBackButton()
        self.tabBarController?.tabBar.isHidden = false
    }
    
    func setupNavButtons(){
        
        let menu = UIImageView(image: R.image.dealsMenuBlack()!)
        menu.frame = CGRect(x: 0, y: 0, width: 24, height: 24)
        menu.addTapGestureRecognizer {
            self.tabBarController?.tabBar.isHidden = true
            CategoryFilterViewController.launch(self, delegate: self)
        }
        let notiBTN = UIBarButtonItem(customView: menu)
        
        let notification = UIImageView(image: R.image.dealsNotifications()!)
        notification.frame = CGRect(x: 0, y: 0, width: 24, height: 24)
        notification.addTapGestureRecognizer {
            NotificationsViewController.launch(self)
        }
        let menuBTN = UIBarButtonItem(customView: notification)
        
        self.navigationItem.rightBarButtonItems = [notiBTN, menuBTN]
        self.navigationController?.navigationBar.backgroundColor = R.color.white()!
    }
}

extension DealsViewController: UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 250, height: 300)
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
            guard viewModel.allDealsResponse.value.count >= 10 else {return}
            if indexPath.row == viewModel.allDealsResponse.value.count - 1 {
                viewModel.getAllDeals(page: viewModel.allDealsResponse.value.count + 10)
            }
    }
    
}

extension DealsViewController: CategoryFilterDelegate {
    func filter(category: String) {
        viewModel.getAllDeals(page: viewModel.allDealsResponse.value.count + 10, categories: [category])
    }
}

extension DealsViewController: SubCategoryFilterDelegate {
    func filter(delivery: String?, discount: String?, price: (min: String, max: String)?) {
        viewModel.getAllDeals(page: viewModel.allDealsResponse.value.count + 10, firstLoad: true, categories: [delivery ?? "", discount ?? "", price?.min ?? "", price?.max ?? ""])
    }
}
