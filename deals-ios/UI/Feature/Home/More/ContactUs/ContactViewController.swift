import UIKit
import FreshchatSDK

class ContactViewController: BaseViewController {

    @IBOutlet weak var freshChatImage: UIImageView!
    @IBOutlet weak var numberOne: UILabel!
    @IBOutlet weak var numberTwo: UILabel!
    @IBOutlet weak var numberThree: UILabel!
    @IBOutlet weak var locationLabel: UILabel!
    
    @IBOutlet weak var whatsApp: UILabel!
    @IBOutlet weak var mail: UILabel!
    @IBOutlet weak var facebook: UILabel!
    @IBOutlet weak var twitter: UILabel!
    @IBOutlet weak var youTube: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addGestures()
    }
    
    private func addGestures(){
        
        freshChatImage.addTapGestureRecognizer {
            Freshchat.sharedInstance().showConversations(self)
        }
        
        numberOne.addTapGestureRecognizer {
            if let url = URL(string: Constants.numberOne), UIApplication.shared.canOpenURL(url){
                UIApplication.shared.open(url)
            }
        }
        numberTwo.addTapGestureRecognizer {
            if let url = URL(string: Constants.numberTwo), UIApplication.shared.canOpenURL(url){
                UIApplication.shared.open(url)
            }
        }
        numberThree.addTapGestureRecognizer {
            if let url = URL(string: Constants.numberThree), UIApplication.shared.canOpenURL(url){
                UIApplication.shared.open(url)
            }
        }
        
        locationLabel.addTapGestureRecognizer {
            UIApplication.tryURL(urls: [
                Constants.googleMapApi.appURl,
                Constants.googleMapApi.webUrl
            ])
        }
        
        mail.addTapGestureRecognizer {
            UIApplication.tryURL(urls: [
                Constants.mail
            ])
        }
        
        whatsApp.addTapGestureRecognizer {
            UIApplication.tryURL(urls: [
                Constants.whatsApp.appURl,
                Constants.whatsApp.webUrl
            ])
        }
        
        twitter.addTapGestureRecognizer {
            UIApplication.tryURL(urls: [
                Constants.twitter.appURl,
                Constants.twitter.webUrl
            ])
        }
        
        facebook.addTapGestureRecognizer {
            UIApplication.tryURL(urls: [
                Constants.faceBook.appURl,
                Constants.faceBook.webUrl
            ])
            
        }
        
        youTube.addTapGestureRecognizer {
            UIApplication.tryURL(urls: [
                Constants.youTube.appURl,
                Constants.youTube.webUrl
            ])
        }
    }

}

extension ContactViewController {
    static func launch(_ caller: UIViewController) {
        let controller = R.storyboard.moreItems.contactViewController()!
        caller.navigationController?.pushViewController(controller, animated: true)
    }
}
