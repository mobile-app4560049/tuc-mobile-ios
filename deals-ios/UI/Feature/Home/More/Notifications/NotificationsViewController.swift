import UIKit

class NotificationsViewController: BaseViewController {

    @IBOutlet weak var notificationsTable: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        notificationsTable.dataSource = self
        notificationsTable.delegate = self
        notificationsTable.register(UINib(nibName: R.nib.notificationsTableViewCell.name, bundle: nil), forCellReuseIdentifier: R.nib.notificationsTableViewCell.identifier)
    }
    

}

extension NotificationsViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: R.nib.notificationsTableViewCell.identifier, for: indexPath) as! NotificationsTableViewCell
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
       return 150
    }
    
}

extension NotificationsViewController {
    static func launch(_ caller: UIViewController) {
        let controller = R.storyboard.moreItems.notificationsViewController()!
        caller.navigationController?.pushViewController(controller, animated: true)
    }
}
