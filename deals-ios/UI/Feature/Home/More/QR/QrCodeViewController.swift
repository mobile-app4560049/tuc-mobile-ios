import UIKit

class QrCodeViewController: BaseViewController {

    @IBOutlet weak var referralQRImageView: UIImageView!

    override func viewDidLoad() {
        super.viewDidLoad()
        setQRImage()
    }

    private func setQRImage(){
        guard let user = DealsKeyValueRepository.sharedInstance.getUser() else {return}
        referralQRImageView.generateQRCode(uniqueId: user.referralCode)
    }
}

extension QrCodeViewController {
    static func launch(_ caller: UIViewController) {
        let controller = R.storyboard.moreItems.qrCodeViewController()!
        caller.navigationController?.pushViewController(controller, animated: true)
    }
}
