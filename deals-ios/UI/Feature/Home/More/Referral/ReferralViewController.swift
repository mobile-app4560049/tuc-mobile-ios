import UIKit

class ReferralViewController: BaseViewController {

    @IBOutlet weak var editReferalView: UIView!
    @IBOutlet weak var copyLabel: UILabel!
    @IBOutlet weak var referralLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupRef()
    }
    
    private func setupRef(){
        guard let user = DealsKeyValueRepository.sharedInstance.getUser() else {return}
        referralLabel.text = "thankucashreferall.com/\(user.referralCode)"
        
        copyLabel.addTapGestureRecognizer {
            UIPasteboard.general.string = "thankucashreferall.com/\(user.referralCode)"
            self.showSuccess(message: SuccessMessages.copySuccess("Referral"))
        }
        editReferalView.addTapGestureRecognizer {
            ReferralEditViewController.launch(self, referral: user.referralCode)
        }
    }

}
extension ReferralViewController {
    static func launch(_ caller: UIViewController) {
        let controller = R.storyboard.moreItems.referralViewController()!
        caller.navigationController?.pushViewController(controller, animated: true)
    }
}
