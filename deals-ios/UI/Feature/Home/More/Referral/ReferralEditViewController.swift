import UIKit

class ReferralEditViewController: BaseViewController {

    var referralString: String!
    @IBOutlet weak var referralField: DealsInputField!

    private var viewModel : MoreItemsViewModel = MoreItemsViewModel(datasource: MoreItemsRepository())

    
    override func viewDidLoad() {
        super.viewDidLoad()
        referralField.content = referralString
        referralField.becomeFirstResponder()
        bindVM()
    }
    
    private func bindVM(){
        viewModel.error.bind { [unowned self] error in
            self.showError(message: error)
        }.disposed(by: disposeBag)
        
        viewModel.progress.bind { [unowned self] loading in
            loading ? self.showProgressIndicator() : self.dismissProgressIndicator()
        }.disposed(by: disposeBag)
        
        viewModel.successMessage.bind { message in
            self.showSuccess(message: message)
        }.disposed(by: disposeBag)
        
        viewModel.updateReferralIdResponse.bind { response in
            if response != nil, let response = response {
                print(response.message)
            }
        }.disposed(by: disposeBag)
    }
    
    @IBAction func saveTapped(_ sender: DealsButton) {
        if referralField.content.isEmpty {
            self.showError(message: ErrorMessages.enterReferralId)
        } else {
            viewModel.updateReferralId(referralID: referralField.content)
        }
    }
    

}
extension ReferralEditViewController {
    static func launch(_ caller: UIViewController, referral: String) {
        let controller = R.storyboard.moreItems.referralEditViewController()!
        controller.referralString = referral
        caller.navigationController?.pushViewController(controller, animated: true)
    }
}
