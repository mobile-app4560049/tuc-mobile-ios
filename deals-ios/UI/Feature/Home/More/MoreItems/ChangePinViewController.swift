import UIKit

class ChangePinViewController: BaseViewController {

    @IBOutlet weak var oldPinField: DealsPinField!
    @IBOutlet weak var newPinField: DealsPinField!
    @IBOutlet weak var confirmPinField: DealsPinField!
    
    private var viewModel : MoreItemsViewModel = MoreItemsViewModel(datasource: MoreItemsRepository())

    override func viewDidLoad() {
        super.viewDidLoad()
        bindVM()
    }
    
    private func bindVM(){
        viewModel.error.bind { [unowned self] error in
            self.showError(message: error)
        }.disposed(by: disposeBag)
        
        viewModel.progress.bind { [unowned self] loading in
            loading ? self.showProgressIndicator() : self.dismissProgressIndicator()
        }.disposed(by: disposeBag)
        
        viewModel.successMessage.bind { message in
            self.showSuccess(message: message)
        }.disposed(by: disposeBag)
        
        viewModel.changePinResponse.bind { message in
            if message != nil {
                DealsKeyValueRepository.sharedInstance.logOut()
                LoginViewController.launchStandAlone()
            }
        }.disposed(by: disposeBag)
    }
    
    @IBAction func changePinTapped(_ sender: DealsButton) {
        let validator = Validator(self)
        if validator.pinValidation(oldPinField.content) && validator.pinValidation(newPinField.content) && validator.checkEqual(newPinField.content, confirmPinField.content, fieldName: "New Pins") {
            viewModel.changePin(oldPin: oldPinField.content, newPin: newPinField.content)
        }
    }

}

extension ChangePinViewController {
    static func launch(_ caller: UIViewController) {
        let controller = R.storyboard.moreItems.changePinViewController()!
        caller.navigationController?.pushViewController(controller, animated: true)
    }
}

