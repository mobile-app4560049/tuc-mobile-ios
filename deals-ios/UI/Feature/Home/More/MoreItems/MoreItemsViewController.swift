import UIKit

class MoreItemsViewController: BaseViewController {
    
    @IBOutlet weak var profileView: UIView!
    @IBOutlet weak var table: UITableView!
    @IBOutlet weak var logoutView: UIView!
    @IBOutlet weak var profileNameLabel: UILabel!
    @IBOutlet weak var countryLabel: UILabel!
    @IBOutlet weak var profileImage: UIImageView!
    
    let viewModel = MoreItemsViewModel(datasource: MoreItemsRepository())
    
    override func viewDidLoad() {
        super.viewDidLoad()
        table.delegate = self
        table.dataSource = self
        table.register(UINib(nibName: R.nib.moreItemsTableViewCell.name, bundle: nil), forCellReuseIdentifier: R.nib.moreItemsTableViewCell.identifier)
        profileView.addTapGestureRecognizer {
            
        }
        logoutView.addTapGestureRecognizer {
            LogOutViewController.show(self, delegate: self)
        }
        profileView.addTapGestureRecognizer {
            ProfileViewController.launch(self)
        }
        setupUI()
    }
    
    private func setupUI(){
        guard let user = DealsKeyValueRepository.sharedInstance.getUser() else {return}
        profileNameLabel.text = user.name
        countryLabel.text = user.country.name
        profileImage.load(url: user.iconUrl)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
        self.tabBarController?.tabBar.isHidden = false
    }

}

extension MoreItemsViewController: LogOutDelegate {
    func proceed() {
        DealsKeyValueRepository.sharedInstance.logOut()
        LoginViewController.launchStandAlone()
        self.showInfo(message: ErrorMessages.loggedOut)
    }
}

extension MoreItemsViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.titles.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: R.nib.moreItemsTableViewCell.identifier, for: indexPath) as! MoreItemsTableViewCell
        
        cell.setupCells(text: viewModel.titles[indexPath.row], image: viewModel.images[indexPath.row])
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.row {
        case 0:
            self.tabBarController?.tabBar.isHidden = true
            NotificationsViewController.launch(self)
        case 1:
            self.tabBarController?.tabBar.isHidden = true
            WishListViewController.launch(self)
        case 2:
            self.tabBarController?.tabBar.isHidden = true
            HistoryViewController.launch(self, selectedHistory: .deals)
        case 3:
            self.tabBarController?.tabBar.isHidden = true
            ChangePinViewController.launch(self)
        case 4:
            self.tabBarController?.tabBar.isHidden = true
            SwitchCountryViewController.launch(self)
        case 5:
            self.tabBarController?.tabBar.isHidden = true
            QrCodeViewController.launch(self)
        case 6:
            self.tabBarController?.tabBar.isHidden = true
            ReferralViewController.launch(self)
        case 7:
            self.tabBarController?.tabBar.isHidden = true
            FAQViewController.launch(self)
        case 8:
            self.tabBarController?.tabBar.isHidden = true
            ContactViewController.launch(self)
        case 9:
            self.tabBarController?.tabBar.isHidden = true
            TermsAndConditionsViewController.launch(self)
        case 10:
            self.tabBarController?.tabBar.isHidden = true
            PrivacyPolicyViewController.launch(self)
        case 11:
            self.tabBarController?.tabBar.isHidden = true
            RefundPolicyViewController.launch(self)
        default:
            return
        }
    }
    
}
