import UIKit

class UserAddressViewController: BaseViewController {
    
    @IBOutlet weak var firstNameField: DealsInputField!
    @IBOutlet weak var lastNameField: DealsInputField!
    @IBOutlet weak var numberField: DealsPhoneNumberField!
    @IBOutlet weak var number2Field: DealsPhoneNumberField!
    @IBOutlet weak var address1Field: DealsInputField!
    @IBOutlet weak var address2Field: DealsInputField!
    @IBOutlet weak var stateField: DealsSpinnerField!
    @IBOutlet weak var cityField: DealsSpinnerField!
    @IBOutlet weak var locationTypeField: DealsSpinnerField!
    @IBOutlet weak var btn: UIView!
    
    let viewModel = LocationViewModel(datasource: LocationRepository())
    
    var states = [State]()
    var cities = [City]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        stateField.delegate = self
        cityField.delegate = self
        locationTypeField.delegate = self
        bindVM()
        addGestures()
    }
    
    private func addGestures(){
        btn.addTapGestureRecognizer {  [self] in
            let validator = Validator(self)
            if validator.textValidation(firstNameField.content, ErrorMessages.fieldError("first name")) &&
                validator.textValidation(lastNameField.content, ErrorMessages.fieldError("last name")) &&
                validator.phoneNumberValidation(numberField.content) &&
                validator.textValidation(address1Field.content, ErrorMessages.fieldError("address")) &&
                validator.textValidation(stateField.content, ErrorMessages.fieldError("state")) &&
                validator.textValidation(cityField.content, ErrorMessages.fieldError("city")) &&
                validator.textValidation(locationTypeField.content, ErrorMessages.fieldError("location")) {
                viewModel.saveAddress(name: "\(firstNameField.content) \(lastNameField.content)", address: address1Field.content, address2: address2Field.content, number: numberField.content, number2: number2Field.content, state: states[stateField.selectedIndex], city: cities[cityField.selectedIndex])
            }
        }
    }
    
    private func bindVM(){
        viewModel.getStates()
        
        viewModel.error.bind { [unowned self] error in
            self.showError(message: error)
        }.disposed(by: disposeBag)
        
        viewModel.progress.bind { [unowned self] loading in
            loading ? self.showProgressIndicator() : self.dismissProgressIndicator()
        }.disposed(by: disposeBag)
        
        viewModel.statesResponse.bind { states in
            self.states = states
            self.stateField.spinnerData = states.map({ (item) in
                "\(item.name) State"
            })
        }.disposed(by: disposeBag)
        
        viewModel.citiesResponse.bind { cities in
            self.cities = cities
            self.cityField.spinnerData = cities.map({ (item) in
                item.name
            })
        }.disposed(by: disposeBag)
        
        viewModel.successMessage.bind { [unowned self] message in
            self.showSuccess(message: message)
            self.navigationController?.popViewController(animated: true)
        }.disposed(by: disposeBag)
        
        locationTypeField.spinnerData = ["Home", "Work", "Others"]
    }
}

extension UserAddressViewController: DealsSpinnerDelegate {
    func onItemSelected(dealsSpinnerfield: DealsSpinnerField, selectedIndex: Int, text: String) {
        switch dealsSpinnerfield {
        case stateField:
            cityField.spinnerData = []
            cityField.content = ""
            guard selectedIndex < states.count else {return}
            viewModel.getCities(state: states[selectedIndex])
        default:
            return
        }
    }
}

extension UserAddressViewController {
    static func launch(_ caller: UIViewController) {
        let controller = R.storyboard.moreItems.userAddressViewController()!
        caller.navigationController?.pushViewController(controller, animated: true)
    }
}
