import UIKit
import FreshchatSDK

class ProfileViewController: BaseViewController {
    
    @IBOutlet weak var nameField: DealsInputField!
    @IBOutlet weak var emailField: DealsInputField!
    @IBOutlet weak var genderField: DealsSpinnerField!
    @IBOutlet weak var dobField: DealsDateField!
    @IBOutlet weak var numberField: DealsPhoneNumberField!
    
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var changeProfile: UIView!

    private var viewModel : MoreItemsViewModel = MoreItemsViewModel(datasource: MoreItemsRepository())

    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        genderField.spinnerData = ["Female", "Male", "Non-binary", "Prefer not to say"]
        changeProfile.addTapGestureRecognizer { [self] in
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = .photoLibrary
            imagePicker.allowsEditing = true
            imagePicker.allowsEditing = false
            present(imagePicker, animated: true, completion: nil)
        }
        bindVM()
        setupNavButtons()
    }
    
    func setupNavButtons(){
        
        let menu = UIImageView(image: R.image.tabHome()!)
        menu.frame = CGRect(x: 0, y: 0, width: 24, height: 24)
        menu.addTapGestureRecognizer {
            self.tabBarController?.tabBar.isHidden = true
            UserAddressViewController.launch(self)
        }
        let addressBTN = UIBarButtonItem(customView: menu)
        self.navigationItem.rightBarButtonItem = addressBTN
    }
    
    private func bindVM(){
        viewModel.error.bind { [unowned self] error in
            self.showError(message: error)
        }.disposed(by: disposeBag)
        
        viewModel.progress.bind { [unowned self] loading in
            loading ? self.showProgressIndicator() : self.dismissProgressIndicator()
        }.disposed(by: disposeBag)
        
        viewModel.successMessage.bind { message in
            self.showSuccess(message: message)
        }.disposed(by: disposeBag)
        
        viewModel.updateAccountResponse.bind { response in
            if response != nil {
             HomeTabBarController.launch()
            }
        }.disposed(by: disposeBag)
    }
    
    
    private func setupUI(){
        guard let user = DealsKeyValueRepository.sharedInstance.getUser() else {return}
        nameField.content = user.name
        emailField.content = user.emailAddress
        profileImage.load(url: user.iconUrl)
    }
    
    @IBAction func updateProfileTapped(_ sender: DealsButton) {
        viewModel.updateAccount(name: nameField.content, gender: genderField.content)
    }

}

extension ProfileViewController: UINavigationControllerDelegate, UIImagePickerControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        picker.dismiss(animated: true, completion: nil)
        if let pickedImage = info[.originalImage] as? UIImage {
            profileImage.image = pickedImage
        } else {
            self.showError(message: ErrorMessages.imageFileError)
        }
      }
    
}

extension ProfileViewController {
    static func launch(_ caller: UIViewController) {
        let controller = R.storyboard.moreItems.profileViewController()!
        caller.navigationController?.pushViewController(controller, animated: true)
    }
}
