import UIKit

protocol LogOutDelegate {
    func proceed()
}

class LogOutViewController: UIViewController {

    @IBOutlet weak var cancel: UIImageView!
    private var delegate: LogOutDelegate!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        cancel.addTapGestureRecognizer {
            self.dismiss(animated: true)
        }
    }
    
    @IBAction func logOutTapped(_ sender: DealsButton) {
        self.dismiss(animated: true) {
            self.delegate.proceed()
        }
    }
    
    static func show(_ caller: UIViewController, delegate: LogOutDelegate){
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        //for ipad
        if let popoverController = alert.popoverPresentationController {
         popoverController.sourceView = caller.view
         popoverController.sourceRect = CGRect(x: caller.view.bounds.midX, y: caller.view.bounds.midY, width: 0, height: 0)
         popoverController.permittedArrowDirections = []
        }
        
        let controller = R.storyboard.moreItems.logOutViewController()!
        controller.delegate = delegate
        controller.preferredContentSize.height = 300
        let cancelAction = UIAlertAction(title: "Cancel", style: .default){ action in
            controller.dismiss(animated: true)
        }
        cancelAction.setValue(R.color.deepPurple()!, forKey: "titleTextColor")
        alert.setValue(controller, forKey: "contentViewController")
        controller.view.widthAnchor.constraint(equalToConstant: controller.view.frame.width - 16).isActive = true
        alert.addAction(cancelAction)
        if let navController = caller.navigationController{
          navController.present(alert, animated: true)
        }
      }
  
}
