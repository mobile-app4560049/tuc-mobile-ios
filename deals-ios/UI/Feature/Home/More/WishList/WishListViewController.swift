import UIKit

class WishListViewController: BaseViewController {

    @IBOutlet weak var topCollection: UICollectionView!
    @IBOutlet weak var wishTable: UITableView!
    
    private var viewModel = MoreItemsViewModel(datasource: MoreItemsRepository())
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Selected History type collection View
        topCollection.dataSource = self
        topCollection.delegate = self
        topCollection.register(UINib(nibName: R.nib.historyCollectionViewCell.name, bundle: nil), forCellWithReuseIdentifier: R.nib.historyCollectionViewCell.name)
        
        wishTable.delegate = self
        wishTable.dataSource = self
        wishTable.register(UINib(nibName: R.nib.dealsHistoryTableViewCell.name, bundle: nil), forCellReuseIdentifier: R.nib.dealsHistoryTableViewCell.identifier)
    }

}

extension WishListViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        viewModel.wishListCategory.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: R.nib.historyCollectionViewCell.name, for: indexPath) as! HistoryCollectionViewCell
        cell.cellLabel.text = viewModel.wishListCategory[indexPath.row]
        cell.backgroundColor = R.color.brightPurple()!
        cell.cellLabel.textColor = R.color.white()!
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        // Calculates the size of a label with text and sets the cell width to that size
        let label = UILabel()
        label.text = viewModel.wishListCategory[indexPath.row]
        let labelSize = label.intrinsicContentSize
        
        let cellSize = CGSize(width: labelSize.width + 32, height: 32)
        return cellSize
    }
    
}

extension WishListViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: R.nib.dealsHistoryTableViewCell.identifier, for: indexPath) as! DealsHistoryTableViewCell
        cell.addTapGestureRecognizer {
            self.tabBarController?.tabBar.isHidden = true
            DealHistoryDetailsViewController.launch(self)
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120
    }
    
}

extension WishListViewController {
    static func launch(_ caller: UIViewController) {
        let controller = R.storyboard.moreItems.wishListViewController()!
        caller.navigationController?.pushViewController(controller, animated: true)
    }
}
