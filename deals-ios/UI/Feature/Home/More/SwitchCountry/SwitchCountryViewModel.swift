import UIKit
import RxSwift
import RxCocoa

class SwitchCountryViewModel: BaseViewModel {
       
    var switchCountryResponse = BehaviorRelay<SelectedCountry?>(value: nil)
    let successMessage = PublishSubject<String>()
    
    /// This function changes the current country of a user
    ///
    /// Returns a `Country` Object
    func switchCountry(country: SelectedCountry){
        DealsKeyValueRepository.sharedInstance.saveUserCountry(country)
        switchCountryResponse.accept(country)
        successMessage.onNext("Country changed successfully!")
    }
    
}
