import UIKit

class SwitchCountryViewController: BaseViewController {

    @IBOutlet weak var nigeria: UIView!
    @IBOutlet weak var ghana: UIView!
    @IBOutlet weak var kenya: UIView!
    @IBOutlet weak var uganda: UIView!
    @IBOutlet weak var rwanda: UIView!
    
    @IBOutlet weak var nigeriaLabel: UILabel!
    @IBOutlet weak var ghanaLabel: UILabel!
    @IBOutlet weak var kenyaLabel: UILabel!
    @IBOutlet weak var ugandaLabel: UILabel!
    @IBOutlet weak var rwandaLabel: UILabel!
    
    private var viewModel : SwitchCountryViewModel = SwitchCountryViewModel()
    var selectedCountry: SelectedCountry?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addGestures()
        bindVM()
    }
    
    private func bindVM(){
        viewModel.error.bind { [unowned self] error in
            self.showError(message: error)
        }.disposed(by: disposeBag)
        
        viewModel.progress.bind { [unowned self] loading in
            loading ? self.showProgressIndicator() : self.dismissProgressIndicator()
        }.disposed(by: disposeBag)
        
        viewModel.successMessage.bind { message in
            self.showSuccess(message: message)
        }.disposed(by: disposeBag)
        
        viewModel.switchCountryResponse.bind { country in
            if country != nil {
                self.navigationController?.popViewController(animated: true)
            }
        }.disposed(by: disposeBag)
    }
    
    private func addGestures(){
        nigeria.addTapGestureRecognizer {
            self.selectCountry(nigeria: true)
            self.selectedCountry = .nigeria
        }
        ghana.addTapGestureRecognizer {
            self.selectCountry(ghana: true)
            self.selectedCountry = .ghana
        }
        kenya.addTapGestureRecognizer {
            self.selectCountry(kenya: true)
            self.selectedCountry = .kenya
        }
        uganda.addTapGestureRecognizer {
            self.selectCountry(uganda: true)
            self.selectedCountry = .uganda
        }
        rwanda.addTapGestureRecognizer {
            self.selectCountry(rwanda: true)
            self.selectedCountry = .rwanda
        }
    }
    
    func selectCountry(nigeria: Bool = false, ghana: Bool = false, kenya: Bool = false, uganda: Bool = false, rwanda: Bool = false){
        self.nigeria.backgroundColor = nigeria ? R.color.brightPurple()! : R.color.white()!
        self.ghana.backgroundColor = ghana ? R.color.brightPurple()! : R.color.white()!
        self.kenya.backgroundColor = kenya ? R.color.brightPurple()! : R.color.white()!
        self.uganda.backgroundColor = uganda ? R.color.brightPurple()! : R.color.white()!
        self.rwanda.backgroundColor = rwanda ? R.color.brightPurple()! : R.color.white()!
        
        self.nigeriaLabel.textColor = nigeria ? R.color.white()! : R.color.textfieldTitleColor()!
        self.ghanaLabel.textColor = ghana ? R.color.white()! : R.color.textfieldTitleColor()!
        self.kenyaLabel.textColor = kenya ? R.color.white()! : R.color.textfieldTitleColor()!
        self.ugandaLabel.textColor = uganda ? R.color.white()! : R.color.textfieldTitleColor()!
        self.rwandaLabel.textColor = rwanda ? R.color.white()! : R.color.textfieldTitleColor()!
    }
    
    @IBAction func changeCountryTapped(_ sender: DealsButton) {
        if let selectedCountry = selectedCountry {
            viewModel.switchCountry(country: selectedCountry)
        } else {
            self.showError(message: ErrorMessages.selectCountry)
        }
    }

}

extension SwitchCountryViewController {
    static func launch(_ caller: UIViewController) {
        let controller = R.storyboard.moreItems.switchCountryViewController()!
        caller.navigationController?.pushViewController(controller, animated: true)
    }
}
