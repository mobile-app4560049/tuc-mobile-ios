import UIKit
import RxSwift
import RxCocoa

class MoreItemsViewModel: BaseViewModel {
    let images : [UIImage] = [
        R.image.moreNotifications()!,
        R.image.moreWishlist()!,
        R.image.moreHistory()!,
        R.image.moreChangePin()!,
        R.image.moreSwitchCountry()!,
        R.image.moreQrCode()!,
        R.image.moreReferFriend()!,
        R.image.moreFaqs()!,
        R.image.moreContactUs()!,
        R.image.moreTermsConditions()!,
        R.image.moreTermsConditions()!,
        R.image.moreTermsConditions()!,
    ]
    
    let titles : [String] = [
        "Notifications",
        "Wishlist",
        "History",
        "Change PIN",
        "Switch Country",
        "Scan QR Code",
        "Refer a friend",
        "FAQ",
        "Contact Us",
        "Terms and Conditions",
        "Privacy Policy",
        "Refund Policy"
    ]
    
    let wishListCategory: [String] = ["Saved Items"]
    
    
    private let datasource: MoreItemsDataSource
    
    var updateAccountResponse = BehaviorRelay<String?>(value: nil)
    var getUserAccountResponse = BehaviorRelay<User?>(value: nil)
    var changePinResponse = BehaviorRelay<PinModel?>(value: nil)
    var updateReferralIdResponse = BehaviorRelay<EmptyResponse?>(value: nil)

    let successMessage = PublishSubject<String>()
    
    init(datasource: MoreItemsDataSource) {
        self.datasource = datasource
    }
    
    /// This function gets the details of an already logged in user
    ///
    /// Returns a `User` Object
    func getUserAccount(){
        progress.accept(true)
        datasource.getUserAccount().subscribeOn(MainScheduler.instance).subscribe(onNext: {[weak self] (response) in
            
            guard let self = self else {return}
            self.progress.accept(false)
            
            if let loggedInUser = response.result {
                self.getUserAccountResponse.accept(loggedInUser)
                self.successMessage.onNext(response.message)
            } else {
                self.error.onNext(response.message)
            }
            
        }, onError: { [weak self] (error) in
            guard let self = self else {return}

            self.progress.accept(false)
            if let error = error as? ApiError {
                self.error.onNext(error.message)
            } else {
                self.error.onNext(error.localizedDescription)
            }
        }).disposed(by: disposeBag)
    }
    
    
    /// This function updates account details of a user
    ///
    /// Returns a `User` Object
    func updateAccount(name: String?, gender: String?){
        progress.accept(true)
        datasource.updateAccount(name: name, genderCode: gender).subscribeOn(MainScheduler.instance).subscribe(onNext: {[weak self] (response) in
            
            guard let self = self else {return}
            self.progress.accept(false)
            
            if let loggedInUser = response.result {
                self.updateAccountResponse.accept(loggedInUser)
                self.successMessage.onNext(response.message)
            } else {
                self.error.onNext(response.message)
            }
            
        }, onError: { [weak self] (error) in
            guard let self = self else {return}

            self.progress.accept(false)
            if let error = error as? ApiError {
                self.error.onNext(error.message)
            } else {
                self.error.onNext(error.localizedDescription)
            }
        }).disposed(by: disposeBag)
    }
    
    
    /// This function changes the login pin of an already existing and logged in user
    ///
    /// Returns a `String` Object
    func changePin(oldPin: String, newPin: String){
        progress.accept(true)
        datasource.changePin(oldPin: oldPin, newPin: newPin).subscribeOn(MainScheduler.instance).subscribe(onNext: {[weak self] (response) in
            
            guard let self = self else {return}
            self.progress.accept(false)
            
            if let pinResp = response.result {
                self.changePinResponse.accept(pinResp)
                self.successMessage.onNext(response.message)
            } else {
                self.error.onNext(response.message)
            }
            
        }, onError: { [weak self] (error) in
            guard let self = self else {return}

            self.progress.accept(false)
            if let error = error as? ApiError {
                self.error.onNext(error.message)
            } else {
                self.error.onNext(error.localizedDescription)
            }
        }).disposed(by: disposeBag)
    }
    
    /// This function updates a user's referral id
    ///
    /// Returns a `EmptyResponse` Object
    func updateReferralId(referralID: String){
        progress.accept(true)
        datasource.updateRefId(referralId: referralID).subscribe(onNext: {[weak self] (response) in
            
            guard let self = self else {return}
            self.progress.accept(false)
            
            if response.status.lowercased() == "success" {
                self.updateReferralIdResponse.accept(response)
                self.successMessage.onNext(response.message)
            } else {
                self.error.onNext(response.message)
            }
            
        }, onError: { [weak self] (error) in
            guard let self = self else {return}
            
            self.progress.accept(false)
            if let error = error as? ApiError {
                self.error.onNext(error.message)
            } else {
                self.error.onNext(error.localizedDescription)
            }
        }).disposed(by: disposeBag)
    }
    
    
}
