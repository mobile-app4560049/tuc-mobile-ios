import Foundation
import RxSwift
import RxCocoa

class LocationViewModel: BaseViewModel {
    private let datasource: LocationDataSource
    
    init(datasource: LocationDataSource) {
        self.datasource = datasource
    }
    
    let successMessage = PublishSubject<String>()
    var statesResponse = BehaviorRelay<[State]>(value: [])
    var citiesResponse = BehaviorRelay<[City]>(value: [])
    var userAddressesResponse = BehaviorRelay<[UserAddress]>(value: [])
    
    /// This function gets states
    ///
    /// Returns a `[State]` Object
    func getStates() {
        progress.accept(true)
        datasource.getStates().observeOn(MainScheduler.instance).subscribe { [weak self] (response)in
            guard let self = self else {return}
            self.progress.accept(false)
            if let states = response.result?.data {
                self.statesResponse.accept(states)
            } else {
                self.error.onNext(response.message)
            }
        } onError: { [weak self] (error) in
            guard let self = self else {return}
            self.progress.accept(false)
            if let error = error as? ApiError {
                self.error.onNext(error.message)
            } else {
                self.error.onNext(error.localizedDescription)
            }
        }.disposed(by: disposeBag)
    }
    
    /// This function gets cities for a state
    ///
    /// Returns a `[City]` Object
    func getCities(state: State) {
        progress.accept(true)
        datasource.getCities(state: state).observeOn(MainScheduler.instance).subscribe { [weak self] (response)in
            guard let self = self else {return}
            self.progress.accept(false)
            if let states = response.result?.data {
                self.citiesResponse.accept(states)
            } else {
                self.error.onNext(response.message)
            }
        } onError: { [weak self] (error) in
            guard let self = self else {return}
            self.progress.accept(false)
            if let error = error as? ApiError {
                self.error.onNext(error.message)
            } else {
                self.error.onNext(error.localizedDescription)
            }
        }.disposed(by: disposeBag)
    }
    
    /// This function saves an address for a user
    ///
    /// Returns a `EmptyResponse` Object
    func saveAddress(name: String, address: String, address2: String, number: String, number2: String, state: State, city: City) {
        progress.accept(true)
        datasource.saveAddress(name: name, address: address, address2: address2, number: number, number2: number2, state: state, city: city).observeOn(MainScheduler.instance).subscribe { [weak self] (response)in
            guard let self = self else {return}
            self.progress.accept(false)
            if response.status.lowercased() == "success" {
                self.successMessage.onNext(response.message)
            }
        } onError: { [weak self] (error) in
            guard let self = self else {return}
            self.progress.accept(false)
            if let error = error as? ApiError {
                self.error.onNext(error.message)
            } else {
                self.error.onNext(error.localizedDescription)
            }
        }.disposed(by: disposeBag)
    }
    
    /// This function gets address for a user
    ///
    /// Returns a `[UserAddress]` Object
    func getUserAddresses(offSet: Int) {
        progress.accept(true)
        datasource.getUserAddressList(offset: offSet).observeOn(MainScheduler.instance).subscribe { [weak self] (response)in
            guard let self = self else {return}
            self.progress.accept(false)
            if let addresses = response.result?.data {
                self.userAddressesResponse.accept(addresses)
            } else {
                self.error.onNext(response.message)
            }
        } onError: { [weak self] (error) in
            guard let self = self else {return}
            self.progress.accept(false)
            if let error = error as? ApiError {
                self.error.onNext(error.message)
            } else {
                self.error.onNext(error.localizedDescription)
            }
        }.disposed(by: disposeBag)
    }
    
    
}
