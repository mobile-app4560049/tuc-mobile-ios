import UIKit

class FAQViewController: BaseViewController {

    @IBOutlet weak var faqTable: UITableView!
    
    let viewModel = FaqViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        faqTable.delegate = self
        faqTable.dataSource = self
        faqTable.register(UINib(nibName: R.nib.faqTableViewCell.name, bundle: nil), forCellReuseIdentifier: R.nib.faqTableViewCell.identifier)
    }


}

extension FAQViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.faqs.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: R.nib.faqTableViewCell.identifier, for: indexPath) as! FaqTableViewCell
        cell.setupCell(question: viewModel.faqs[indexPath.row].question, answer: viewModel.faqs[indexPath.row].answer)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        UIView.animate(withDuration: 0.3) {
            self.faqTable.performBatchUpdates(nil)
        }
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        guard let cell = faqTable.cellForRow(at: indexPath) as? FaqTableViewCell else {return}
        cell.hideAnswer()
    }
}

extension FAQViewController {
    static func launch(_ caller: UIViewController) {
        let controller = R.storyboard.moreItems.faqViewController()!
        caller.navigationController?.pushViewController(controller, animated: true)
    }
}
