import UIKit
import WebKit

class PrivacyPolicyViewController: BaseViewController {

    @IBOutlet weak var webView: WKWebView!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.showProgressIndicator()
        webView.uiDelegate = self
        webView.navigationDelegate = self
        guard let url = URL(string: Constants.privacyPolicy) else {
            self.dismissProgressIndicator()
            self.showError(message: ErrorMessages.invalidURL)
            return
        }
        webView.load(URLRequest(url: url))
        
    }
}

extension PrivacyPolicyViewController: WKUIDelegate, WKNavigationDelegate {
    
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        self.dismissProgressIndicator()
        self.showError(message: error.localizedDescription)
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        self.dismissProgressIndicator()
    }
    
    func webView(_ webView: WKWebView, didFailProvisionalNavigation navigation: WKNavigation!, withError error: Error) {
        self.dismissProgressIndicator()
        self.showError(message: error.localizedDescription)
    }
}

extension PrivacyPolicyViewController {
    static func launch(_ caller: UIViewController) {
        let controller = R.storyboard.moreItems.privacyPolicyViewController()!
        caller.navigationController?.pushViewController(controller, animated: true)
    }
}
