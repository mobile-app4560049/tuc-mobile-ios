import UIKit
import FreshchatSDK

class HomeViewController: BaseViewController {
    
    @IBOutlet weak var topView: UIView!
    
    @IBOutlet weak var walletBalance: UILabel!
    @IBOutlet weak var hideBalance: UIImageView!
    @IBOutlet weak var notifications: UIImageView!
    
    @IBOutlet weak var fundWallet: UIImageView!
    @IBOutlet weak var topDeals: UIImageView!
    @IBOutlet weak var rewards: UIImageView!
    
    @IBOutlet weak var airtime: UIView!
    @IBOutlet weak var data: UIView!
    @IBOutlet weak var utilityBills: UIView!
    @IBOutlet weak var more: UIView!
    
    @IBOutlet weak var seeAllFlashDeals: UILabel!
    @IBOutlet weak var flashDealsCollection: UICollectionView!
    
    @IBOutlet weak var seeAllFeaturedDeals: UILabel!
    @IBOutlet weak var featuredDealsCollection: UICollectionView!
    
    @IBOutlet weak var flashTimer: UILabel!
    
    private let viewModel: DealsViewModel = DealsViewModel(datasource: DealsRepository())
    private var flashDealsList = [Deal]()
    private var featuredDealsList = [Deal]()

    override func viewDidLoad() {
        super.viewDidLoad()

        topView.dropShadow(opacity: 0.5)
        
        flashDealsCollection.delegate = self
        featuredDealsCollection.delegate = self
        
        flashDealsCollection.register(UINib(nibName: R.nib.dealsCollectionViewCell.name, bundle: nil), forCellWithReuseIdentifier: R.nib.dealsCollectionViewCell.name)
        featuredDealsCollection.register(UINib(nibName: R.nib.dealsCollectionViewCell.name, bundle: nil), forCellWithReuseIdentifier: R.nib.dealsCollectionViewCell.name)
        addGestures()
        bindVm()
    }
    
    private func bindVm(){
        viewModel.getFlashDeals(page: 1, firstLoad: true)
        viewModel.getFeaturedDeals(page: 10, firstLoad: true)
        
        viewModel.error.bind { [unowned self] error in
            self.showError(message: error)
        }.disposed(by: disposeBag)
        
        viewModel.progress.bind { [unowned self] loading in
            loading ? self.showProgressIndicator() : self.dismissProgressIndicator()
        }.disposed(by: disposeBag)
        
        viewModel.flashDealsResponse.bind(to: flashDealsCollection.rx.items(cellIdentifier: R.nib.dealsCollectionViewCell.identifier)) {  [weak self]
            (index, item: Deal, cell: DealsCollectionViewCell) in
            guard let self = self else {return}
            self.flashDealsList.append(item)
            cell.configure(with: item)
            cell.addTapGestureRecognizer {
                DealDetailsViewController.launch(self, deal: item)
            }
        }.disposed(by: disposeBag)
        
        viewModel.featuredDealsResponse.bind(to: featuredDealsCollection.rx.items(cellIdentifier: R.nib.dealsCollectionViewCell.identifier)) {  [weak self]
            (index, item: Deal, cell: DealsCollectionViewCell) in
            guard let self = self else {return}
            self.flashDealsList.append(item)
            cell.configure(with: item)
            cell.addTapGestureRecognizer {
                DealDetailsViewController.launch(self, deal: item)
            }
        }.disposed(by: disposeBag)
        
        viewModel.flashDealsResponse.bind { deals in
            if !self.viewModel.progress.value && !self.viewModel.pageProgress.value && deals.isEmpty {
                self.flashDealsCollection.setEmptyString(ErrorMessages.noDealFound)
            }
        }.disposed(by: disposeBag)
        
        viewModel.featuredDealsResponse.bind { deals in
            if !self.viewModel.progress.value && !self.viewModel.pageProgress.value && deals.isEmpty {
                self.featuredDealsCollection.setEmptyString(ErrorMessages.noDealFound)
            }
        }.disposed(by: disposeBag)
    }
    
    func addGestures() {
        fundWallet.addTapGestureRecognizer {
            self.tabBarController?.tabBar.isHidden = true
            FundWalletViewController.launch(self)
        }
        hideBalance.addTapGestureRecognizer { [self] in
            guard let _ = DealsKeyValueRepository.sharedInstance.getUser() else {return}
            if walletBalance.text == "• • • • • • •" {
                walletBalance.text = "₦ 30,000.00"
                hideBalance.image = R.image.dashboardWalletVisible()!
            } else {
                walletBalance.text = "• • • • • • •"
                hideBalance.image = R.image.dashboardWalletInvisible()!
            }
        }
        topDeals.addTapGestureRecognizer {
            self.tabBarController?.tabBar.isHidden = true
            TopDealsViewController.launch(self)
        }
        rewards.addTapGestureRecognizer {
            self.tabBarController?.tabBar.isHidden = true
            RewardCardsViewController.launch(self)
        }
        
        seeAllFlashDeals.addTapGestureRecognizer {
            self.tabBarController?.tabBar.isHidden = true
            FlashDealsViewController.launch(self)
        }
        airtime.addTapGestureRecognizer {
            self.tabBarController?.tabBar.isHidden = true
            TopUpViewController.launch(self)
        }
        data.addTapGestureRecognizer {
            self.tabBarController?.tabBar.isHidden = true
            TopUpViewController.launch(self, topUpType: .data)
        }
        seeAllFeaturedDeals.addTapGestureRecognizer {
            self.tabBarController?.tabBar.isHidden = true
            FeaturedDealsViewController.launch(self)
        }
        notifications.addTapGestureRecognizer {
            self.tabBarController?.tabBar.isHidden = true
            NotificationsViewController.launch(self)
        }
        more.addTapGestureRecognizer {
            self.showError(message: "No screen available")
        }
        utilityBills.addTapGestureRecognizer {
            self.tabBarController?.tabBar.isHidden = true
            UtilityBillsViewController.launch(self)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
        self.tabBarController?.tabBar.isHidden = false
    }
    
}

extension HomeViewController: UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 250, height: 300)
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        
        if collectionView == flashDealsCollection {
            guard viewModel.flashDealsResponse.value.count >= 10 else {return}
            if indexPath.row == viewModel.flashDealsResponse.value.count - 1 {
                viewModel.getTopDeals(page: viewModel.flashDealsResponse.value.count + 10)
            }
        } else {
            guard viewModel.featuredDealsResponse.value.count >= 10 else {return}
            if indexPath.row == viewModel.featuredDealsResponse.value.count - 1 {
                viewModel.getTopDeals(page: viewModel.featuredDealsResponse.value.count + 10)
            }
        }
    }
    
}

extension HomeViewController {
    static func launch(_ caller: UIViewController) {
        let controller = R.storyboard.home.homeViewController()!
        let nav = DealsNavController(rootViewController: controller)
        controller.navigationController?.isNavigationBarHidden = true
        nav.modalPresentationStyle = .fullScreen
        caller.present(nav, animated: true)
    }
}
