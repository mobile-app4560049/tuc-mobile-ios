import UIKit


class HomeTabBarController: UITabBarController {
  override func viewDidLoad() {
    super.viewDidLoad()
    setTabBarIcons()
  }
  override func viewWillAppear(_ animated: Bool) {
    setTabBarIcons()
  }
  private func setTabBarIcons() {
    if #available(iOS 13.0, *) {
      if let tabBarItem1 = self.tabBarController?.tabBar.items?[0] {
        tabBarItem1.image = R.image.tabHome()!.withRenderingMode(.alwaysOriginal)
      }
      if let tabBarItem2 = self.tabBarController?.tabBar.items?[1] {
        tabBarItem2.image = R.image.tabDeals()!.withRenderingMode(.alwaysOriginal)
      }
      if let tabBarItem3 = self.tabBarController?.tabBar.items?[2] {
        tabBarItem3.image = R.image.tabHistory()!.withRenderingMode(.alwaysOriginal)
      }
      if let tabBarItem3 = self.tabBarController?.tabBar.items?[3] {
        tabBarItem3.image = R.image.tabProfile()!.withRenderingMode(.alwaysOriginal)
      }
    }
  }
  static func launch() {
    if let dashboardVC = R.storyboard.home.homeTabBarController(),
      let window = UIApplication.shared.keyWindow {
      window.rootViewController = dashboardVC
    }
  }
}
