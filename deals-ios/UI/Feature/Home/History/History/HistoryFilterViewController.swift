import UIKit

protocol HistoryFilterDelegate {
    func filter()
}

class HistoryFilterViewController: UIViewController {

    var filterDelegate: HistoryFilterDelegate!
    @IBOutlet weak var close: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        close.addTapGestureRecognizer {
            self.dismiss(animated: true)
        }
    }
    
    @IBAction func filterTapped(_ sender: DealsButton) {
        self.dismiss(animated: true){
            self.filterDelegate.filter()
        }
    }
    
}

extension HistoryFilterViewController {
    static func launch(_ caller: UIViewController, filterDelegate: HistoryFilterDelegate) {
        let controller = R.storyboard.history.historyFilterViewController()!
        controller.filterDelegate = filterDelegate
        if let sheet = controller.sheetPresentationController {
            sheet.detents = [.medium(), .large()]
            sheet.preferredCornerRadius = 20
        }
        caller.present(controller, animated: true)
    }
}
