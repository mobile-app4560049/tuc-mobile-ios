import UIKit

class HistoryViewController: BaseViewController {
    
    @IBOutlet weak var topCollection: UICollectionView!
    
    @IBOutlet weak var dealsHistoryTable: UITableView!
    @IBOutlet weak var billPaymentHistoryTable: UITableView!
    @IBOutlet weak var rewardHistoryTable: UITableView!
    @IBOutlet weak var topUpHistoryTable: UITableView!

    
    var fromHome: Bool = true
    let viewModel = HistoryViewModel()
    
    var selectedHistory: HistoryType = .deals
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavButton()
        
        // Selected History type collection View
        topCollection.dataSource = self
        topCollection.delegate = self
        topCollection.register(UINib(nibName: R.nib.historyCollectionViewCell.name, bundle: nil), forCellWithReuseIdentifier: R.nib.historyCollectionViewCell.name)
        
        // Topup history table
        topUpHistoryTable.dataSource = self
        topUpHistoryTable.delegate = self
        topUpHistoryTable.register(UINib(nibName: R.nib.historyTableViewCell.name, bundle: nil), forCellReuseIdentifier: R.nib.historyTableViewCell.identifier)
        
        //Rewards History table
        rewardHistoryTable.dataSource = self
        rewardHistoryTable.delegate = self
        rewardHistoryTable.register(UINib(nibName: R.nib.historyTableViewCell.name, bundle: nil), forCellReuseIdentifier: R.nib.historyTableViewCell.identifier)
        
        //Deals History table
        dealsHistoryTable.dataSource = self
        dealsHistoryTable.delegate = self
        dealsHistoryTable.register(UINib(nibName: R.nib.dealsHistoryTableViewCell.name, bundle: nil), forCellReuseIdentifier: R.nib.dealsHistoryTableViewCell.identifier)
        
        //Rewards History table
        billPaymentHistoryTable.dataSource = self
        billPaymentHistoryTable.delegate = self
        billPaymentHistoryTable.register(UINib(nibName: R.nib.historyTableViewCell.name, bundle: nil), forCellReuseIdentifier: R.nib.historyTableViewCell.identifier)
        
        showTable(historyType: selectedHistory)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.title = "History"
        self.navigationController?.isNavigationBarHidden = false
        if fromHome{
            self.tabBarController?.tabBar.isHidden = false
            self.removeBackButton()
        } else {
            self.tabBarController?.tabBar.isHidden = true
        }
    }
    
    func showTable(historyType: HistoryType) {
        switch historyType {
        case .deals:
            dealsHistoryTable.isHidden = false
            billPaymentHistoryTable.isHidden = true
            rewardHistoryTable.isHidden = true
            topUpHistoryTable.isHidden = true
        case .topUp:
            dealsHistoryTable.isHidden = true
            billPaymentHistoryTable.isHidden = true
            rewardHistoryTable.isHidden = true
            topUpHistoryTable.isHidden = false
        case .rewards:
            dealsHistoryTable.isHidden = true
            billPaymentHistoryTable.isHidden = true
            rewardHistoryTable.isHidden = false
            topUpHistoryTable.isHidden = true
        case .billsPayment:
            dealsHistoryTable.isHidden = true
            billPaymentHistoryTable.isHidden = false
            rewardHistoryTable.isHidden = true
            topUpHistoryTable.isHidden = true
        }
    }
    
    func setupNavButton(){
        
        let menu = UIImageView(image: R.image.dealsFilter()!)
        menu.frame = CGRect(x: 0, y: 0, width: 24, height: 24)
        menu.addTapGestureRecognizer {
            HistoryFilterViewController.launch(self, filterDelegate: self)
        }
        let notiBTN = UIBarButtonItem(customView: menu)
        
        self.navigationItem.rightBarButtonItems = [notiBTN]
    }
    
}

extension HistoryViewController: HistoryFilterDelegate {
    func filter() {
        // Filter history
    }
}

//MARK: Top collection view delegates
extension HistoryViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        viewModel.historyTypes.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: R.nib.historyCollectionViewCell.name, for: indexPath) as! HistoryCollectionViewCell
        cell.cellLabel.text = viewModel.historyTypes[indexPath.row]
        if cell.cellLabel.text == selectedHistory.displayValue {
            cell.backgroundColor = R.color.brightPurple()!
            cell.cellLabel.textColor = R.color.white()!
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        // Calculates the size of a label with text and sets the cell width to that size
        let label = UILabel()
        label.text = viewModel.historyTypes[indexPath.row]
        let labelSize = label.intrinsicContentSize
        
        let cellSize = CGSize(width: labelSize.width + 32, height: 32)
        return cellSize
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        for cell in collectionView.visibleCells {
            if cell.backgroundColor == R.color.brightPurple()! {
                //Changes the color of a previously selected cell back to default (lightPurple) and its text color to black
                cell.backgroundColor = R.color.lightPurple()!
                (cell as? HistoryCollectionViewCell)?.cellLabel.textColor = .black
            }
        }
        
        // Changes the actual selected cell background color and text color
        if let cell = collectionView.cellForItem(at: indexPath) as? HistoryCollectionViewCell{
            cell.backgroundColor = R.color.brightPurple()!
            cell.cellLabel.textColor = R.color.white()!
            
            
            switch cell.cellLabel.text {
            case HistoryType.deals.displayValue:
                showTable(historyType: .deals)
                selectedHistory = .deals
            case HistoryType.billsPayment.displayValue:
                showTable(historyType: .billsPayment)
                selectedHistory = .billsPayment
            case HistoryType.rewards.displayValue:
                showTable(historyType: .rewards)
                selectedHistory = .rewards
            case HistoryType.topUp.displayValue:
                showTable(historyType: .topUp)
                selectedHistory = .topUp
            default:
                return
            }
            
        }
    }
    
}

//MARK: Payment type history Table View delegates
extension HistoryViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch tableView {
            
        case topUpHistoryTable:
            let cell = tableView.dequeueReusableCell(withIdentifier: R.nib.historyTableViewCell.identifier, for: indexPath) as! HistoryTableViewCell
            cell.giftCard.isHidden = true
            return cell
            
        case rewardHistoryTable:
            let cell = tableView.dequeueReusableCell(withIdentifier: R.nib.historyTableViewCell.identifier, for: indexPath) as! HistoryTableViewCell
            cell.setupCell(historyType: "Rewarded at", historyDescription: "Ebeano Lekki", date: "22-07-2023", time: "12:22 pm", price: "20.00")
            let details = RewardHistoryDetail(rewardPoints: "20.00", location: "Ebeano Lekki", transactionStatus: "Successful", paidAmount: "₦1,000", date: "22-09-2023", time: "12:22 pm", referenceID: "", referenceNumber: "", salesInvoiceNumber: "")
            cell.addTapGestureRecognizer {
                self.tabBarController?.tabBar.isHidden = true
                RewardHistoryDetailsViewController.launch(self, details: details)
            }
            return cell
            
        case dealsHistoryTable:
            let cell = tableView.dequeueReusableCell(withIdentifier: R.nib.dealsHistoryTableViewCell.identifier, for: indexPath) as! DealsHistoryTableViewCell
            cell.addTapGestureRecognizer {
                self.tabBarController?.tabBar.isHidden = true
                DealHistoryDetailsViewController.launch(self)
            }
            return cell
            
        case billPaymentHistoryTable:
            let cell = tableView.dequeueReusableCell(withIdentifier: R.nib.historyTableViewCell.identifier, for: indexPath) as! HistoryTableViewCell
            cell.setupCell(historyType: "Airtime Top Up", historyDescription: "NGN 1500 - 1.5GB MTN Data Bundle", date: "27-09-2023", time: "12:22 pm", price: "20.00")
            cell.addTapGestureRecognizer {
                self.tabBarController?.tabBar.isHidden = true
                BillPaymentHistoryDetailsViewController.launch(self)
            }
            return cell
            
        default:
            return UITableViewCell()
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch tableView {
        case dealsHistoryTable:
            return 120
        default:
            return 100
        }
    }
    
}

extension HistoryViewController {
    static func launch(_ caller: UIViewController, selectedHistory: HistoryType) {
        let controller = R.storyboard.home.historyViewController()!
        controller.fromHome = false
        controller.selectedHistory = selectedHistory
        caller.navigationController?.pushViewController(controller, animated: true)
    }
}
