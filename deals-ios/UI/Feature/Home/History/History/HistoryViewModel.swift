import UIKit

enum HistoryType {
    case deals, billsPayment, rewards, topUp
    var displayValue: String {
        switch self {
        case .deals:
            return "Deals"
        case .billsPayment:
            return "Bills Payment"
        case .rewards:
            return "Rewards"
        case .topUp:
            return "Top Up"
        }
    }
}

class HistoryViewModel: BaseViewModel {
  
    let historyTypes: [String] = [
        HistoryType.deals.displayValue,
        HistoryType.billsPayment.displayValue,
        HistoryType.rewards.displayValue,
        HistoryType.topUp.displayValue
    ]
    
    let cardImages: [UIImage] = [R.image.rewardCardOne()!, R.image.rewardCardTwo()!, R.image.rewardCardThree()!]
    
}
