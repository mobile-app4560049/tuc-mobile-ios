import UIKit

class BillPaymentHistoryDetailsViewController: BaseViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = "Airtime History"
    }

}

extension BillPaymentHistoryDetailsViewController {
    static func launch(_ caller: UIViewController) {
        let controller = R.storyboard.history.billPaymentHistoryDetailsViewController()!
//        vc.historyDetails = details
        caller.navigationController?.pushViewController(controller, animated: true)
    }
}
