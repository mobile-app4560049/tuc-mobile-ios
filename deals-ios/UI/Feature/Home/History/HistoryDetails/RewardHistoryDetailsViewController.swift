import UIKit

class RewardHistoryDetailsViewController: BaseViewController {

    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var bottomView: UIView!
    
    @IBOutlet weak var pointLabel: UILabel!
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var amountLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var refIDLabel: UILabel!
    @IBOutlet weak var refNumLabel: UILabel!
    @IBOutlet weak var invoiceLabel: UILabel!

    var historyDetails: RewardHistoryDetail!
    override func viewDidLoad() {
        super.viewDidLoad()
        topView.dropShadow(opacity: 0.1)
        bottomView.dropShadow(opacity: 0.1)
    }
   
    @IBAction func goHomeTapped(_ sender: DealsButton) {
        HomeTabBarController.launch()
    }
}

extension RewardHistoryDetailsViewController {
    static func launch(_ caller: UIViewController, details: RewardHistoryDetail) {
        let controller = R.storyboard.history.rewardHistoryDetailsViewController()!
        controller.historyDetails = details
        caller.navigationController?.pushViewController(controller, animated: true)
    }
}
