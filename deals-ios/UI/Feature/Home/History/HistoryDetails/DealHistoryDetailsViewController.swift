import UIKit

class DealHistoryDetailsViewController: BaseViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }


}

extension DealHistoryDetailsViewController {
    static func launch(_ caller: UIViewController) {
        let controller = R.storyboard.history.dealHistoryDetailsViewController()!
//        vc.historyDetails = details
        caller.navigationController?.pushViewController(controller, animated: true)
    }
}
