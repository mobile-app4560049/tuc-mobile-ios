import UIKit

class RewardCardsViewController: BaseViewController {
    
    @IBOutlet weak var cardView: UIView!
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var dealsHistoryTable: UITableView!
    @IBOutlet weak var viewAll: UILabel!
    
    let viewModel = HistoryViewModel()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        topView.dropShadow(opacity: 0.1)
        dealsHistoryTable.dataSource = self
        dealsHistoryTable.delegate = self
        dealsHistoryTable.register(UINib(nibName: R.nib.dealsHistoryTableViewCell.name, bundle: nil), forCellReuseIdentifier: R.nib.dealsHistoryTableViewCell.identifier)
        viewAll.addTapGestureRecognizer {
            HistoryViewController.launch(self, selectedHistory: .rewards)
        }
    }
    
}

extension RewardCardsViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: R.nib.dealsHistoryTableViewCell.identifier, for: indexPath) as! DealsHistoryTableViewCell
        cell.addTapGestureRecognizer {
            self.tabBarController?.tabBar.isHidden = true
            DealHistoryDetailsViewController.launch(self)
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120
    }
    
}

extension RewardCardsViewController {
    static func launch(_ caller: UIViewController) {
        let controller = R.storyboard.history.rewardCardsViewController()!
        caller.navigationController?.pushViewController(controller, animated: true)
    }
}
