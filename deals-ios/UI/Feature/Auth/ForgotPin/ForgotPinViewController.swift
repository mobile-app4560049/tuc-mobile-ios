import UIKit

class ForgotPinViewController: BaseViewController {
    
    @IBOutlet weak var numberField: DealsPhoneNumberField!
    
    private var selectedCountry: SelectedCountry?
    private let viewModel = ForgotPinViewModel(datasource: AuthRepository())
    
    override func viewDidLoad() {
        super.viewDidLoad()
        selectedCountry = .nigeria
        bindVM()
    }
    
    @IBAction func verifyNumberTapped(_ sender: DealsButton) {
        switch numberField.countryPhoneCode {
        case SelectedCountry.nigeria.isd:
            selectedCountry = .nigeria
        case SelectedCountry.ghana.isd:
            selectedCountry = .ghana
        case SelectedCountry.kenya.isd:
            selectedCountry = .kenya
        case SelectedCountry.rwanda.isd:
            selectedCountry = .rwanda
        case SelectedCountry.uganda.isd:
            selectedCountry = .uganda
        default:
            selectedCountry = nil
        }
        
        guard selectedCountry != nil else {
            self.showError(message: ErrorMessages.unsupportedCountry)
            return
        }
        
        if let selectedCountry = selectedCountry {
            viewModel.resetPin(countryIsd: selectedCountry.isd, mobileNumber: numberField.phoneNumber, countryId: selectedCountry.id, countryKey: selectedCountry.countryKey)
        }
    }
    
    private func bindVM(){
        viewModel.error.bind { [unowned self] error in
            self.showError(message: error)
        }.disposed(by: disposeBag)
        
        viewModel.progress.bind { [unowned self] loading in
            loading ? self.showProgressIndicator() : self.dismissProgressIndicator()
        }.disposed(by: disposeBag)
        
        viewModel.successMessage.bind { message in
            self.showSuccess(message: message)
        }.disposed(by: disposeBag)
        
        viewModel.resetPinResponse.bind { [weak self] response in
            guard let self = self else {return}
            if response != nil, let selectedCountry = self.selectedCountry {
                let data = ResetPinData(selectedCountry: selectedCountry, phoneNumber: self.numberField.phoneNumber)
                PinGeneratedViewController.launch(self, delegate: self, pinData: data)
            }
        }.disposed(by: disposeBag)
    }
    
}

extension ForgotPinViewController: PinGeneratedDelegate {
    func proceed(pinData: ResetPinData) {
        ResetPinViewController.launch(self, data: pinData)
    }
}

extension ForgotPinViewController {
    static func launch(_ caller: UIViewController) {
        let controller = R.storyboard.forgotPin.forgotPinViewController()!
        caller.navigationController?.pushViewController(controller, animated: true)
    }
}
