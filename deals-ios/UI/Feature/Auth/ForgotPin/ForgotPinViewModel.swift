import Foundation
import RxSwift
import RxCocoa

class ForgotPinViewModel: BaseViewModel {
    
    private let datasource: AuthDataSource
    
    var resetPinResponse = BehaviorRelay<UserVerificationResponse?>(value: nil)
    var updatePinResponse = BehaviorRelay<EmptyResponse?>(value: nil)
    let successMessage = PublishSubject<String>()
    
    init(datasource: AuthDataSource) {
        self.datasource = datasource
    }
    
    
    /// This function resets a user pin and allows them set a new password
    ///
    /// Returns a `UserVerificationResponse` Object
    func resetPin(countryIsd: String, mobileNumber: String, countryId: Int, countryKey: String){
        progress.accept(true)
        datasource.resetPin(countryIsd: countryIsd, mobileNumber: mobileNumber, countryId: countryId, countryKey: countryKey).subscribeOn(MainScheduler.instance).subscribe(onNext: {[weak self] (response) in
            
            guard let self = self else {return}
            self.progress.accept(false)
            
            if let resp = response.result {
                self.resetPinResponse.accept(resp)
                self.successMessage.onNext(response.message)
            } else {
                self.error.onNext(response.message)
            }
            
        }, onError: { [weak self] (error) in
            guard let self = self else {return}

            self.progress.accept(false)
            if let error = error as? ApiError {
                self.error.onNext(error.message)
            } else {
                self.error.onNext(error.localizedDescription)
            }
        }).disposed(by: disposeBag)
    }
    
    
    /// This function updates a user pin after it has been reset
    ///
    /// Returns a `EmptyResponse` Object
    func updatePin(countryIsd: String, mobileNumber: String, tempAccessPin: String, newAccessPin: String, countryId: Int, countryKey: String){
        progress.accept(true)
        datasource.updatePin(countryIsd: countryIsd, mobileNumber: mobileNumber, tempAccessPin: tempAccessPin, newAccessPin: newAccessPin, countryId: countryId, countryKey: countryKey).subscribe(onNext: {[weak self] (response) in
            
            guard let self = self else {return}
            self.progress.accept(false)
            
            if response.status.lowercased() == "success" {
                self.updatePinResponse.accept(response)
                self.successMessage.onNext(response.message)
            } else {
                self.error.onNext(response.message)
            }
            
        }, onError: { [weak self] (error) in
            guard let self = self else {return}

            self.progress.accept(false)
            if let error = error as? ApiError {
                self.error.onNext(error.message)
            } else {
                self.error.onNext(error.localizedDescription)
            }
        }).disposed(by: disposeBag)
    }
    
  
}
