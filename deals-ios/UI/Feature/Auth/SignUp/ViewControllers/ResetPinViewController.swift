import UIKit

class ResetPinViewController: BaseViewController {

    @IBOutlet weak var tempPinField: DealsPinField!
    @IBOutlet weak var newPinField: DealsPinField!
    @IBOutlet weak var confirmPinField: DealsPinField!
    
    private let viewModel = ForgotPinViewModel(datasource: AuthRepository())
    private var data: ResetPinData!

    override func viewDidLoad() {
        super.viewDidLoad()
        bindVM()
    }
    
    private func bindVM(){
        viewModel.error.bind { [unowned self] error in
            self.showError(message: error)
        }.disposed(by: disposeBag)
        
        viewModel.progress.bind { [unowned self] loading in
            loading ? self.showProgressIndicator() : self.dismissProgressIndicator()
        }.disposed(by: disposeBag)
        
        viewModel.updatePinResponse.bind { response in
            if response != nil, let response = response {
                LoginViewController.launchStandAlone()
                self.showSuccess(message: response.message)
            }
        }.disposed(by: disposeBag)
    }

    @IBAction func verifyNumberTapped(_ sender: DealsButton) {
        let validator = Validator(self)
        if validator.pinValidation(tempPinField.content) && validator.pinValidation(tempPinField.content) && validator.pinValidation(tempPinField.content) {
            if validator.checkEqual(newPinField.content, confirmPinField.content, fieldName: "New Pins") {
                viewModel.updatePin(countryIsd: data.selectedCountry.isd, mobileNumber: data.phoneNumber, tempAccessPin: tempPinField.content, newAccessPin: newPinField.content, countryId: data.selectedCountry.id, countryKey: data.selectedCountry.countryKey)
            }
        }
    }
    
}


extension ResetPinViewController {
    static func launch(_ caller: UIViewController, data: ResetPinData) {
        let controller = R.storyboard.signUp.resetPinViewController()!
        controller.data = data
        caller.navigationController?.pushViewController(controller, animated: true)
    }
}

struct ResetPinData {
    let selectedCountry: SelectedCountry
    let phoneNumber: String
}
