import UIKit
import CBPinEntryView

class VerifyPhoneNumberViewController: BaseViewController {

    @IBOutlet weak var pinField: CBPinEntryView!
    private var data: PhoneNumberVerificationData!

    private let viewModel = SignUpViewModel(datasource: AuthRepository())
    
    override func viewDidLoad() {
        super.viewDidLoad()
        bindVM()
    }
    
    @IBAction func verifyNumberTapped(_ sender: DealsButton) {
        if pinField.getPinAsString().count == 4 {
            viewModel.verifyUserWithToken(countryIsd: data.selectedCountry.isd, mobileNumber: data.phoneNumber, requestToken: data.userVerificationResponse.requestToken, accessCode: pinField.getPinAsString(), countryId: data.selectedCountry.id, countryKey: data.selectedCountry.countryKey)
        } else {
            self.showError(message: ErrorMessages.enterPin)
        }
    }
    
    private func bindVM(){
        viewModel.error.bind { [unowned self] error in
            self.showError(message: error)
        }.disposed(by: disposeBag)
        
        viewModel.progress.bind { [unowned self] loading in
            loading ? self.showProgressIndicator() : self.dismissProgressIndicator()
        }.disposed(by: disposeBag)
        
        viewModel.successMessage.bind { message in
            self.showSuccess(message: message)
        }.disposed(by: disposeBag)
        
        viewModel.user.bind { user in
            if user != nil {
                var country: SelectedCountry = .nigeria
                switch user?.country.name.lowercased() {
                case SelectedCountry.ghana.countryKey:
                    country = .ghana
                case SelectedCountry.rwanda.countryKey:
                    country = .rwanda
                case SelectedCountry.uganda.countryKey:
                    country = .uganda
                case SelectedCountry.kenya.countryKey:
                    country = .kenya
                default:
                    country = .nigeria
                }
                DealsKeyValueRepository.sharedInstance.loginUser(countryForApi: country)
                HomeTabBarController.launch()
            }
        }.disposed(by: disposeBag)
    }

}


extension VerifyPhoneNumberViewController {
    static func launch(_ caller: UIViewController, data: PhoneNumberVerificationData) {
        let controller = R.storyboard.signUp.verifyPhoneNumberViewController()!
        controller.data = data
        caller.navigationController?.pushViewController(controller, animated: true)
    }
}

struct PhoneNumberVerificationData {
     let userVerificationResponse: UserVerificationResponse
     let selectedCountry: SelectedCountry
     let phoneNumber: String
}
