import UIKit

protocol PinGeneratedDelegate {
    func proceed(pinData: ResetPinData)
}

class PinGeneratedViewController: BaseViewController {

    var delegate: PinGeneratedDelegate!
    var pinData: ResetPinData!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        DispatchQueue.main.asyncAfter(deadline: .now() + 4) {
            self.goToNextScreen()
        }
    }
    
    private func goToNextScreen() {
        self.dismiss(animated: true){
            self.delegate.proceed(pinData: self.pinData)
        }
    }

}
extension PinGeneratedViewController {
    static func launch(_ caller: UIViewController, delegate: PinGeneratedDelegate, pinData: ResetPinData) {
        let controller = R.storyboard.signUp.pinGeneratedViewController()!
        controller.delegate = delegate
        controller.pinData = pinData
        controller.hasBack = false
        controller.modalPresentationStyle = .overFullScreen
        caller.navigationController?.present(controller, animated: true)
    }
}
