import UIKit

class SignUpViewController: BaseViewController {
    
    @IBOutlet weak var nameField: DealsInputField!
    @IBOutlet weak var emailField: DealsInputField!
    @IBOutlet weak var numberField: DealsPhoneNumberField!
    @IBOutlet weak var pinField: DealsPinField!
    @IBOutlet weak var login: UILabel!
    @IBOutlet weak var referralField: DealsOptionalInputField!
    
    private let viewModel = SignUpViewModel(datasource: AuthRepository())
    private var selectedCountry: SelectedCountry?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        selectedCountry = .nigeria
        login.addTapGestureRecognizer {
            LoginViewController.launch(self)
        }
        bindVM()
    }
    
    @IBAction func signUpTapped(_ sender: DealsButton) {
        switch numberField.countryPhoneCode {
        case SelectedCountry.nigeria.isd:
            selectedCountry = .nigeria
        case SelectedCountry.ghana.isd:
            selectedCountry = .ghana
        case SelectedCountry.kenya.isd:
            selectedCountry = .kenya
        case SelectedCountry.rwanda.isd:
            selectedCountry = .rwanda
        case SelectedCountry.uganda.isd:
            selectedCountry = .uganda
        default:
            selectedCountry = nil
        }
        
        let validator = Validator(self)
        if validator.textValidation(nameField.content, "Please Enter a name") && validator.emailValidation(emailField.content) {
            
            if let selectedCountry = selectedCountry {
                if validator.lengthValidation("Phone Number", numberField.phoneNumber, minLenght: 10) && validator.pinValidation(pinField.content) {
                    viewModel.createUser(countryIsd: selectedCountry.isd, mobileNumber: numberField.phoneNumber, accountPin: pinField.content, emailAddress: emailField.content, name: nameField.content, referredBy: referralField.content, countryId: selectedCountry.id, countryKey: selectedCountry.countryKey)
                }
            } else {
                self.showError(message: ErrorMessages.unsupportedCountry)
                return
            }
        }
    }
    
    private func bindVM(){
        viewModel.error.bind { [unowned self] error in
            self.showError(message: error)
        }.disposed(by: disposeBag)
        
        viewModel.progress.bind { [unowned self] loading in
            loading ? self.showProgressIndicator() : self.dismissProgressIndicator()
        }.disposed(by: disposeBag)
        
        viewModel.signUpResponse.bind { [weak self] response in
            guard let self = self else {return}
            if response != nil, let response = response {
                let data = PhoneNumberVerificationData(
                    userVerificationResponse: response,
                    selectedCountry: self.selectedCountry!,
                    phoneNumber: self.numberField.phoneNumber)
                VerifyPhoneNumberViewController.launch(self, data: data)
            }
        }.disposed(by: disposeBag)
        
        viewModel.successMessage.bind { message in
            self.showSuccess(message: message)
        }.disposed(by: disposeBag)
        
    }
    
}


extension SignUpViewController {
    static func launch(_ caller: UIViewController) {
        let controller = R.storyboard.signUp.signUpViewController()!
        if let navController = caller.navigationController {
            navController.pushViewController(controller, animated: true)
        }
    }
    
    static func launcFromOnboarding(_ caller: UIViewController){
        let controller = R.storyboard.signUp.signUpViewController()!
        controller.hasBack = false
        let nav = DealsNavController(rootViewController: controller)
        controller.navigationItem.leftBarButtonItem = nil
        nav.modalPresentationStyle = .overFullScreen
        caller.present(nav, animated: true)
    }
}
