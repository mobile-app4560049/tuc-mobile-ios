import Foundation
import RxSwift
import RxCocoa
import FreshchatSDK

class SignUpViewModel: BaseViewModel {
    
    private let datasource: AuthDataSource
    
    var signUpResponse = BehaviorRelay<UserVerificationResponse?>(value: nil)
    let successMessage = PublishSubject<String>()
    
    var user = BehaviorRelay<User?>(value: nil)
    
    init(datasource: AuthDataSource) {
        self.datasource = datasource
    }
    
    
    /// This function performs the login action using the country code, mobile number, access pin, country ID and country Key(country name)
    ///
    /// Returns a `UserVerificationResponse` Object
    func createUser(countryIsd: String, mobileNumber: String, accountPin: String, emailAddress: String, name: String, referredBy: String, countryId: Int, countryKey: String){
        progress.accept(true)
        datasource.createUser(countryIsd: countryIsd, mobileNumber: mobileNumber, accountPin: accountPin, emailAddress: emailAddress, name: name, referredBy: referredBy, countryId: countryId, countryKey: countryKey).subscribeOn(MainScheduler.instance).subscribe(onNext: {[weak self] (response) in
            
            guard let self = self else {return}
            self.progress.accept(false)
            
            if let verificationResponse = response.result {
                self.signUpResponse.accept(verificationResponse)
                self.successMessage.onNext(response.message)
            } else {
                self.error.onNext(response.message)
            }
            
        }, onError: { [weak self] (error) in
            guard let self = self else {return}

            self.progress.accept(false)
            if let error = error as? ApiError {
                self.error.onNext(error.message)
            } else {
                self.error.onNext(error.localizedDescription)
            }
        }).disposed(by: disposeBag)
    }
    
    /// This function verifies a registered user
    ///
    /// Returns a `User` Object
    func verifyUserWithToken(countryIsd: String, mobileNumber: String, requestToken: String, accessCode: String, countryId: Int, countryKey: String){
        progress.accept(true)
        datasource.verifyUser(countryIsd: countryIsd, mobileNumber: mobileNumber, requestToken: requestToken, accessCode: accessCode, countryId: countryId, countryKey: countryKey).subscribeOn(MainScheduler.instance).subscribe(onNext: {[weak self] (response) in
            
            guard let self = self else {return}
            self.progress.accept(false)
            
            if let user = response.result {
                DealsKeyValueRepository.sharedInstance.saveUser(user)
                self.identifyFreshChatUser(user, mobileNumber: mobileNumber)
                self.user.accept(user)
                self.successMessage.onNext(response.message)
            } else {
                self.error.onNext(response.message)
            }
            
        }, onError: { [weak self] (error) in
            guard let self = self else {return}

            self.progress.accept(false)
            if let error = error as? ApiError {
                self.error.onNext(error.message)
            } else {
                self.error.onNext(error.localizedDescription)
            }
        }).disposed(by: disposeBag)
    }
  
    private func identifyFreshChatUser(_ user: User, mobileNumber: String){
        let userInstance = FreshchatUser.sharedInstance()
        userInstance.firstName = user.name
        userInstance.lastName = user.displayName
        userInstance.email = user.emailAddress
        userInstance.phoneCountryCode = user.country.isd
        userInstance.phoneNumber = mobileNumber
        Freshchat.sharedInstance().setUser(userInstance)
    }
}
