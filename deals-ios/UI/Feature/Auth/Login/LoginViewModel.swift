import Foundation
import RxSwift
import RxCocoa
import FreshchatSDK

class LoginViewModel: BaseViewModel {
    
    private let datasource: AuthDataSource
    
    var user = BehaviorRelay<User?>(value: nil)
    let successMessage = PublishSubject<String>()
    
    init(datasource: AuthDataSource) {
        self.datasource = datasource
    }
    
    
    /// This function performs the login action using the country code, mobile number, access pin, country ID and country Key(country name)
    ///
    /// Returns a `User` Object
    func login(countryIsd: String, mobileNumber: String, accessPin: String, countryId: Int, countryKey: String){
        progress.accept(true)
        datasource.login(countryIsd: countryIsd, mobileNumber: mobileNumber, accessPin: accessPin, countryId: countryId, countryKey: countryKey).subscribeOn(MainScheduler.instance).subscribe(onNext: {[weak self] (response) in
            
            guard let self = self else {return}
            self.progress.accept(false)
            
            if let loggedInUser = response.result {
                DealsKeyValueRepository.sharedInstance.saveUser(loggedInUser)
                switch loggedInUser.country.name.lowercased() {
                case SelectedCountry.nigeria.countryKey:
                    DealsKeyValueRepository.sharedInstance.saveUserCountry(.nigeria)
                case SelectedCountry.ghana.countryKey:
                    DealsKeyValueRepository.sharedInstance.saveUserCountry(.ghana)
                case SelectedCountry.kenya.countryKey:
                    DealsKeyValueRepository.sharedInstance.saveUserCountry(.kenya)
                case SelectedCountry.uganda.countryKey:
                    DealsKeyValueRepository.sharedInstance.saveUserCountry(.uganda)
                case SelectedCountry.rwanda.countryKey:
                    DealsKeyValueRepository.sharedInstance.saveUserCountry(.rwanda)
                default:
                    DealsKeyValueRepository.sharedInstance.saveUserCountry(.nigeria)
                }
                self.identifyFreshChatUser(loggedInUser, mobileNumber: mobileNumber)
                self.user.accept(loggedInUser)
                self.successMessage.onNext(response.message)
            } else {
                self.error.onNext(response.message)
            }
            
        }, onError: { [weak self] (error) in
            guard let self = self else {return}

            self.progress.accept(false)
            if let error = error as? ApiError {
                self.error.onNext(error.message)
            } else {
                self.error.onNext(error.localizedDescription)
            }
        }).disposed(by: disposeBag)
    }
    
    /// This function sets the fresh chat instance to the currently logged in user
    private func identifyFreshChatUser(_ user: User, mobileNumber: String){
        let userInstance = FreshchatUser.sharedInstance()
        userInstance.firstName = user.name
        userInstance.lastName = user.displayName
        userInstance.email = user.emailAddress
        userInstance.phoneCountryCode = user.country.isd
        userInstance.phoneNumber = mobileNumber
        Freshchat.sharedInstance().setUser(userInstance)
    }
  
}
