import UIKit
import FreshchatSDK

class LoginViewController: BaseViewController {
    
    @IBOutlet weak var numberField: DealsPhoneNumberField!
    @IBOutlet weak var pinField: DealsPinField!
    @IBOutlet weak var signUp: UILabel!
    @IBOutlet weak var forgotPin: UILabel!

    private let viewModel = LoginViewModel(datasource: AuthRepository())
    private var selectedCountry: SelectedCountry?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        selectedCountry = .nigeria
        signUp.addTapGestureRecognizer {
            SignUpViewController.launch(self)
        }
        forgotPin.addTapGestureRecognizer {
            ForgotPinViewController.launch(self)
        }

        bindVM()
    }
    
    private func bindVM(){
        viewModel.error.bind { [unowned self] error in
            self.showError(message: error)
        }.disposed(by: disposeBag)
        
        viewModel.progress.bind { [unowned self] loading in
            loading ? self.showProgressIndicator() : self.dismissProgressIndicator()
        }.disposed(by: disposeBag)
        
        viewModel.successMessage.bind { message in
            self.showSuccess(message: message)
        }.disposed(by: disposeBag)
        
        viewModel.user.bind { user in
            if user != nil {
                var country: SelectedCountry = .nigeria
                switch user?.country.name.lowercased() {
                case SelectedCountry.ghana.countryKey:
                    country = .ghana
                case SelectedCountry.rwanda.countryKey:
                    country = .rwanda
                case SelectedCountry.uganda.countryKey:
                    country = .uganda
                case SelectedCountry.kenya.countryKey:
                    country = .kenya
                default:
                    country = .nigeria
                }
                DealsKeyValueRepository.sharedInstance.loginUser(countryForApi: country)
                HomeTabBarController.launch()
            }
        }.disposed(by: disposeBag)
        
    }
    
    @IBAction func logInTapped(_ sender: DealsButton) {
        switch numberField.countryPhoneCode {
        case SelectedCountry.nigeria.isd:
            selectedCountry = .nigeria
        case SelectedCountry.ghana.isd:
            selectedCountry = .ghana
        case SelectedCountry.kenya.isd:
            selectedCountry = .kenya
        case SelectedCountry.rwanda.isd:
            selectedCountry = .rwanda
        case SelectedCountry.uganda.isd:
            selectedCountry = .uganda
        default:
            selectedCountry = nil
        }
        
        guard selectedCountry != nil else {
            self.showError(message: ErrorMessages.unsupportedCountry)
            return
        }
        
        if let selectedCountry = selectedCountry {
            let validator = Validator(self)
            if  validator.lengthValidation("Phone Number", numberField.phoneNumber, minLenght: 10) && validator.pinValidation(pinField.content) {
                viewModel.login(countryIsd: selectedCountry.isd, mobileNumber: numberField.phoneNumber, accessPin: pinField.content, countryId: selectedCountry.id, countryKey: selectedCountry.countryKey)
            }
        }
    }

}

extension LoginViewController {
    static func launch(_ caller: UIViewController) {
        let controller = R.storyboard.login.loginViewController()!
        if let navController = caller.navigationController {
            navController.pushViewController(controller, animated: true)
        } else {
            controller.hasBack = false
            let nav = DealsNavController(rootViewController: controller)
            nav.modalPresentationStyle = .overFullScreen
            controller.navigationController?.navigationBar.backgroundColor = R.color.white()!
            caller.present(nav, animated: true)
        }
    }
    
    static func launchStandAlone() {
      if let controller = R.storyboard.login.loginViewController(),
        let window = UIApplication.shared.keyWindow {
        window.rootViewController = controller
      }
    }
}
