import UIKit
import FSPagerView

class OnBoardingViewController: BaseViewController {
    
    let viewModel = OnboardingViewModel()
    
    @IBOutlet weak var guestContinue: UILabel!
    
    @IBOutlet weak var pagerView: FSPagerView! {
        didSet {
            let nib = UINib(nibName: R.nib.onboardingCell.name, bundle: nil)
            self.pagerView.register(nib, forCellWithReuseIdentifier: R.nib.onboardingCell.name)
        }
    }
    @IBOutlet weak var pageControl: FSPageControl! {
        didSet {
            self.pageControl.contentHorizontalAlignment = .center
            self.pageControl.contentInsets = UIEdgeInsets(top: 0, left: 20, bottom: 0, right: 20)
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        pagerView.delegate = self
        pagerView.dataSource = self
        pagerView.automaticSlidingInterval = 3.0
        pageControl.numberOfPages = viewModel.images.count
        pageControl.setImage(R.image.unselectedOnboardingCell()!, for: .normal)
        pageControl.setImage(R.image.selectedOnboardingCell()!, for: .selected)
        bindVM()
        addGestures()
        self.removeBackButton()
    }
    private func bindVM(){
        viewModel.guestLoggedIn.bind { value in
            if value {
                DealsKeyValueRepository.sharedInstance.onBoardUser()
                DealsKeyValueRepository.sharedInstance.saveUserCountry(.nigeria)
                HomeTabBarController.launch()
            }
        }.disposed(by: disposeBag)
    }
    
    private func addGestures() {
        guestContinue.addTapGestureRecognizer { [weak self] in
            guard let self = self else {return}
            self.viewModel.guestLogin()
        }
    }
    
    @IBAction func getStartedTapped(_ sender: DealsButton) {
        DealsKeyValueRepository.sharedInstance.onBoardUser()
        SignUpViewController.launcFromOnboarding(self)
    }
}

extension OnBoardingViewController: FSPagerViewDelegate, FSPagerViewDataSource {
    func numberOfItems(in pagerView: FSPagerView) -> Int {
        return viewModel.images.count
    }
    
    func pagerView(_ pagerView: FSPagerView, cellForItemAt index: Int) -> FSPagerViewCell {
        let cell = pagerView.dequeueReusableCell(withReuseIdentifier: R.nib.onboardingCell.name, at: index) as! OnboardingViewCell
        let dataImage = viewModel.images[index]
        cell.cellImage.image = dataImage
        return cell
    }
    
    func pagerViewDidScroll(_ pagerView: FSPagerView) {
        self.pageControl.currentPage = pagerView.currentIndex
    }
    
}


extension OnBoardingViewController {
    static func launch(_ caller: UIViewController) {
        let controller = R.storyboard.onBoarding.onBoardingViewController()!
        controller.modalPresentationStyle = .overFullScreen
        caller.present(controller, animated: true)
    }
}
