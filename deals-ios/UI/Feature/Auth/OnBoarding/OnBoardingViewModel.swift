import UIKit
import RxSwift
import RxCocoa

class OnboardingViewModel: BaseViewModel {
    
    let successMessage = PublishSubject<String>()
    let guestLoggedIn = BehaviorRelay<Bool>(value: false)
    
    
    let images = [R.image.onboardingOne()!, R.image.onboardingTwo()!, R.image.onboardingThree()!]
    
    
    /// This function performs the login action for a guest
    func guestLogin(){
        DealsKeyValueRepository.sharedInstance.saveUserCountry(.nigeria)
        self.successMessage.onNext(SuccessMessages.guestLogin)
        guestLoggedIn.accept(true)
    }
}
