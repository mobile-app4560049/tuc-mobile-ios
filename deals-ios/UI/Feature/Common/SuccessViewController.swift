import UIKit

enum SuccessType { case fundWallet, airTime, data, purchaseDeal, utility, changePin, switchCountry}

class SuccessViewController: BaseViewController {
    
    @IBOutlet weak var messageTitle: UILabel!
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var backHome: UILabel!
    
    @IBOutlet weak var saveBeneficiary: RaisedView!
    @IBOutlet weak var viewToken: RaisedView!
    @IBOutlet weak var viewHistory: RaisedView!
    
    var successType: SuccessType!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.removeBackButton()
        if successType == .utility {
            viewToken.isHidden = false
            saveBeneficiary.isHidden = true
        }
        addGestures()
    }
    
    @IBAction func goHomeTapped(_ sender: DealsButton) {
        navigationController?.popViewController(animated: true)
        navigationController?.popViewController(animated: true)
    }
    
    func addGestures(){
        backHome.addTapGestureRecognizer {
            HomeTabBarController.launch()
        }
        viewHistory.addTapGestureRecognizer {
            switch self.successType {
            case .utility:
                HistoryViewController.launch(self, selectedHistory: .billsPayment)
            case .data, .airTime:
                HistoryViewController.launch(self, selectedHistory: .topUp)
            default:
                HistoryViewController.launch(self, selectedHistory: .deals)
            }
            
        }
        saveBeneficiary.addTapGestureRecognizer {
            SaveBeneficiaryViewController.launch(self)
        }
    }

}

extension SuccessViewController {
    static func launch(_ caller: UIViewController, type: SuccessType) {
        let controller = R.storyboard.moreItems.successViewController()!
        controller.successType = type
        caller.navigationController?.pushViewController(controller, animated: true)
    }
}
