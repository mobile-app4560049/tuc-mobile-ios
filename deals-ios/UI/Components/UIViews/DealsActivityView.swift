import UIKit

class DealsActivityView: UIActivityIndicatorView {
    
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.color = R.color.brightPurple()!
    }
}
