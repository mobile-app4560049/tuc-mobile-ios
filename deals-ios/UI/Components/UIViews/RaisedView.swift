import UIKit

class RaisedView: UIView {
    override func layoutSubviews() {
        super.layoutSubviews()
        self.dropShadow(opacity: 0.1)
    }
}
