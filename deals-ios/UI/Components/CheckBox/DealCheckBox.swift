import Foundation
import BEMCheckBox

class DealCheckBox: BEMCheckBox {
    
    override init(frame: CGRect) {
      super.init(frame: frame)
    }
      
    required init?(coder aDecoder: NSCoder) {
      super.init(coder: aDecoder)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        setup()
    }
    
    private func setup(){
        self.tintColor = R.color.brightPurple()!
        self.onCheckColor = R.color.brightPurple()!
        self.boxType = .square
    }
}
