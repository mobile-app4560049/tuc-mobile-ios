import UIKit

@IBDesignable
class DealsButton:  UIButton {
    
    @IBInspectable override var backgroundColor: UIColor? {
        didSet {
            if let newColor = backgroundColor {
                super.backgroundColor = newColor
            } else {
                super.backgroundColor = R.color.brightPurple()!
            }
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        setup()
    }
    
    override var isEnabled: Bool {
        didSet {
            self.backgroundColor = isEnabled ? backgroundColor :  backgroundColor?.withAlphaComponent(0.4)
        }
    }
    
    init(title: String? = nil){
        super.init(frame: .zero)
        self.setTitle(title, for: .normal)
        setup()
    }
    
    
    func setup() {
        self.heightAnchor.constraint(equalToConstant: 47).isActive = true
        self.translatesAutoresizingMaskIntoConstraints = false
        self.backgroundColor = backgroundColor
        self.titleLabel?.textColor = R.color.white()!
        self.titleLabel?.font = R.font.brFirmaSemiBold(size: 16)!
        self.layer.cornerRadius = 6
        
    }
}
