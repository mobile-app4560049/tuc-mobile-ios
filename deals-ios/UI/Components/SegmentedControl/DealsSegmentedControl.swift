import UIKit


@IBDesignable
  class DealsSegmentedControl: UISegmentedControl{
      
    override init(frame: CGRect) {
      super.init(frame: frame)
    }
      
    required init?(coder aDecoder: NSCoder) {
      super.init(coder: aDecoder)
    }
      
    override func layoutSubviews() {
      super.layoutSubviews()
      setup()
    }
      
    func setup() {
      if #available(iOS 13.0, *) {
          self.selectedSegmentTintColor = R.color.brightPurple()!
      }
      self.translatesAutoresizingMaskIntoConstraints = false
    }
  }

  extension UISegmentedControl{
    @IBInspectable var textColor: UIColor{
      get {
        return self.textColor
      }
      set {
          let normalAttributes = [NSAttributedString.Key.foregroundColor: R.color.black()!,
                                    NSAttributedString.Key.font: R.font.brFirmaRegular(size: 14)]
        let selectedAttributes = [NSAttributedString.Key.foregroundColor: newValue,
                                  NSAttributedString.Key.font: R.font.brFirmaSemiBold(size: 14)]
          self.setTitleTextAttributes(normalAttributes as [NSAttributedString.Key : Any], for: .normal)
          self.setTitleTextAttributes(selectedAttributes as [NSAttributedString.Key : Any], for: .selected)
          
      }
    }
  }
