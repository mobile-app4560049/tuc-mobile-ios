import UIKit
import SnapKit
import Then
import IQKeyboardManagerSwift
import PhoneNumberKit

class DealsPhoneNumberTextField : PhoneNumberTextField{
    override var defaultRegion: String{
        get{
            return "NG"
        }
        set{}
    }
}


class DealsPhoneNumberField: UIView, UITextFieldDelegate {
    var valueChanged:(()->())?
    private var contentMaxLenght = 20
    
    @IBInspectable var title: String? {
        set(value){titleLabel.text = value}
        get{return titleLabel.text}
    }
    
    @IBInspectable var placeholder: String?{
        set(value){
            contentTextField.toolbarPlaceholder = value
            contentTextField.placeholder = value
        }
        get{return contentTextField.toolbarPlaceholder ?? contentTextField.placeholder}
    }
    
    @IBInspectable var content: String {
        set(value){contentTextField.text = value}
        get{
            
            if let text = contentTextField.text?.replacingOccurrences(of: " ", with: ""){
               let newText  = text.replacingOccurrences(of: "+", with: "")
                return newText
            } else {
                return ""
            }
        }
    }
    
    var phoneNumber: String {
        get {
            return content.replacingOccurrences(of: countryPhoneCode, with: "")
        }
    }
    
    var countryPhoneCode: String {
        get {
            if let code = contentTextField.phoneNumberKit.countryCode(for: contentTextField.currentRegion) {
                return "\(code)"
            } else {
                return ""
            }
        }
    }
    

    @IBInspectable var isSecureTextEntry: Bool{
        set(value){contentTextField.isSecureTextEntry = value}
        get{return contentTextField.isSecureTextEntry}
    }
    
    @IBInspectable var isEditable: Bool{
        set(value){contentTextField.isUserInteractionEnabled = value}
        get{return contentTextField.isUserInteractionEnabled}
    }
    
    @IBInspectable var maxLenght:Int{
        set(value){contentMaxLenght = value}
        get{return contentMaxLenght}
    }
    
    @IBInspectable var labelFont:UIFont{
        set(value){titleLabel.font = value}
        get{return titleLabel.font}
    }
    
    internal lazy var inputContainer:UIView  = {
        return UIView().then{
            $0.backgroundColor = isEditable ? R.color.white() : R.color.textfieldDisabled()!
            $0.borderColor = R.color.textfieldBorder()
            $0.borderWidth = 0.5
            $0.layer.cornerRadius = 4
        }
    }()
    
    internal lazy var titleLabel: UILabel = {
        return UILabel().then{
            $0.numberOfLines = 0
            $0.lineBreakMode = .byWordWrapping
            $0.font = R.font.brFirmaMedium(size: 12)
            $0.textColor = R.color.textfieldTitleColor()
            $0.textAlignment = .left
        }
    }()
    
    internal lazy var contentTextField: DealsPhoneNumberTextField =  {
        return DealsPhoneNumberTextField().then{
            $0.font = R.font.brFirmaMedium(size: 14)
            $0.textAlignment = .left
            $0.textColor = R.color.black()
            $0.tintColor = R.color.brightPurple()
            $0.withPrefix = true
            $0.withFlag = true
            $0.withExamplePlaceholder = true
        }
    }()

    
    
    override init(frame: CGRect) {super.init(frame: frame)}
    
    required init?(coder aDecoder: NSCoder) {super.init(coder: aDecoder)}
    
    
    override func layoutSubviews() {
        super.layoutSubviews()
        setupView()
        setupOnEditingChanged()
        contentTextField.withPrefix = true
        contentTextField.keyboardType = UIKeyboardType.numberPad
    }
    
    private func setupOnEditingChanged(){
        contentTextField.addTarget(self, action: #selector(onEditingChanged), for: .editingChanged)
    }
    
    
    @objc func onEditingChanged() {
        if contentTextField.text?.first != "+" {
            contentTextField.text = "+\(contentTextField.text ?? "")"
        }
    }
    
    /// Sets the UIKeyboard type for the content fieled
    ///
    /// - Parameter keyboardType: The desired UIKeyboard type
    func setKeyBoardType(_ keyboardType:UIKeyboardType){
        contentTextField.keyboardType = UIKeyboardType.numberPad
    }
    
    func setmaxLenght(_ lenght:Int){
        self.maxLenght = lenght
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let text = textField.text ?? ""
        let nsString = text as NSString
        let newText = nsString.replacingCharacters(in: range, with: string)
        valueChanged?()
        return newText.count <= maxLenght
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if textField.text?.first != "+" {
            textField.text = "+\(textField.text ?? "")"
        }
        
        //reset input field color for error
//        titleLabel.textColor  = UIColor.mintLabel()
//        inputContainer.borderColor = UIColor.mintBottomSheetBorderColor()
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return self.contentTextField.resignFirstResponder()
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        contentTextField.updateFlag()
    }
    
    private func setupView() {
        //add the label subview
        addSubview(titleLabel)
        addSubview(inputContainer)
        inputContainer.addSubview(contentTextField)
//        inputContainer.addSubview(iconImageView)
    
        
        titleLabel.snp.makeConstraints { make in
            make.left.equalToSuperview()
            make.top.equalToSuperview().offset(8)
            make.right.equalToSuperview()
        }
        
        
        inputContainer.snp.makeConstraints{make in
            make.bottom.equalTo(titleLabel.snp_bottom).offset(50)
            make.right.equalToSuperview()
            make.left.equalToSuperview()
            make.width.equalToSuperview()
        }
        
        contentTextField.snp.makeConstraints {make in
            make.left.equalToSuperview().offset(8)
            make.top.equalToSuperview().offset(4)
            make.right.equalToSuperview().inset(8)
            make.bottom.equalToSuperview().inset(4)
            make.height.equalTo(35).priority(1000)
        }
        
        contentTextField.delegate = self
        setKeyBoardType(.phonePad)
        
        if isEditable{
            //add a click listener to focus on the contentTextField
            self.inputContainer.addTapGestureRecognizer{
                self.contentTextField.becomeFirstResponder()
            }
            
            self.addTapGestureRecognizer{
                self.contentTextField.becomeFirstResponder()
            }
        }
        
        // Country picker is only available on >iOS 11.0
        if #available(iOS 11.0, *) {
            PhoneNumberKit.CountryCodePicker.commonCountryCodes = ["NG","GH", "KE", "RW", "UG"]
            //has wrong bahavior for a viw controller that has a navigation controller.
//            PhoneNumberKit.CountryCodePicker.forceModalPresentation = true

            self.contentTextField.withDefaultPickerUI = true
        }
    }
}
