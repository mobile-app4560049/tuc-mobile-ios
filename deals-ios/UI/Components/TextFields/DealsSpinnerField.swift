import UIKit
import ActionSheetPicker_3_0

protocol DealsSpinnerDelegate {
    func onItemSelected(dealsSpinnerfield: DealsSpinnerField, selectedIndex: Int, text: String)
}

class DealsSpinnerField: DealsInputField {
    var spinnerData: [String]?
    var delegate: DealsSpinnerDelegate?
    var selectedIndex:Int = 0
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        self.rightIcon = R.image.arrowDropDown()!
        contentTextField.addTapGestureRecognizer {
            ActionSheetStringPicker.show(
                withTitle: self.title,
                rows: self.spinnerData,
                initialSelection: 0,
                doneBlock: { picker, index, item in
                    if let item = item {
                        self.content = "\(item)"
                    }
                    self.selectedIndex = index
                    self.delegate?.onItemSelected(dealsSpinnerfield: self, selectedIndex: index, text: self.content)
                },
                cancel: nil,
                origin: self
            )
        }
        
    }
}

