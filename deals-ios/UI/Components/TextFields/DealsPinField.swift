import UIKit
import SnapKit
import IQKeyboardManagerSwift

class DealsPinField : DealsInputField{
    
    
    
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        setupView()
        self.iconImageView.addTapGestureRecognizer {
            self.isSecureTextEntry.toggle()
        }
        
        iconImageView.snp.makeConstraints{make in
            make.right.equalToSuperview().inset(16)
            make.centerY.equalToSuperview()
            make.width.height.equalTo(24)
        }
        self.contentTextField.keyboardType = UIKeyboardType.numberPad
        
    }
    
    private func setupView(){
        self.contentTextField.addTarget(self, action: #selector(self.fieldTextChanged), for: .editingChanged)
        self.contentTextField.keyboardType = UIKeyboardType.numberPad
    }
    
    @objc private func fieldTextChanged() {
        self.textChanged?(content)
    }
}
