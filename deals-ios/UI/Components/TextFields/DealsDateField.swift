import UIKit

class DealsDateField: DealsInputField {
    
    var datePickerMode: UIDatePicker.Mode = .date
    let datePicker = UIDatePicker()
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        datePicker.datePickerMode = datePickerMode
        datePicker.preferredDatePickerStyle = .wheels
        
        contentTextField.inputView = datePicker
        
        //Add Tool Bar as input AccessoryView
        let toolBar = UIToolbar(frame: CGRect(x: 0, y: 0, width: (self.superview?.frame.width)!, height: 44))
        let flexibleSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelBarButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelPressed))
        let doneBarButton = UIBarButtonItem(title: "Done", style: .plain, target: target, action: #selector(doneButtonPressed))
        doneBarButton.tintColor = R.color.dashboardPurple()
        cancelBarButton.tintColor = R.color.red()
        toolBar.setItems([cancelBarButton, flexibleSpace, doneBarButton], animated: false)

        contentTextField.inputAccessoryView = toolBar
    }
    
    @objc func cancelPressed() {
        self.contentTextField.resignFirstResponder()
      }
    
    @objc func doneButtonPressed() {
        let dateFormatter = DateFormatter()
        switch datePickerMode {
        case .date:
            dateFormatter.dateFormat = "dd-MM-yyyy"
        case .time:
            dateFormatter.dateFormat = "hh:mm a"
        case .dateAndTime:
            dateFormatter.dateFormat = "dd-MM-yyyy hh:mm a"
        default:
            return
        }
        self.content = dateFormatter.string(from: datePicker.date)
        self.contentTextField.resignFirstResponder()
        
     }
    
    
}
