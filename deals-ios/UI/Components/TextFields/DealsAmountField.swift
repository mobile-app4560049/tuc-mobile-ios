import UIKit

class DealsAmountField: DealsInputField {
    var unformattedContent:String = ""
    
    
    
    override func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let oldText = textField.text! as NSString
        var newText = oldText.replacingCharacters(in: range, with: string)
        
        //add a check for max count
        if !(newText.count <= maxLenght){
            return false
        }
        
        //change the new text to string
        let newTextString = String(newText)
        
        //filter
        let digits = CharacterSet.decimalDigits
        var digitText = ""
        
        //loop through the sting representation as a collection of unicode scalar value
        for c in (newTextString.unicodeScalars) {
            //check if the values of belongs to the digits collection
            if digits.contains(UnicodeScalar(c.value)!){
                digitText.append("\(c)")
            }
        }
        
        // Format the new string and limit it to an Int value
        if let numOfPennies = Int(digitText) {
            newText = currencyFormat(numOfPennies)
            self.content = self.dollarStringFromInt(numOfPennies) + "." + self.centsStringFromInt(numOfPennies)
        } else {
            newText = "0.00"
            self.content = "0.00"
        }
        
        //set the modified text
        textField.text = newText
        return false
        
    }
    
    private func dollarStringFromInt(_ value: Int) -> String {
        return String(value / 100)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.contentTextField.keyboardType = UIKeyboardType.numberPad
    }
    override func textFieldDidBeginEditing(_ textField: UITextField) {
        
    }
    
    override var content: String {
        set(value){
            if let textfieldText = contentTextField.text {
                if textfieldText.isEmpty {
                    contentTextField.text = value
                }
            }
            self.unformattedContent = value
        }
        get{return self.unformattedContent}
    }
    
    func currencyFormat(_ thetext:Int) -> String {
        var returnString:String!
        let floatString = Float(thetext)
        
        let currencyFormatter = NumberFormatter()
        currencyFormatter.usesGroupingSeparator = true
        currencyFormatter.numberStyle = .currency
        currencyFormatter.locale = Locale(identifier: "en_NG")
        
        if let priceString = currencyFormatter.string(from: NSNumber(value: floatString/100)){
            returnString = priceString
        } else {
            returnString = "0.00"
        }
        return returnString
    }
    
    private func centsStringFromInt(_ value: Int) -> String {
        
        let cents = value % 100
        var centsString = String(cents)
        
        if cents < 10 {
            centsString = "0" + centsString //we have 00
        }
        
        return centsString
    }
}

