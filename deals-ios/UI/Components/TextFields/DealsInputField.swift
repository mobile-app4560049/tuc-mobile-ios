import UIKit
import SnapKit
import Then
import IQKeyboardManagerSwift


class DealsInputField: UIView, UITextFieldDelegate {
    var didEndEditing:(()->())?
    var textDidChange: ((String)->())? = nil
    var textChanged: ((String)->())?

    private var contentMaxLenght = Int.max
    
    @IBInspectable var title: String? {
        set(value){titleLabel.text = value}
        get{return titleLabel.text}
    }
    @IBInspectable var hasAsterisk: Bool {
        set(value){asterisk.isHidden = !value}
        get{return asterisk.isHidden}
    }
    
    @IBInspectable var placeholder: String?{
        set(value){
            contentTextField.toolbarPlaceholder = value
            contentTextField.placeholder = value
        }
        get{return contentTextField.toolbarPlaceholder ?? contentTextField.placeholder}
    }
    
    @IBInspectable var content: String{
        set(value){contentTextField.text = value}
        get{return contentTextField.text ?? ""}
    }
    
    @IBInspectable var rightIcon:UIImage?{
        set(value){iconImageView.image = value}
        get{return iconImageView.image}
    }
    
    @IBInspectable var isSecureTextEntry: Bool{
        set(value){
            contentTextField.isSecureTextEntry = value
            
            if value{
                iconImageView.image = R.image.passwordVisible()
            }else{
                iconImageView.image = R.image.passwordInvisible()

            }
            
        }
        get{
            return contentTextField.isSecureTextEntry
            
        }
    }
    
    @IBInspectable var isEditable: Bool{
        set(value){contentTextField.isUserInteractionEnabled = value}
        get{return contentTextField.isUserInteractionEnabled}
    }
    
    @IBInspectable var maxLenght:Int{
        set(value){contentMaxLenght = value}
        get{return contentMaxLenght}
    }
    
    @IBInspectable var labelFont:UIFont{
        set(value){titleLabel.font = value}
        get{return titleLabel.font}
    }
    
    internal lazy var inputContainer:UIView  = {
        return UIView().then{
            $0.backgroundColor = isEditable ? R.color.white() : R.color.textfieldDisabled()!
            $0.borderColor = R.color.textfieldBorder()
            $0.borderWidth = 0.5
            $0.layer.cornerRadius = 4
        }
    }()
    
    internal lazy var titleLabel: UILabel = {
        return UILabel().then{
            $0.numberOfLines = 0
            $0.lineBreakMode = .byWordWrapping
            $0.font = R.font.brFirmaMedium(size: 12)
            $0.textColor = R.color.textfieldTitleColor()
            $0.textAlignment = .left
        }
    }()
    
    internal lazy var asterisk: UILabel = {
        return UILabel().then{
            $0.text = "*"
            $0.textColor = R.color.red()!
            $0.numberOfLines = 0
            $0.font = R.font.brFirmaMedium(size: 8)
            $0.textAlignment = .left
        }
    }()
    internal lazy var titleStack: UIStackView = {
        return UIStackView().then{
            $0.addArrangedSubview(titleLabel)
            $0.addArrangedSubview(asterisk)
            $0.axis = .horizontal
            $0.alignment = .top
            $0.spacing = .zero
            $0.distribution = .fillProportionally
        }
    }()
    
    
    internal lazy var contentTextField: UITextField =  {
        return UITextField().then{
            $0.font = R.font.brFirmaMedium(size: 14)
            $0.textAlignment = .left
            $0.tintColor = R.color.brightPurple()!
            $0.autocapitalizationType = .none
            $0.autocorrectionType = .no
        }
    }()
    
    
    internal lazy var iconImageView:UIImageView = {
        return UIImageView().then{
            $0.contentMode = .scaleAspectFit
            $0.heightAnchor.constraint(equalToConstant: 14).isActive = true
            $0.widthAnchor.constraint(equalToConstant: 20).isActive = true
        }
    }()
    
    
    override init(frame: CGRect) {super.init(frame: frame)}
    
    required init?(coder aDecoder: NSCoder) {super.init(coder: aDecoder)}
    
    
    override func layoutSubviews() {
        super.layoutSubviews()
        setupView()
    }
    
    
    
    /// Sets the UIKeyboard type for the content fieled
    ///
    /// - Parameter keyboardType: The desired UIKeyboard type
    func setKeyBoardType(_ keyboardType:UIKeyboardType?){
        contentTextField.keyboardType = keyboardType ?? UIKeyboardType.default
    }
    
    func setmaxLenght(_ lenght:Int){
        self.maxLenght = lenght
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let text = textField.text ?? ""
        let nsString = text as NSString
        let newText = nsString.replacingCharacters(in: range, with: string)
        textDidChange?(newText)
        return newText.count <= maxLenght
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        //reset input field color for error
        titleLabel.textColor  = R.color.textfieldTitleColor()!
        inputContainer.borderColor = R.color.textfieldBorder()!
        
    }
    
    func textFieldDidEndEditing(_ textField: UITextField, reason: UITextField.DidEndEditingReason) {
        didEndEditing?()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return self.contentTextField.resignFirstResponder()
    }
    
    private func setupView() {
        
        //add the label subview
        addSubview(titleStack)
        addSubview(inputContainer)
        inputContainer.addSubview(contentTextField)
        inputContainer.addSubview(iconImageView)
    
        
        titleStack.snp.makeConstraints { make in
            make.left.equalToSuperview()
            make.top.equalToSuperview().offset(8)
            if hasAsterisk{
               make.right.equalToSuperview()
            }
        }
        
        
        inputContainer.snp.makeConstraints{make in
            make.bottom.equalTo(titleLabel.snp_bottom).offset(50)
            make.right.equalToSuperview()
            make.left.equalToSuperview()
            make.width.equalToSuperview()
        }
        
        contentTextField.snp.makeConstraints {make in
            make.left.equalToSuperview().offset(8)
            make.top.equalToSuperview().offset(8)
            make.right.equalToSuperview().inset(8)
            make.bottom.equalToSuperview().inset(8)
            make.height.equalTo(30).priority(1000)
        }
        
        //add the right icon
        iconImageView.snp.makeConstraints{make in
            make.right.equalToSuperview().inset(16)
            make.centerY.equalToSuperview()
        }
        
        
        
        contentTextField.delegate = self
        
        if isEditable{
            //add a cick listener to focus on the contentTextField
            self.inputContainer.addTapGestureRecognizer{
                self.contentTextField.becomeFirstResponder()
            }
            
            self.addTapGestureRecognizer{
                self.contentTextField.becomeFirstResponder()
            }
        }
        contentTextField.addTarget(self, action: #selector(self.textFieldTextChanged), for: .editingChanged)
    }
    
    @objc private func textFieldTextChanged() {
        self.textChanged?(content)
    }
}




