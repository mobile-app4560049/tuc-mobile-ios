import UIKit

class StrikeThroughLabel: UILabel {
    
    override func drawText(in rect: CGRect) {
        let attributeString = NSMutableAttributedString(string: self.text ?? "")
        attributeString.addAttributes([NSAttributedString.Key.strikethroughStyle: NSUnderlineStyle.single.rawValue], range: NSMakeRange(0, attributeString.length))
        self.attributedText = attributeString
        super.drawText(in: rect)
    }
}
