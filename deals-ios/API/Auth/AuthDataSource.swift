import Foundation
import RxSwift

protocol AuthDataSource {
    func login(countryIsd: String, mobileNumber: String, accessPin: String, countryId: Int, countryKey: String) -> Observable<ApiResponse<User>>
    
    func createUser(countryIsd: String, mobileNumber: String, accountPin: String, emailAddress: String, name: String, referredBy: String, countryId: Int, countryKey: String) -> Observable<ApiResponse<UserVerificationResponse>>
    
    func verifyUser(countryIsd: String, mobileNumber: String, requestToken: String, accessCode: String, countryId: Int, countryKey: String) -> Observable<ApiResponse<User>>
    
    func resetPin(countryIsd: String, mobileNumber: String, countryId: Int, countryKey: String) -> Observable<ApiResponse<UserVerificationResponse>>
    
    func updatePin(countryIsd: String, mobileNumber: String, tempAccessPin: String, newAccessPin: String, countryId: Int, countryKey: String) -> Observable<EmptyResponse>
}
