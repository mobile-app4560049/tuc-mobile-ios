import Foundation
import RxSwift

class AuthRepository: AuthDataSource {

    /// Logs In an already registered user
    ///
    /// Returns `User`
    func login(countryIsd: String, mobileNumber: String, accessPin: String, countryId: Int, countryKey: String) -> Observable<ApiResponse<User>> {
        return ApiClient.request(AuthApiRouter.userLogIn(countryIsd: countryIsd, mobileNumber: mobileNumber, accessPin: accessPin, countryId: countryId, countryKey: countryKey))
    }
    
    /// Creates a new account for a user
    ///
    /// Returns `User`
    func createUser(countryIsd: String, mobileNumber: String, accountPin: String, emailAddress: String, name: String, referredBy: String, countryId: Int, countryKey: String) -> Observable<ApiResponse<UserVerificationResponse>> {
        return ApiClient.request(AuthApiRouter.userRegistration(countryIsd: countryIsd, mobileNumber: mobileNumber, accountPin: accountPin, emailAddress: emailAddress, name: name, referredBy: referredBy, countryId: countryId, countryKey: countryKey))
    }
    
    /// Verifies a registered account
    ///
    /// Returns `UserVerificationResponse`
    func verifyUser(countryIsd: String, mobileNumber: String, requestToken: String, accessCode: String, countryId: Int, countryKey: String) -> Observable<ApiResponse<User>> {
        return ApiClient.request(AuthApiRouter.userVerification(countryIsd: countryIsd, mobileNumber: mobileNumber, requestToken: requestToken, accessCode: accessCode, countryId: countryId, countryKey: countryKey))
    }
    
    /// Resets a user forgotten password
    ///
    /// Returns `UserVerificationResponse`
    func resetPin(countryIsd: String, mobileNumber: String, countryId: Int, countryKey: String) -> Observable<ApiResponse<UserVerificationResponse>> {
        return ApiClient.request(AuthApiRouter.resetPin(countryIsd: countryIsd, mobileNumber: mobileNumber, countryId: countryId, countryKey: countryKey))
    }
    
    /// Updates a user's pin after it has been reset
    ///
    /// Returns `UserVerificationResponse`
    func updatePin(countryIsd: String, mobileNumber: String, tempAccessPin: String, newAccessPin: String, countryId: Int, countryKey: String) -> Observable<EmptyResponse> {
        return ApiClient.requestSingle(AuthApiRouter.updatePin(countryIsd: countryIsd, mobileNumber: mobileNumber, tempAccessPin: tempAccessPin, newAccessPin: newAccessPin, countryId: countryId, countryKey: countryKey))
    }
    
}
