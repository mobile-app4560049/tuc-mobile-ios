import Foundation
import Alamofire

/// API Routes handler for user authentication
enum AuthApiRouter {
    case userLogIn(countryIsd: String, mobileNumber: String, accessPin: String, countryId: Int, countryKey: String)
    case userRegistration(countryIsd: String, mobileNumber: String, accountPin: String, emailAddress: String, name: String, referredBy: String, countryId: Int, countryKey: String)
    case userVerification(countryIsd: String, mobileNumber: String, requestToken: String, accessCode: String, countryId: Int, countryKey: String)
    case resetPin(countryIsd: String, mobileNumber: String, countryId: Int, countryKey: String)
    case updatePin(countryIsd: String, mobileNumber: String, tempAccessPin: String, newAccessPin: String, countryId: Int, countryKey: String)
}


extension AuthApiRouter {
    /// Auth API Routes url path handler for user authentication
    private var path: String {
        switch self {
        case .userLogIn:
            return "\(ApiService.maddeals)/\(ApiSubService.register)/userlogin"
        case .userRegistration:
            return "\(ApiService.maddeals)/\(ApiSubService.register)/userregistration"
        case .userVerification:
            return "\(ApiService.maddeals)/\(ApiSubService.register)/userverification"
        case .resetPin:
            return "\(ApiService.maddeals)/\(ApiSubService.register)/resetpin"
        case .updatePin:
            return "\(ApiService.maddeals)/\(ApiSubService.register)/updatepin"
        }
    }
}

extension AuthApiRouter {
    /// Auth API Routes url http method handler for user authentication
    private var method: HTTPMethod {
        return .post
    }
}

extension AuthApiRouter {
    
    ///String Value of each task to be carried out
    private var task: String {
        switch self {
        case .userLogIn:
            return "userlogin"
        case .userRegistration:
            return "userregistration"
        case .userVerification:
            return "userverification"
        case .resetPin:
            return "resetpin"
        case .updatePin:
            return "updatepin"
        }
    }
}

extension AuthApiRouter {
    /// Auth API Routes url body parametres handler for user authentication
    private var parameters : Parameters {
        switch self {
            
        case .userLogIn(let countryIsd, let mobileNumber, let accessPin, let countryId, let countryKey):
            let params: [String: Any] = [
                "Task": self.task,
                "CountryIsd": countryIsd,
                "MobileNumber": mobileNumber,
                "AccessPin": accessPin,
                "CountryId": countryId,
                "CountryKey": countryKey
            ]
            return params
            
        case .userRegistration(let countryIsd, let mobileNumber, let accountPin, let emailAddress, let name, let referredBy, let countryId, let countryKey):
            let params: [String: Any] = [
                "Task": self.task,
                "CountryIsd": countryIsd,
                "MobileNumber": mobileNumber,
                "AccountPin": accountPin,
                "EmailAddress": emailAddress,
                "Name": name,
                "ReferredBy": referredBy,
                "CountryId": countryId,
                "CountryKey": countryKey
            ]
            return params
            
        case .userVerification(let countryIsd, let mobileNumber, let requestToken, let accessCode, let countryId, let countryKey) :
            let params: [String: Any] = [
                "Task": self.task,
                "CountryIsd": countryIsd,
                "MobileNumber": mobileNumber,
                "RequestToken": requestToken,
                "AccessCode": accessCode,
                "CountryId": countryId,
                "CountryKey": countryKey
            ]
            return params
            
        case .resetPin(let countryIsd, let mobileNumber, let countryId, let countryKey):
            let params: [String: Any] = [
                "Task": self.task,
                "CountryIsd": countryIsd,
                "MobileNumber": mobileNumber,
                "CountryId": countryId,
                "CountryKey": countryKey
            ]
            return params
            
        case .updatePin(let countryIsd, let mobileNumber, let tempAccessPin, let newAccessPin, let countryId, let countryKey):
            let params: [String: Any] = [
                "Task": self.task,
                "CountryIsd": countryIsd,
                "MobileNumber": mobileNumber,
                "TempAccessPin": tempAccessPin,
                "NewAccessPin": newAccessPin,
                "CountryId": countryId,
                "CountryKey": countryKey
            ]
            return params
        }
    }
}

extension AuthApiRouter {
    /// Auth API Routes url body encoding handler for user authentication
    private var encoding: ParameterEncoding {
        return JSONEncoding.default
    }
}

extension AuthApiRouter: URLRequestConvertible {
    func asURLRequest() throws -> URLRequest {
        let url = try Configuration.environment.baseURL.asURL()
        let endpoint = path
        var urlRequest = URLRequest(url: url.appendingPathComponent(endpoint))
        
        if path.contains("?") {
            let alternativeUrlStringPath = Configuration.environment.baseURL + endpoint
            let alternativeUrl = try alternativeUrlStringPath.asURL()
            urlRequest = URLRequest(url: alternativeUrl)
        }
        
        urlRequest.httpMethod = method.rawValue
        
        if let parameters = self.parameters.removeNullValues() as? Parameters, !parameters.isEmpty{
            urlRequest = try encoding.encode(urlRequest, with: parameters)
        }
        
        if let bodyData = urlRequest.httpBody {
            URLProtocol.setProperty(bodyData, forKey: "NFXBodyData", in: urlRequest as! NSMutableURLRequest)
        }
    
        var request = ""
        if let body = urlRequest.httpBody?.base64EncodedString(){
            request.append(body)
        }
        if let query = url.query{
            request.append(query)
        }
        
        let headers = [
            "Authorization": Configuration.environment.authorizationKey,
            "Content-Type": "charset=utf-8"
        ]
        
        headers.forEach { key, value in
            urlRequest.addValue(value, forHTTPHeaderField: key)
        }
        
        return urlRequest
    }
    
    
}
