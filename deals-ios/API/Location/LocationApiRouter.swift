import Foundation
import Alamofire


enum LocationApiRouter {
    case getStates
    case getCities(state: State)
    case saveAddress(name: String, address: String, address2: String, number: String, number2: String, state: State, city: City)
    case getUserAddressList(offSet: Int)
}

extension LocationApiRouter {
    /// Location API Routes url path handler for user authentication
    private var path: String {
        switch self {
        case .getStates:
            return "\(ApiService.maddeals)/\(ApiSubService.system)/getstates"
        case .getCities:
            return "\(ApiService.maddeals)/\(ApiSubService.system)/getcities"
        case .saveAddress:
            return "\(ApiService.maddeals)/\(ApiSubService.system)/saveaddress"
        case .getUserAddressList:
            return "\(ApiVersion.v3)/\(ApiService.customer)/\(ApiSubService.address)/getaddresses"
        }
    }
}

extension LocationApiRouter {
    /// Location API Routes url http method handler for user authentication
    private var method: HTTPMethod {
        return .post
    }
}

extension LocationApiRouter {
    
    ///String Value of each task to be carried out
    private var task: String {
        switch self {
        case .getStates:
            return "getstates"
        case .getCities:
            return "getcities"
        case .saveAddress:
            return "saveaddress"
        case .getUserAddressList:
            return "getaddresses"
        }
    }
}

extension LocationApiRouter {
    /// Location API Routes url body parametres handler for user authentication
    private var parameters : Parameters {
        switch self {
        case .getStates:
            var params: [String: Any] = [
                "Task": self.task,
                "Offset": 0,
                "Limit": 1000,
            ]
            if let country = DealsKeyValueRepository.sharedInstance.getCountryForApi() {
                params["ReferenceId"] = country.id
                params["ReferenceKey"] = country.countryKey
                params["CountryId"] = country.id
                params["CountryKey"] = country.countryKey
                
            } else {
                params["CountryId"] = 1
                params["CountryKey"] = "nigeria"
                params["CountryId"] = 1
                params["CountryKey"] = "nigeria"
            }
            return params
            
        case .getCities(state: let state):
            var params: [String: Any] = [
                "Task": self.task,
                "Offset": 0,
                "Limit": 1000,
                "ReferenceId": state.referenceID,
                "ReferenceKey": state.referenceKey,
            ]
            if let country = DealsKeyValueRepository.sharedInstance.getCountryForApi() {
                params["CountryId"] = country.id
                params["CountryKey"] = country.countryKey
                
            } else {
                params["CountryId"] = 1
                params["CountryKey"] = "nigeria"
            }
            return params
        case .saveAddress(name: let name, address: let address, address2: let address2, number: let number, number2: let number2, state: let state, city: let city):
            guard let user = DealsKeyValueRepository.sharedInstance.getUser() else {return [:]}
            var params: [String: Any] = [
                "Task": self.task,
                "AccountId": user.accountId,
                "AccountKey": user.accountKey,
                "ReferenceId": user.accountId,
                "ReferenceKey": user.accountKey,
                "Name": name,
                "ContactNumber": number,
                "EmailAddress": user.emailAddress,
                "AddressLine1": address,
                "AddressLine2": address2,
                "Landmark": "",
                "AlternateMobileNumber": number2,
                "CityAreaId": 0,
                "CityAreaKey": "",
                "CityAreaName": "",
                "CityId": city.referenceID,
                "CityKey": city.referenceKey,
                "CityName": city.name,
                "StateId": state.referenceID,
                "StateKey": state.referenceKey,
                "StateName": state.name,
                "ZipCode": "",
                "Instructions": "",
                "MapAddress": "",
                "Latitude": -1,
                "Longitude": -1,
                "IsPrimary": false,
                "LocationTypeId": 800
            ]
            if let country = DealsKeyValueRepository.sharedInstance.getCountryForApi() {
                params["CountryName"] = country.countryKey.firstUppercased
                params["CountryId"] = country.id
                params["CountryKey"] = country.countryKey
                
            } else {
                params["CountryName"] = "Nigeria"
                params["CountryId"] = 1
                params["CountryKey"] = "nigeria"
            }
            return params
        case .getUserAddressList(offSet: let offset):
            guard let user = DealsKeyValueRepository.sharedInstance.getUser() else {return [:]}
            let params: [String: Any] = [
                "Task": self.task,
                "TotalRecords": -1,
                "Offset": offset,
                "Limit": 100,
                "RefreshCount": true,
                "SearchCondition": "",
                "AccountId": user.accountId,
                "AccountKey": user.accountKey
            ]
            return params
        }
    }
}

extension LocationApiRouter {
    /// Deals API Routes url body encoding handler for user authentication
    private var encoding: ParameterEncoding {
        return JSONEncoding.default
    }
}


extension LocationApiRouter: URLRequestConvertible {
    func asURLRequest() throws -> URLRequest {
        let url = try Configuration.environment.baseURL.asURL()
        let endpoint = path
        var urlRequest = URLRequest(url: url.appendingPathComponent(endpoint))
        
        if path.contains("?") {
            let alternativeUrlStringPath = Configuration.environment.baseURL + endpoint
            let alternativeUrl = try alternativeUrlStringPath.asURL()
            urlRequest = URLRequest(url: alternativeUrl)
        }
        
        urlRequest.httpMethod = method.rawValue
        
        if let parameters = self.parameters.removeNullValues() as? Parameters, !parameters.isEmpty{
            urlRequest = try encoding.encode(urlRequest, with: parameters)
        }
        
        if let bodyData = urlRequest.httpBody {
            URLProtocol.setProperty(bodyData, forKey: "NFXBodyData", in: urlRequest as! NSMutableURLRequest)
        }
        
        var request = ""
        if let body = urlRequest.httpBody?.base64EncodedString(){
            request.append(body)
        }
        if let query = url.query{
            request.append(query)
        }
        
        let headers = [
            "Authorization": Configuration.environment.authorizationKey,
            "Content-Type": "charset=utf-8"
        ]
        
        headers.forEach { key, value in
            urlRequest.addValue(value, forHTTPHeaderField: key)
        }
        
        return urlRequest
    }
}

