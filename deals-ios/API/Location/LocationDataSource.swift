import Foundation
import RxSwift

protocol LocationDataSource {
    func getStates() -> Observable<ApiResponse<GenericDealsResponse<[State]>>>
    func getCities(state: State) -> Observable<ApiResponse<GenericDealsResponse<[City]>>>
    func saveAddress(
        name: String, address: String,
        address2: String, number: String,
        number2: String, state: State,
        city: City) -> Observable<EmptyResponse>
    func getUserAddressList(offset: Int) -> Observable<ApiResponse<GenericDealsResponse<[UserAddress]>>>
}
