import Foundation
import RxSwift

class LocationRepository: LocationDataSource {
    
    func getStates() -> RxSwift.Observable<ApiResponse<GenericDealsResponse<[State]>>> {
        return ApiClient.request(LocationApiRouter.getStates)
    }
    
    func getCities(state: State) -> RxSwift.Observable<ApiResponse<GenericDealsResponse<[City]>>> {
        return ApiClient.request(LocationApiRouter.getCities(state: state))
    }
    
    func saveAddress(name: String, address: String, address2: String, number: String, number2: String, state: State, city: City) -> RxSwift.Observable<EmptyResponse> {
        return ApiClient.request(LocationApiRouter.saveAddress(name: name, address: address, address2: address2, number: number, number2: number2, state: state, city: city))
    }
    
    func getUserAddressList(offset: Int) -> RxSwift.Observable<ApiResponse<GenericDealsResponse<[UserAddress]>>> {
        return ApiClient.request(LocationApiRouter.getUserAddressList(offSet: offset))
    }
    
}
