import Foundation
import RxSwift

protocol BuyDealDataSource {
    func buyDealInitialize(
        deal: Deal,
        paymentSource: PaymentSource,
        quantity: Int) -> Observable<ApiResponse<BuyDealInitResponse>>
    
    func buyPointInitialize(
        deal: Deal,
        amount: String,
        paymentMode: String
    ) -> Observable<ApiResponse<BuyPointInitResponse>>
    
    func buyPointConfirm(
        amount: Int,
        paymentReference: String,
        refTransactionId: String,
        refStatus: String,
        refMessage: String,
        paymentMode: String
    ) -> Observable<ApiResponse<BuyPointConfirmResponse>>
    
    func buyDealConfirm(
        dealInitResp: BuyDealInitResponse,
        deal: Deal,
        quantity: Int,
        buyPointConfRes: BuyPointConfirmResponse
    ) -> Observable<ApiResponse<BuyDealConfirmResponse>>
    
    func getDealCode(
        bbuyDealResp: BuyDealConfirmResponse
    ) -> Observable<ApiResponse<DealCodeResponse>>
}
