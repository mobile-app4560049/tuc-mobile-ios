import Foundation
import RxSwift

class BuyDealRepository: BuyDealDataSource {

    func buyDealInitialize(
        deal: Deal,
        paymentSource: PaymentSource,
        quantity: Int) -> RxSwift.Observable<ApiResponse<BuyDealInitResponse>> {
        return ApiClient.request(BuyDealApiRouter.buyDealInitialize(deal: deal, paymentSource: paymentSource, quantity: quantity))
    }
    
    func buyPointInitialize(deal: Deal, amount: String, paymentMode: String) -> RxSwift.Observable<ApiResponse<BuyPointInitResponse>> {
        return ApiClient.request(BuyDealApiRouter.buyPointInitialize(deal: deal, amount: amount, paymentMode: paymentMode))
    }
    
    func buyPointConfirm(amount: Int, paymentReference: String, refTransactionId: String, refStatus: String, refMessage: String, paymentMode: String) -> RxSwift.Observable<ApiResponse<BuyPointConfirmResponse>> {
        return ApiClient.request(BuyDealApiRouter.buyPointConfirm(amount: amount, paymentReference: paymentReference, refTransactionId: refTransactionId, refStatus: refStatus, refMessage: refMessage, paymentMode: paymentMode))
    }
    
    func buyDealConfirm(dealInitResp: BuyDealInitResponse, deal: Deal, quantity: Int, buyPointConfRes: BuyPointConfirmResponse) -> RxSwift.Observable<ApiResponse<BuyDealConfirmResponse>> {
        return ApiClient.request(BuyDealApiRouter.buyDealConfirm(dealInitResp: dealInitResp, deal: deal, quantity: quantity, buyPointConfRes: buyPointConfRes))
    }
    
    func getDealCode(bbuyDealResp: BuyDealConfirmResponse) -> RxSwift.Observable<ApiResponse<DealCodeResponse>> {
        return ApiClient.request(BuyDealApiRouter.getDealCode(dealConfirmResp: bbuyDealResp))
    }
    
    
}
