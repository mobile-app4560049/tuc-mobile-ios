import Alamofire


enum BuyDealApiRouter {
    case buyDealInitialize(deal: Deal, paymentSource: PaymentSource, quantity: Int)
    case buyPointInitialize(deal: Deal, amount: String, paymentMode: String)
    case buyPointConfirm(amount: Int, paymentReference: String, refTransactionId: String, refStatus: String, refMessage: String, paymentMode: String)
    case buyDealConfirm(dealInitResp: BuyDealInitResponse, deal: Deal, quantity: Int, buyPointConfRes: BuyPointConfirmResponse)
    case getDealCode(dealConfirmResp: BuyDealConfirmResponse)
    
}


extension BuyDealApiRouter {
    /// Buy Deals API Routes url path handler for user authentication
    private var path: String {
        switch self {
        case .buyDealInitialize:
            return "\(ApiVersion.v3)/\(ApiService.deals)/\(ApiSubService.operation)/buydeal_initizalize"
        case .buyPointInitialize:
            return "\(ApiVersion.v3)/\(ApiService.customer)/\(ApiSubService.transaction)/buypointinitialize"
        case .buyPointConfirm:
            return "\(ApiVersion.v3)/\(ApiService.customer)/\(ApiSubService.transaction)/buypointverify"
        case .buyDealConfirm:
            return "\(ApiVersion.v3)/\(ApiService.deals)/\(ApiSubService.operation)/buydeal_confirm"
        case .getDealCode:
            return "\(ApiVersion.v3)/\(ApiService.customer)/\(ApiSubService.deals)/getdealcode"
        }
    }
}

extension BuyDealApiRouter {
    /// Buy Deals API Routes url http method handler for user authentication
    private var method: HTTPMethod {
        return .post
    }
}

extension BuyDealApiRouter {
    
    ///String Value of each task to be carried out
    private var task: String {
        switch self {
        case .buyDealInitialize:
            return "buydeal_initizalize"
        case .buyPointInitialize:
            return "buypointinitialize"
        case .buyPointConfirm:
            return "buypointverify"
        case .buyDealConfirm:
            return "buydeal_confirm"
        case .getDealCode:
            return "getdealcode"
        }
    }
}

extension BuyDealApiRouter {
    /// Buy Deals  API Routes url body parametres handler for user authentication
    private var parameters : Parameters {
        switch self {
        case .buyDealInitialize(deal: let deal, paymentSource: let paymentSource, quantity: let quantity):
            var params: [String: Any] = [
                "Task": self.task,
                "ReferenceId": deal.referenceID,
                "ReferenceKey": deal.referenceKey,
                "PaymentSource": paymentSource.literal,
                "ItemCount": quantity
            ]
            if let country = DealsKeyValueRepository.sharedInstance.getCountryForApi() {
                params["CountryId"] = country.id
                params["CountryKey"] = country.countryKey
            } else {
                params["CountryId"] = 1
                params["CountryKey"] = "nigeria"
            }
            return params
            
        case .buyPointInitialize(deal: let deal, amount: let amount, paymentMode: let paymentMode):
            let params: [String: Any] = [
                "Task": self.task,
                "Amount": amount,
                "PaymentMode": paymentMode,
                "MerchantId": deal.merchantID,
                "DealId": deal.referenceID,
                "DealKey": deal.referenceKey,
            ]
            return params
            
        case .buyPointConfirm(amount: let amount, paymentReference: let paymentReference, refTransactionId: let refTransactionId, refStatus: let refStatus, refMessage: let refMessage, paymentMode: let paymentMode):
            let params: [String: Any] = [
                "Task": self.task,
                "Amount": amount,
                "PaymentReference": paymentReference,
                "RefTransactionId": refTransactionId,
                "RefStatus": refStatus,
                "RefMessage": refMessage,
                "PaymentMode": paymentMode
            ]
            return params
        case .buyDealConfirm(dealInitResp: let dealInitResp, deal: let deal, quantity: let quantity, buyPointConfRes: let buyPointConfRes):
            var params: [String: Any] = [
                "Task": self.task,
                "ReferenceId": dealInitResp.referenceID,
                "ReferenceKey": dealInitResp.referenceKey,
                "DealId": deal.referenceID,
                "DealKey": deal.referenceKey,
                "PaymentReference": dealInitResp.referenceKey,
                "TransactionId": buyPointConfRes.referenceID,
                "Amount": dealInitResp.amount,
                "Charge": dealInitResp.fee,
                "DeliveryCharge": dealInitResp.deliveryCharge,
                "TotalAmount": dealInitResp.totalAmount,
                "ItemCount": quantity,
                "PaymentSource": "wallet",
            ]
            if let country = DealsKeyValueRepository.sharedInstance.getCountryForApi() {
                params["CountryId"] = country.id
                params["CountryKey"] = country.countryKey
            } else {
                params["CountryId"] = 1
                params["CountryKey"] = "nigeria"
            }
            return params
            
        case .getDealCode(dealConfirmResp: let dealConfirmResp):
            var params: [String: Any] = [
                "Task": self.task,
                "DealCodeKey": dealConfirmResp.referenceKey,
                "ReferenceId": dealConfirmResp.referenceID,
                "ReferenceKey": dealConfirmResp.referenceKey,
            ]
            if let country = DealsKeyValueRepository.sharedInstance.getCountryForApi() {
                params["CountryId"] = country.id
                params["CountryKey"] = country.countryKey
            } else {
                params["CountryId"] = 1
                params["CountryKey"] = "nigeria"
            }
            return params
        }
    }
}

extension BuyDealApiRouter {
    /// Buy Deals API Routes url body encoding handler for user authentication
    private var encoding: ParameterEncoding {
        return JSONEncoding.default
    }
}


extension BuyDealApiRouter: URLRequestConvertible {
    func asURLRequest() throws -> URLRequest {
        let url = try Configuration.environment.baseURL.asURL()
        let endpoint = path
        var urlRequest = URLRequest(url: url.appendingPathComponent(endpoint))
        
        if path.contains("?") {
            let alternativeUrlStringPath = Configuration.environment.baseURL + endpoint
            let alternativeUrl = try alternativeUrlStringPath.asURL()
            urlRequest = URLRequest(url: alternativeUrl)
        }
        
        urlRequest.httpMethod = method.rawValue
        
        if let parameters = self.parameters.removeNullValues() as? Parameters, !parameters.isEmpty{
            urlRequest = try encoding.encode(urlRequest, with: parameters)
        }
        
        if let bodyData = urlRequest.httpBody {
            URLProtocol.setProperty(bodyData, forKey: "NFXBodyData", in: urlRequest as! NSMutableURLRequest)
        }
        
        var request = ""
        if let body = urlRequest.httpBody?.base64EncodedString(){
            request.append(body)
        }
        if let query = url.query{
            request.append(query)
        }
        
        var headers = [
            "Authorization": Configuration.environment.authorizationKey,
            "Content-Type": "charset=utf-8"
        ]
        
        if let user = DealsKeyValueRepository.sharedInstance.getUser() {
            headers["ask"] = user.key
        }
        
        headers.forEach { key, value in
            urlRequest.addValue(value, forHTTPHeaderField: key)
        }
        
        return urlRequest
    }
    
    
}
