import Foundation
import Alamofire


enum DealsApiRouter {
    case getMerchantCategories
    case getCategories
    case getMerchants
    case getDeals(
        offset: Int,
        searchCondition: String,
        sortExpression: String,
        categories: [String],
        merchants: [String],
        city: String,
        location: String
    )
    case getAllDeals(
        offset: Int,
        searchCondition: String,
        sortExpression: String,
        categories: [String],
        merchants: [String],
        city: String,
        cityId: Int,
        cityKey: String
    )
}


extension DealsApiRouter {
    /// Deals API Routes url path handler for user authentication
    private var path: String {
        switch self {
        case .getMerchantCategories:
            return "\(ApiVersion.v3)/\(ApiService.customer)/\(ApiSubService.category)/getmerchantcategories"
        case .getCategories:
            return "\(ApiVersion.v3)/\(ApiService.customer)/\(ApiSubService.category)/getcategories"
        case .getMerchants:
            return "\(ApiVersion.v3)/\(ApiService.customer)/\(ApiSubService.category)/getmerchants"
        case .getDeals:
            return "\(ApiVersion.v3)/\(ApiService.customer)/\(ApiSubService.deals)/getdealpromotions"
        case .getAllDeals:
            return "\(ApiVersion.v3)/\(ApiService.customer)/\(ApiSubService.deals)/getdeals"
        }
    }
}

extension DealsApiRouter {
    /// Deals API Routes url http method handler for user authentication
    private var method: HTTPMethod {
        return .post
    }
}

extension DealsApiRouter {
    
    ///String Value of each task to be carried out
    private var task: String {
        switch self {
        case .getMerchantCategories:
            return "getmerchantcategories"
        case .getCategories:
            return "getcategories"
        case .getMerchants:
            return "getmerchants"
        case .getDeals:
            return "getdealpromotions"
        case .getAllDeals:
            return "getdeals"
        }
    }
}

extension DealsApiRouter {
    /// Deals API Routes url body parametres handler for user authentication
    private var parameters : Parameters {
        switch self {
        case .getMerchantCategories:
            let params: [String: Any] = [
                "Task": self.task,
                "Offset": 0,
                "Limit": 200]
            return params
            
        case .getCategories:
            let params: [String: Any] = [
                "Task": self.task,
                "Offset": 0,
                "Limit": 100]
            return params
        case .getMerchants:
            let params: [String: Any] = [
                "Task": self.task,
                "Offset": 0,
                "Limit": 200]
            return params
        case .getDeals(offset: let offset, searchCondition: let searchCondition, sortExpression: let sortExpression, categories: let categories, merchants: let merchants, city: let city, location: let location):
            
            var params: [String: Any] = [
                "Task": self.task,
                "Offset": offset,
                "Limit": 20,
                "SearchCondition": searchCondition,
                "SortExpression": sortExpression,
                "Categories": categories,
                "Merchants": merchants,
                "City": city,
                "Location": location,
            ]
            if let country = DealsKeyValueRepository.sharedInstance.getCountryForApi() {
                params["CountryId"] = country.id
                params["CountryKey"] = country.countryKey
            } else {
                params["CountryId"] = 1
                params["CountryKey"] = "nigeria"
            }
            return params
        case .getAllDeals(offset: let offset, searchCondition: let searchCondition, sortExpression: let sortExpression, categories: let categories, merchants: let merchants, city: let city, cityId: let cityId, cityKey: let cityKey):
            var params: [String: Any] = [
                "Task": self.task,
                "Offset": offset,
                "Limit": 10,
                "SearchCondition": searchCondition,
                "SortExpression": sortExpression,
                "Categories": categories,
                "Merchants": merchants,
                "City": city,
                "CityId": cityId,
                "CityKey": cityKey,
                "Latitude": 0,
                "Longitude": 0
            ]
            
            if let country = DealsKeyValueRepository.sharedInstance.getCountryForApi() {
                params["CountryId"] = country.id
                params["CountryKey"] = country.countryKey
            } else {
                params["CountryId"] = 1
                params["CountryKey"] = "nigeria"
            }
            return params
        }
    }
}

extension DealsApiRouter {
    /// Deals API Routes url body encoding handler for user authentication
    private var encoding: ParameterEncoding {
        return JSONEncoding.default
    }
}


extension DealsApiRouter: URLRequestConvertible {
    func asURLRequest() throws -> URLRequest {
        let url = try Configuration.environment.baseURL.asURL()
        let endpoint = path
        var urlRequest = URLRequest(url: url.appendingPathComponent(endpoint))
        
        if path.contains("?") {
            let alternativeUrlStringPath = Configuration.environment.baseURL + endpoint
            let alternativeUrl = try alternativeUrlStringPath.asURL()
            urlRequest = URLRequest(url: alternativeUrl)
        }
        
        urlRequest.httpMethod = method.rawValue
        
        if let parameters = self.parameters.removeNullValues() as? Parameters, !parameters.isEmpty{
            urlRequest = try encoding.encode(urlRequest, with: parameters)
        }
        
        if let bodyData = urlRequest.httpBody {
            URLProtocol.setProperty(bodyData, forKey: "NFXBodyData", in: urlRequest as! NSMutableURLRequest)
        }
    
        var request = ""
        if let body = urlRequest.httpBody?.base64EncodedString(){
            request.append(body)
        }
        if let query = url.query{
            request.append(query)
        }
        
        let headers = [
            "Authorization": Configuration.environment.authorizationKey,
            "Content-Type": "charset=utf-8"
        ]
        
        headers.forEach { key, value in
            urlRequest.addValue(value, forHTTPHeaderField: key)
        }
        
        return urlRequest
    }
    
    
}
