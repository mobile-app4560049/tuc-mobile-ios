import Foundation
import RxSwift

class DealsRepository: DealsDataSource {
    
    func getMerchantCategories() -> RxSwift.Observable<ApiResponse<GenericDealsResponse<[MerchantCategory]>>> {
        return ApiClient.request(DealsApiRouter.getMerchantCategories)
    }
    
    func getCategories() -> RxSwift.Observable<ApiResponse<GenericDealsResponse<[Category]>>> {
        return ApiClient.request(DealsApiRouter.getCategories)
    }
    
    func getMerchants() -> RxSwift.Observable<ApiResponse<GenericDealsResponse<[Merchant]>>> {
        return ApiClient.request(DealsApiRouter.getMerchants)
    }
    
    func getTopDeals(offset: Int, searchCondition: String, sortExpression: String, categories: [String], merchants: [String], city: String, location: String) -> RxSwift.Observable<ApiResponse<GenericDealsResponse<[Deal]>>> {
        return ApiClient.request(DealsApiRouter.getDeals(offset: offset, searchCondition: searchCondition, sortExpression: sortExpression, categories: categories, merchants: merchants, city: city, location: location))
    }
    
    func getAllDeals(offset: Int, searchCondition: String, sortExpression: String, categories: [String], merchants: [String], city: String, cityId: Int, cityKey: String) -> RxSwift.Observable<ApiResponse<GenericDealsResponse<[Deal]>>> {
        return ApiClient.request(DealsApiRouter.getAllDeals(offset: offset, searchCondition: searchCondition, sortExpression: sortExpression, categories: categories, merchants: merchants, city: city, cityId: cityId, cityKey: cityKey))
    }
   
}
