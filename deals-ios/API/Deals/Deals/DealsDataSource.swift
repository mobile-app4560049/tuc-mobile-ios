import Foundation
import RxSwift

protocol DealsDataSource {
    func getMerchantCategories() -> Observable<ApiResponse<GenericDealsResponse<[MerchantCategory]>>>
    func getCategories() -> Observable<ApiResponse<GenericDealsResponse<[Category]>>>
    func getMerchants() -> Observable<ApiResponse<GenericDealsResponse<[Merchant]>>>
    func getTopDeals(
        offset: Int,
        searchCondition: String,
        sortExpression: String,
        categories: [String],
        merchants: [String],
        city: String,
        location: String
    ) -> Observable<ApiResponse<GenericDealsResponse<[Deal]>>>
    func getAllDeals(
        offset: Int,
        searchCondition: String,
        sortExpression: String,
        categories: [String],
        merchants: [String],
        city: String,
        cityId: Int,
        cityKey: String
    ) -> Observable<ApiResponse<GenericDealsResponse<[Deal]>>>
}
