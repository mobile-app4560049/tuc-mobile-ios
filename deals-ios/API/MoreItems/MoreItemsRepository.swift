import Foundation
import RxSwift

class MoreItemsRepository: MoreItemsDataSource {
    
    func getUserAccount() -> RxSwift.Observable<ApiResponse<User>> {
        return ApiClient.request(MoreItemsApiRouter.getUserAccount)
    }
    
    func updateAccount(name: String?, genderCode: String?) -> RxSwift.Observable<ApiResponse<String>> {
        return ApiClient.request(MoreItemsApiRouter.updateAccount(name: name, genderCode: genderCode))
    }
    
    func changePin(oldPin: String, newPin: String) -> RxSwift.Observable<ApiResponse<PinModel>> {
        return ApiClient.request(MoreItemsApiRouter.changePin(oldPin: oldPin, newPin: newPin))
    }
    
    func updateRefId(referralId: String) -> RxSwift.Observable<EmptyResponse> {
        return ApiClient.requestSingle(MoreItemsApiRouter.updateRefId(referralId: referralId))
    }
    
}
