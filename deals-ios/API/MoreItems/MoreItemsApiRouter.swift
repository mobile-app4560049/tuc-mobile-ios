import Foundation
import Alamofire

enum MoreItemsApiRouter {
    case getUserAccount
    case updateAccount(name: String?, genderCode: String?)
    case changePin(oldPin: String, newPin: String)
    case updateRefId(referralId: String)
}



extension MoreItemsApiRouter {
    /// API Routes url path handler for user authentication
    private var path: String {
        switch self {
        case .getUserAccount:
            return "\(ApiService.maddeals)/\(ApiSubService.account)/getuseraccount"
        case .updateAccount:
            return "\(ApiService.maddeals)/\(ApiSubService.account)/updateuseraccount"
        case .changePin:
            return "\(ApiService.maddeals)/\(ApiSubService.register)/changepin"
        case .updateRefId:
            return "\(ApiService.maddeals)/\(ApiSubService.account)/updaterefid"
        }
    }
}

extension MoreItemsApiRouter {
    /// API Routes url http method handler for user authentication
    private var method: HTTPMethod {
        return .post
    }
}

extension MoreItemsApiRouter {
    
    ///String Value of each task to be carried out
    private var task: String {
        switch self {
        case .getUserAccount:
            return "getuseraccount"
        case .updateAccount:
            return "updateuseraccount"
        case .changePin:
            return "updatepin"
        case .updateRefId:
            return "updaterefid"
        }
    }
}

extension MoreItemsApiRouter {
    /// API Routes url body parametres handler for user authentication
    private var parameters : Parameters {
        
        guard let user = DealsKeyValueRepository.sharedInstance.getUser() else {return [:]}
        switch self {
        case .getUserAccount:
            let params: [String: Any] = [
                "Task": self.task,
                "Reference": "ReferenceKey = \(user.accountKey)"
            ]
            return params
        case .updateAccount(let name, let genderCode):
            var params: [String: Any] = [
                "AuthAccountId" : user.accountId,
            ]
            if let name = name {
                params["Name"] = name
            }
            if let genderCode = genderCode {
                params["Gender"] = genderCode
            }
            return params
        case .changePin(oldPin: let oldPin, newPin: let newPin):
            var params: [String: Any] = [
                "Task": self.task,
                "AuthAccountKey": user.accountKey,
                "CountryIsd": user.country.isd,
                "OldPin": oldPin,
                "NewPin": newPin,
                "CountryKey": user.country.name.lowercased()
            ]
            switch user.name.lowercased(){
            case SelectedCountry.nigeria.countryKey:
                params["CountryId"] = SelectedCountry.nigeria.id
            case SelectedCountry.ghana.countryKey:
                params["CountryId"] = SelectedCountry.ghana.id
            case SelectedCountry.kenya.countryKey:
                params["CountryId"] = SelectedCountry.kenya.id
            case SelectedCountry.uganda.countryKey:
                params["CountryId"] = SelectedCountry.uganda.id
            case SelectedCountry.rwanda.countryKey:
                params["CountryId"] = SelectedCountry.rwanda.id
            default:
                break
            }
            return params
        case .updateRefId(referralId: let id):
            var params: [String: Any] = [
                "Task": self.task,
                "emailAddress": user.emailAddress,
                "ReferralCode": id,
                "CountryKey": user.country.name.lowercased()
            ]
            switch user.country.name.lowercased(){
            case SelectedCountry.nigeria.countryKey:
                params["CountryId"] = SelectedCountry.nigeria.id
            case SelectedCountry.ghana.countryKey:
                params["CountryId"] = SelectedCountry.ghana.id
            case SelectedCountry.kenya.countryKey:
                params["CountryId"] = SelectedCountry.kenya.id
            case SelectedCountry.uganda.countryKey:
                params["CountryId"] = SelectedCountry.uganda.id
            case SelectedCountry.rwanda.countryKey:
                params["CountryId"] = SelectedCountry.rwanda.id
            default:
                break
            }
            return params
        }
    }
    
}


extension MoreItemsApiRouter {
    /// API Routes url body encoding handler for user authentication
    private var encoding: ParameterEncoding {
        return JSONEncoding.default
    }
}

extension MoreItemsApiRouter: URLRequestConvertible {
    func asURLRequest() throws -> URLRequest {
        let url = try Configuration.environment.baseURL.asURL()
        let endpoint = path
        var urlRequest = URLRequest(url: url.appendingPathComponent(endpoint))
        
        if path.contains("?") {
            let alternativeUrlStringPath = Configuration.environment.baseURL + endpoint
            let alternativeUrl = try alternativeUrlStringPath.asURL()
            urlRequest = URLRequest(url: alternativeUrl)
        }
        
        urlRequest.httpMethod = method.rawValue
        
        if let parameters = self.parameters.removeNullValues() as? Parameters, !parameters.isEmpty{
            urlRequest = try encoding.encode(urlRequest, with: parameters)
        }
        
        if let bodyData = urlRequest.httpBody {
            URLProtocol.setProperty(bodyData, forKey: "NFXBodyData", in: urlRequest as! NSMutableURLRequest)
        }
    
        var request = ""
        if let body = urlRequest.httpBody?.base64EncodedString(){
            request.append(body)
        }
        if let query = url.query{
            request.append(query)
        }
        
        let headers = [
            "Authorization": Configuration.environment.authorizationKey,
            "Content-Type": "charset=utf-8"
        ]
        
        headers.forEach { key, value in
            urlRequest.addValue(value, forHTTPHeaderField: key)
        }
        
        return urlRequest
    }
    
    
}
