import Foundation
import RxSwift

protocol MoreItemsDataSource {
    func getUserAccount() -> Observable<ApiResponse<User>>
    func updateAccount(name: String?, genderCode: String?) -> Observable<ApiResponse<String>>
    func changePin(oldPin: String, newPin: String) -> Observable<ApiResponse<PinModel>>
    func updateRefId(referralId: String) -> Observable<EmptyResponse>
}
