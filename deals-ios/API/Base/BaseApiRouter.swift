import Alamofire

enum BaseApiRouter: URLRequestConvertible {
    
    /// API body parameters
    var parameters: Parameters {
        return ["test" : "test"]
    }
    
    /// API method type (post, get, put, patch, delete)
    var method: HTTPMethod {
        return .post
    }
    /// API Url bath excluding the base url
    var path: String {
        return ""
    }
    
    /// API parameter encoding type (JSONEncoding, UrlEncoding)
    var parameterEncoding: Alamofire.ParameterEncoding {
        return JSONEncoding.default
    }
    
    /// API base url
    var baseURLString: String {
        return Configuration.environment.baseURL
    }
    
      /// Converts all of the params, paths and methos into a URLRequest
      ///
      /// - Returns: UrlRequst tha can be used by Alamorefire
      /// - Throws: Exception during conversion
      func asURLRequest() throws -> URLRequest {
        let url = try baseURLString.asURL()
        var urlRequest = URLRequest(url: url.appendingPathComponent(path))
        urlRequest.httpMethod = method.rawValue
        /// ignores the request body for .get and .delete request
        if let parameters = self.parameters.removeNullValues() as? Parameters, !parameters.isEmpty{
          urlRequest = try parameterEncoding.encode(urlRequest, with: parameters)
        }

          let headers = [
          "client-key":"\("Configuration.environment.clientKey")"
        ]
        debugPrint("header>>>>>\("Configuration.environment.clientKey")")
        headers.forEach { key, value in
          urlRequest.addValue(value, forHTTPHeaderField: key)
        }
        if let bodyData = urlRequest.httpBody {
          URLProtocol.setProperty(bodyData, forKey: "NFXBodyData", in: urlRequest as! NSMutableURLRequest)
        }
        debugPrint(urlRequest)
        return urlRequest
      }
    
}
