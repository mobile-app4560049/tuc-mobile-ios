import UIKit
import netfox
import RxSwift
import Alamofire
import FCUUID

func fromJSONResponse<T: Codable>(data: Data?) -> T? {
    do {
        if let data = data {
            return try JSONDecoder().decode(T.self, from: data)
        } else {
            return nil
        }
    } catch {
        return nil
    }
}

class DealApiRequestInterceptor: RequestInterceptor,RequestRetrier {
    
    private var isRefreshing = false
    private let lock = NSLock()
}


class ApiClient {
    static let session: Session = {
        let config = URLSessionConfiguration.default
        if Configuration.buildType == .debug {
            config.protocolClasses?.insert(NFXProtocol.self, at: 0)
        }
        let authInterceptor = DealApiRequestInterceptor()
        let cacher = ResponseCacher(behavior: .cache)
        let session = Session(configuration:config, interceptor:authInterceptor,cachedResponseHandler: cacher)
        return session
    }()
    
    /// The request function for most request to get results in an Observable
    static func request<T: Codable> (_ urlRequestConvertible:URLRequestConvertible) -> Observable<T> {
            return Observable<T>.create { observer in
                let request = session.request(urlRequestConvertible)
                    .validate()
                    .responseDecodable { (response: AFDataResponse<T>) in
                        switch response.result{
                        case .success(let apiResponse):
                            observer.onNext(apiResponse)
                            observer.onCompleted()
                        case .failure(let error):
                            var dealsError:ApiError?
                            if let data = response.data {
                                let errorResponse = try? JSONDecoder().decode(ApiError.self, from: data)
                                dealsError = errorResponse
                            } else {
                                dealsError =  ApiError(status: "",message: error.localizedDescription, responseCode: "", mode: "")
                            }
                            observer.onError(dealsError ?? ApiError(status: "Error", message: ErrorMessages.serviceUnavailable, responseCode: "", mode: ""))
                        }
                    }
                //Finally, we return a disposable to stop the request
                return Disposables.create {
                    request.cancel()
                }
            }
        }

    static func requestSingle(_ urlRequestConvertible:URLRequestConvertible) -> Observable<EmptyResponse> {
            return Observable<EmptyResponse>.create { observer in
                let request = session.request(urlRequestConvertible)
                    .validate()
                    .responseDecodable { (response: AFDataResponse<EmptyResponse>) in
                        switch response.result{
                        case .success(let apiResponse):
                            
                            observer.onNext(apiResponse)
                            observer.onCompleted()
                        case .failure(let error):
                            var dealsError:ApiError?
                            if let data = response.data {
                                let errorResponse = try? JSONDecoder().decode(ApiError.self, from: data)
                                dealsError = errorResponse
                            } else {
                                dealsError =  ApiError(status: "",message: error.localizedDescription, responseCode: "", mode: "")
                            }
                            observer.onError(dealsError ?? ApiError(status: "Error", message: ErrorMessages.serviceUnavailable, responseCode: "", mode: ""))
                        }
                    }
                //Finally, we return a disposable to stop the request
                return Disposables.create {
                    request.cancel()
                }
            }
        }


}

enum FileUploadType {
    case pdf
    case image
    
    func details() -> (extension: String, mimeType: String) {
        switch self {
        case .pdf:
            return ("pdf", "application/pdf")
        case .image:
            return ("jpeg", "image/jpg")
        }
    }
}
