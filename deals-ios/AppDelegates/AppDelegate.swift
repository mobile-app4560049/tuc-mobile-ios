import UIKit
import netfox
import FreshchatSDK
import Reachability
import SwiftMessages
import CoreLocation
import AppCenter
import AppCenterAnalytics
import AppCenterCrashes

@main
class AppDelegate: UIResponder, UIApplicationDelegate {

    var reachability: Reachability?

    static let networkMessageId = "no_network"
    static let networkMessageView: MessageView = {
        let view = MessageView.viewFromNib(layout: .statusLine)
        view.configureContent(body: ErrorMessages.internetDisconnected)
        view.configureTheme(backgroundColor: R.color.red()!, foregroundColor: R.color.white()!)
        view.id = AppDelegate.networkMessageId
        return view
    }()
    
    static let networkMessageConfig: SwiftMessages.Config = {
        var config = SwiftMessages.Config()
        config.presentationStyle = .top
        config.presentationContext = .window(windowLevel: .normal)
        config.duration = .forever
        return config
    }()
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        #if DEBUG
        NFX.sharedInstance().start()
        #endif
        
        UITabBar.appearance().tintColor = R.color.brightPurple()!
        initFreshChat()
        
        reachability = try? Reachability()
        
        reachability?.whenReachable = { _ in
            SwiftMessages.hide(id: AppDelegate.networkMessageId)
        }
        
        reachability?.whenUnreachable = { _ in
            SwiftMessages.show(config: AppDelegate.networkMessageConfig, view: AppDelegate.networkMessageView)
        }
        
        try? reachability?.startNotifier()
        
        let locationManager = CLLocationManager()
        
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
        
        #if DEBUG
            AppCenter.start(withAppSecret: Configuration.environment.appCenterKey, services: [Analytics.self, Crashes.self])
        #else
            AppCenter.start(withAppSecret: Configuration.environment.appCenterKey, services: [Analytics.self, Crashes.self, Distribute.self])
        #endif
        
        return true
    }

    // MARK: - UISceneSession Lifecycle

    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        /// Called when a new scene session is being created.
        /// Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        /// Called when the user discards a scene session.
        /// If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        /// Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }


}

extension AppDelegate {
    func initFreshChat(){
        let freshchatConfig:FreshchatConfig = FreshchatConfig.init(appID: Configuration.environment.freshChatConfig.appID, andAppKey: Configuration.environment.freshChatConfig.appKey)
        freshchatConfig.domain = Configuration.environment.freshChatConfig.domain
        freshchatConfig.gallerySelectionEnabled = true
        freshchatConfig.cameraCaptureEnabled = true
        freshchatConfig.teamMemberInfoVisible = true
        freshchatConfig.showNotificationBanner = true
        Freshchat.sharedInstance().initWith(freshchatConfig)
    }
}
